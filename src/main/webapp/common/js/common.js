//Ajax
function fn_ajax(url, params, successCallback){
	$.ajax({
		url: url,
		type:"post",
		data:params,
		datatype:"json",
		success:function(data) {
			successCallback(data);
		}
		,beforeSend:function(){
			//이미지 보여주기 처리
			$('.wrap-loading').removeClass('display-none');
		}
		,complete:function(){
			//이미지 감추기 처리
			$('.wrap-loading').addClass('display-none');
		}
		,error:function(e){
			//조회 실패일 때 처리
//			alert(e);
		}
		,timeout:30000 //응답제한시간 ms

	});
	
}

//Post Submit (window.location.href)
function locationToPost(url, params) 
{
	var temp=document.createElement("form");
	temp.action = url;
	temp.method = "POST";
	temp.style.display = "none";
	for(var x in params) {
		var opt = document.createElement("textarea");
		opt.name = x;
		opt.value = params[x];
		temp.appendChild(opt);
	}
	
	document.body.appendChild(temp);
	temp.submit();
	return temp;
};


function fn_excel_ajax(url, params, successCallback){
	$.ajax({
		url: url,
		type:"post",
		data: params, 
		datatype:"txt", 
		processData: false,
		contentType: false,
		success:function(data) {
			insertExcelCheckSuc(data);
		},
		error:function(data){
			alert("ajax Exception");
		}
	});
}

//excel file upload (.xlsx)
function uploadExcel(file, type){
	
	if(!isExcel(file.val())){
		alert("xlsx 형식으로 입력해주세요.")
		return false;
	}

	if(confirm("업로드 하시겠습니까?")){
		var excelForm = new FormData();
		excelForm.append("type", type)
		excelForm.append("file", $('#file')[0].files[0]);
		var url = contextRoot + "/common/uploadExcel";
		fn_excel_ajax(url, excelForm);
	}
} 

function insertExcelCheckSuc(data){
	var result = data.result;
	var size = data.size;
	
	if(result == "fail"){
		alert("데이터 적재실패 로그를 확인해 주세요.");
	}else{
		if(size > 0){
			alert(size + "건 정상 반영완료");
			location.reload();
		}else{
			alert("파일이 비어있습니다.");
		}
	}
}

function excelTemplateDown(type){ // Javascript보안 상 ajax로 다운되지 않음, form POST방식 구현
	var input = $("<input>").attr("type", "hidden").attr("name", "type").val(type); //parameter 전달 용 hidden input 구현 (params - new, old, user, scramble)
	
	$("form[name=form]").append($(input));
	$("form[name=form]").serialize();
	$("form[name=form]").attr("action", contextRoot + "/service/downloadExcelTemplate");
	$("form[name=form]").submit();
}

//페이징 처리
function setPageNav(options) {
	var dataCount = parseInt(options.dataCount, 10);

	if (0 >= dataCount) {
		return;
	}

	var pageSize = parseInt(options.pageSize, 10);
	var pageNo = parseInt(options.pageNo, 10);
	var pageGroupSize = parseInt(options.pageGroupSize, 10);
	
	console.log("dataCount >> "+dataCount);
	console.log("pageSize >> "+pageSize);
	console.log("pageNo >> "+pageNo);
	console.log("pageGroupSize >> "+pageGroupSize);
	
	var pageCount = Math.floor((dataCount - 1) / pageSize) + 1;
	var sPageNoInPageGroup = Math.floor( (pageNo - 1) / pageGroupSize ) * pageGroupSize + 1;			// pageGroup 시작 페이지 번호
	var ePageNoInPageGroup = ( Math.floor( (pageNo - 1) / pageGroupSize ) + 1 ) * pageGroupSize;		// pageGroup 종료 페이지 번호
	if (pageCount < ePageNoInPageGroup) {
		ePageNoInPageGroup = pageCount;
	}
	
	options.pageNavList.children().remove();
	
//	options.pageNavList.append('<ul class="pagination modal">');

	if (1 < pageNo) {
		options.pageNavList.append('<li><a href="' + options.makePageLink(1) + '"> 처음 페이지 </a></li>');						// 첫 페이지 link
		options.pageNavList.append('<li><a href="' + options.makePageLink(pageNo - 1) + '"> << </a></li>');			// 직전 페이지 link
	}
	else {
		// 현재 페이지가 1페이지이면 직전 페이지, 첫 페이지 link 걸지 않는다.
		options.pageNavList.append('<li class="disabled"><a href="javascript:;"> 처음 페이지 </a></li>');				// 첫 페이지 link
		options.pageNavList.append('<li class="disabled"><a href="javascript:;"> << </a></li>');				// 직전 페이지 link
	}
	
	for (var idx = sPageNoInPageGroup; idx <= ePageNoInPageGroup; idx++) {
		if(idx == pageNo){
			options.pageNavList.append('<li class="active"><a href="' + options.makePageLink(idx) + '"> ' + idx + ' </a></li>');
		}else{
			options.pageNavList.append('<li><a href="' + options.makePageLink(idx) + '"> ' + idx + ' </a></li>');
		}
	}
	
	if (pageNo < pageCount) {
		options.pageNavList.append('<li><a href="' + options.makePageLink(pageNo + 1) + '"> >> </a></li>');		// 다음 페이지 link
		options.pageNavList.append('<li><a href="' + options.makePageLink(pageCount) + '"> 끝 페이지 </a></li>');			// 마지막 페이지 link
	}
	else {
		// 현재 페이지가 마지막 페이지이면 다음 페이지, 마지막 페이지 link 걸지 않는다.
		options.pageNavList.append('<li class="disabled"><a href="javascript:;"> >> </a></li>');			// 다음 페이지 link
		options.pageNavList.append('<li class="disabled"><a href="javascript:;"> 끝 페이지 </a></li>');				// 마지막 페이지 link
	}
	
//	options.pageNavList.append('</ul>');
}

//스크롤 고정
function disableScroll(){
	$(document).on("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function(e) {
	    e.preventDefault();
	    return;
	});
	$(document).on("keydown.disableScroll", function(e) {
	    var eventKeyArray = [32, 33, 34, 35, 36, 37, 38, 39, 40];
	    for (var i = 0; i < eventKeyArray.length; i++) {
	        if (e.keyCode === eventKeyArray [i]) {
	            e.preventDefault();
	            return;
	        }
	    }
	});
}

//레이어 팝업 open 
function openLayerPopup(url){
	
//	disableScroll();
	
	$("#modifyLayer").empty();
	
	//iframe 사용 -> 새창 변경 (size문제)
	$("#modifyLayer").bPopup({
//      content:'iframe',
//      iframeAttr:'frameborder=”auto”',
//      iframeAttr:'frameborder=”0',
//      contentContainer:'.popupContent',
      loadUrl: url,
      modalClose : false
  	});
	
	
}

//레이어 팝업 close
function closeLayerPopup(successList){
//	$(document).off(".disableScroll");
	$("#modifyLayer").bPopup().close(); 
	if(successList != null && successList != ""){
		successList(1);
	}
//	location.reload();
}

function isExcel(str){
	if(str != ""){
		var ext = str.substring(str.lastIndexOf(".")+1, str.len).toLowerCase();
		if(ext == "xlsx"){
			return true;
		}else{
			return false;
		}
	}
}

//한글 달력 셋팅
$.datepicker.setDefaults({
    dateFormat: 'yy-mm-dd',
    prevText: '이전 달',
    nextText: '다음 달',
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    showMonthAfterYear: true,
    yearSuffix: '년'
});

//파일업로드
function selectUploadFile(formId)
{	
	var fileObj = document.getElementById("files");
	
	var dupChkObj = uploadFormFileChk(fileObj);
	if(dupChkObj.result){
		var fileSeq = $("#fileSeq").val();
		
		var option = {
		   	url : contextRoot + "/admin/fileSave",
		   	type : "post",
		   	data : $("#" + formId).serialize(),
		   	dataType : "xml",
	   		success : function(data) {
	   			// ajaxSubmit은 파일업로드시 임의로 iframe을 만들어서 submit하므로 ajax헤더가 없다.(form submit사용)
	   			// interceptor에서 isAjaxRequest==false이므로 에러시 에러페이지를 받아오고
	   			// ajaxSubmit에서 ajax응답으로 바꿔 보여준다. (alert으로 에러페이지 코드출력)
	   			// 에러인지 확인하기위해 exception으로 사용하는 페이지에 <div id="errorMsg">태그를 추가함
	   			if($(data).find("#errorMsg").length > 0){
	   				alert($(data).find("#errorMsg").attr("error"));
	   			}else{
	   				var retData = JSON.parse(data);	// ie8 다운로드 문제로 text값으로 return받아 json으로 변환
			   		if( retData != "" && retData.result)
		   			{
			   			$("#fileSeq").val(retData.fileSeq);
			   			
				   		var params = {
				   				fileSeq : retData.fileSeq
						};
				   		fn_ajax(contextRoot + "/admin/fileList", params, function(retData) { resultFileList(retData); } );
		   			}
	   			}
	   		},
	   		error : function(xhr,status,err)
			{
	   			fileObj.value = "";
				if(xhr.status == 420){
					//alert(com_msg_00021);/*세션이 종료되어 로그인화면으로 이동합니다.*/
					//location.href = contextRoot + "/login/ssoLogin";
				}else if(xhr.status == 421){
					alert(com_msg_00022);/*권한이 없습니다.*/
				}else{
					alert("[Error] status : " + xhr.status + ", resText : " + xhr.responseText);
				}
			},
			beforeSend : function(xhr){
				$.blockUI({	timeout: 60000*5});
			},
			complete : function(xhr, textStatus) {	$.unblockUI();}
	  	};
	   	$("#" + formId).ajaxSubmit(option);
	}else{
		alert(cts_msg_00015 + " [" + dupChkObj.dupCnt + com_txt_00004 + "]");	/*중복된 파일입니다. [x건]*/
		return false;
	}
	
}

//파일명 중복체크
function uploadFormFileChk(obj)
{
	var pathHeader = obj.value.lastIndexOf("\\");
	var pathMiddle = obj.value.lastIndexOf(".");
	var pathEnd = obj.value.length;
	var fileName = obj.value.substring(pathHeader+1, pathMiddle);
	var extName = obj.value.substring(pathMiddle+1, pathEnd);
	var fileNm = fileName + "." + extName;
	
	var dupCnt = 0;
	
	$("#uploadFileListArea").find("li").each(function(){
		var fileName = $(this).attr("filename");
		if(fileNm == fileName){
			dupCnt += 1;
		}
	});
	
	var retObj = new Object();
	if(dupCnt > 0){
		retObj.result = false;
	}else{
		retObj.result = true;
	}
	retObj.dupCnt = dupCnt;
	return retObj;
}


//function CallbackObj() {
//	   this.result_cd;
//	   this.result_msg;
//	   this.extra;
//	}
//
//	CallbackObj.prototype = {
//	      setResult: function(result_cd, result_msg, extra) { // callback
//	         this.result_cd = result_cd;
//	         this.result_msg = result_msg;
//	         this.extra = extra;
//	      },
//	      getExtra: function() {
//	         return extra;
//	      },
//	      isSuccess: function() {
//	         return (result_cd === 200 && result_msg == RESPONSE_OK) ? true : false;
//	      }
//	}
//	
//	
//	gigagenie.init(options, cb.setResult);
//	if(cb.isSuccess) {
//	   console.log("initialize success");
//	}