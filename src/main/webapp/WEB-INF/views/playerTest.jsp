<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>SoundJS: Media Player Example</title>

<%-- 	<link href="${pageContext.request.contextPath}/resources/soundjs/css/shared.css" rel="stylesheet" type="text/css"/> --%>
	<link href="${pageContext.request.contextPath}/resources/soundjs/css/examples.css" rel="stylesheet" type="text/css"/>
	<link href="${pageContext.request.contextPath}/resources/soundjs/css/soundjs.css" rel="stylesheet" type="text/css"/>
	<script src="${pageContext.request.contextPath}/resources/soundjs/js/examples.js"></script>

</head>
<body onload="init()">
<div id="content" class="content" style="position: absolute; height: auto">
	<div id="player">
		<div id="playBtn" class="button playBtn"></div>
		<div id="stopBtn" class="button stopBtn"></div>
		<div id="labels">
			<label id="song">Song: <strong>Pirates Love Daisies</strong></label><br/>
			<label id="artist">Artist: <strong>Washingtron</strong></label><br/><br/>
			<label id="loading">Waiting...</label>
		</div>
		<div id="track">
			<div id="progress"></div>
			<div id="thumb"></div>
		</div>
	</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/soundjs/lib/soundjs-NEXT.combined.js"></script>
<!-- We also provide hosted minified versions of all CreateJS libraries.
	http://code.createjs.com -->

<!-- other libraries -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<script type="text/javascript">
	function init() {
		if (!createjs.Sound.initializeDefaultPlugins()) {
			document.getElementById("error").style.display = "block";
			document.getElementById("content").style.display = "none";
			return;
		}

		$("#position").css("display", "none");
		$("#playPauseBtn").attr("disabled", true);
		$("#stopBtn").attr("disabled", true);
		$("#track").css("display", "none");

		examples.showDistractor("content");
		var assetsPath = "${pageContext.request.contextPath}/resources/soundjs/audio/";
		var src = assetsPath + "M-GameBG.ogg";

		createjs.Sound.alternateExtensions = ["mp3"];	// add other extensions to try loading if the src file extension is not supported
		createjs.Sound.addEventListener("fileload", createjs.proxy(handleLoadComplete, this)); // add an event listener for when load is completed
		createjs.Sound.registerSound(src, "music");
	}

	var instance;
	var positionInterval;
	var seeking = false;

	function handleLoadComplete(event) {
		examples.hideDistractor();

		$("#track").css("display", "block");
		$("#loading").css("display", "none");
		$("#progress").css("display", "none");
		$("#position").css("display", "block");

		instance = createjs.Sound.play("music");
		instance.addEventListener("complete", function () {
			clearInterval(positionInterval);
			$("#playBtn").removeClass("pauseBtn").addClass("playBtn")
			$("#stopBtn").attr("disabled", true);
		});
		$("#playPauseBtn").attr("disabled", false);
		$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
		$("#playBtn").click(function (event) {
			if (instance.playState == createjs.Sound.PLAY_FINISHED) {
				instance.play();
				$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
				trackTime();
				return;
			} else {
				instance.paused ? instance.paused = false : instance.paused = true;
			}

			if (instance.paused) {
				$("#playBtn").removeClass("pauseBtn").addClass("playBtn");
			} else {
				$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
			}
		});
		$("#stopBtn").click(function (event) {
			instance.stop();
			//console.log("stop");
			clearInterval(positionInterval);
			$("#playBtn").removeClass("pauseBtn").addClass("playBtn");
			$("#thumb").css("left", 0);
		});
		$("#stopBtn").attr("disabled", false);

		trackTime();

		// http://forums.mozillazine.org/viewtopic.php?f=25&t=2329667
		$("#thumb").mousedown(function (event) {
			//console.log("mousedown");
			var div = $();
			$("#player").append($("<div id='blocker'></div>"));
			seeking = true;
			$("#player").mousemove(function (event) {
				// event.offsetX is not supported by FF, hence the following from http://bugs.jquery.com/ticket/8523
				if (typeof event.offsetX === "undefined") { // || typeof event.offsetY === "undefined") {
					var targetOffset = $(event.target).offset();
					event.offsetX = event.pageX - targetOffset.left;
					//event.offsetY = event.pageY - targetOffset.top;
				}
				$("#thumb").css("left", Math.max(0, Math.min($("#track").width() - $("#thumb").width(), event.offsetX - $("#track").position().left)));
			})
			$("#player").mouseup(function (event) {
				//console.log("mouseup");
				seeking = false;
				$(this).unbind("mouseup mousemove");
				var pos = $("#thumb").position().left / $("#track").width();
				instance.position = (pos * instance.duration);
				$("#blocker").remove();
			});
		});
	}

	var dragOffset;
	function trackTime() {
		positionInterval = setInterval(function (event) {
			if (seeking) {
				return;
			}
			$("#thumb").css("left", instance.getPosition() / instance.getDuration() * $("#track").width());
		}, 30);
	}
</script>

</body>
</html>
