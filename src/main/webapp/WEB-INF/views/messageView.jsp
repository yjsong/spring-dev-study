<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.form.js"></script>
<html>
<head>
	<title>Message</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//보내기 버튼 클릭시
	$("#send_btn").click(function(){
		var userKey = $("#userKey").val();
		var userId = $("#userId").val();
		var messageContent = $("#messageContent").val();
		
		if(messageContent == null || messageContent == ""){
			alert("내용을 입력하세요.");
			$("#messageContent").focus();
			return;
		}

		var param = {
				userKey	:	userKey,
				userId	:	userId,
				messageContent	:	messageContent
		}

		var url = contextRoot + "/admin/insertMessageAjax";
		
		fn_ajax(url, param, insertCheckSuc);
		
	});
	
	//취소 버튼 클릭시
	$("[name=cancel_btn]").click(function(){
		parent.closeLayerPopup();
	});
	
});

function insertCheckSuc(data){
	if(data == 1){
		alert("전송 되었습니다.");
		parent.closeLayerPopup(parent.goUserList);
	}else{
		alert("전송 실패");
		return;
	}
}
</script>
</head>
<body style="overflow: hidden;">
<form name="editForm" id="editForm" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
<input type="hidden" id="userKey" value="${userKey}">
<input type="hidden" id="userId" value="${userId}">
	<div class="row" id="defaultView">
    	<div class="panel panel-default">
        	<div class="panel-heading">
                Message View
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive" style="overflow: hidden;">
                    <table class="table table-striped table-bordered table-hover">
						<tr>
							<td>user id</td>
							<td colspan="3">
								${userId}
<!-- 								<input type="text" id="userId" value="" style="width: 100%;"> -->
							</td>
<!-- 							<td colspan="3"><input type="file" size="30" id="file" name="file" onchange="getExcelForm()" /></td> -->
						</tr>
						<tr>
							<td height="200px;">content</td>
							<td colspan="3"><textarea rows="11" cols="54" id="messageContent"></textarea></td>
						</tr>
						<tr style="border: 0" height="30">
							<td colspan="4" align="center">
								<a href="javascript:;" id="send_btn" class="btn btn-outline btn-primary">보내기</a>
								<a href="javascript:;" name="cancel_btn" class="btn btn-outline btn-info">취소</a>
							</td>
						</tr>
					</table>
				</div>
	           <!-- /.table-responsive -->
			</div>
	        <!-- /.panel-body -->
		</div>
	   	<!-- /.panel -->
	</div>
</form>
</body>
</html>
