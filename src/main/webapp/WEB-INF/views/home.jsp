<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
<%-- <link href="${pageContext.request.contextPath}/resources/soundjs/css/shared.css" rel="stylesheet" type="text/css"/> --%>
<link href="${pageContext.request.contextPath}/resources/soundjs/css/examples.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/resources/soundjs/css/soundjs.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/resources/c3/css/c3.css" rel="stylesheet" type="text/css">

<!-- Morris Charts JavaScript -->
<%-- <script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/raphael/raphael.min.js"></script> --%>
<%-- <script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/morrisjs/morris.min.js"></script> --%>
<%-- <script src="${pageContext.request.contextPath}/resources/bootstrap/data/morris-data.js"></script> --%>
<script src="${pageContext.request.contextPath}/resources/soundjs/js/examples.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/soundjs/lib/soundjs-NEXT.combined.js"></script>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/resources/c3/js/c3.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	goPlayer();
	goChart();
});

function goChart(){
	var url = contextRoot + "/dashInfoAjax";
	fn_ajax(url, "", setChart);
}

function setChart(data){
		
	var date = JSON.parse("[" + data.date + "]");
	var visit = JSON.parse("[" + data.visit + "]");
	var members = JSON.parse("[" + data.members + "]");
	var boardTime = $("#boardTime").html(data.boardTime + " ago");
	
	var chart = c3.generate({
	    data: {

	    	x: "date",
	    	columns: [
// 				[test, "2017-07-05", "2017-07-06","2017-07-07","2017-07-08","2017-07-09", "2017-07-10"],
				date,
// 	        	['members', 18, 18, 18, 18, 18, 19],
				visit,
// 	        	['user', 15, 20, 10, 40, 15, 3]
				members
	      	],
	      	axes: {
	      		data1: 'y',
	      	}
		},
	    axis: {
	      	x: {
	    		type: "category"
	      	},
	      	y: {
	        	label: {
	          	text: 'Y Axis Label',
	          	position: 'outer-middle'
	       		}
	    	},
	    },
		});
	
	
}

function goPlayer() {
	if (!createjs.Sound.initializeDefaultPlugins()) {
		document.getElementById("error").style.display = "block";
		document.getElementById("content").style.display = "none";
		return;
	}

	$("#position").css("display", "none");
	$("#playPauseBtn").attr("disabled", true);
	$("#stopBtn").attr("disabled", true);
	$("#track").css("display", "none");

	examples.showDistractor("content");
	var assetsPath = "${pageContext.request.contextPath}/resources/soundjs/audio/";
	var src = assetsPath + "M-GameBG.ogg";

	createjs.Sound.alternateExtensions = ["mp3"];	// add other extensions to try loading if the src file extension is not supported
	createjs.Sound.addEventListener("fileload", createjs.proxy(handleLoadComplete, this)); // add an event listener for when load is completed
	createjs.Sound.registerSound(src, "music");
}

var instance;
var positionInterval;
var seeking = false;

function handleLoadComplete(event) {
	examples.hideDistractor();

	$("#track").css("display", "block");
	$("#loading").css("display", "none");
	$("#progress").css("display", "none");
	$("#position").css("display", "block");

	instance = createjs.Sound.play("music");
	instance.addEventListener("complete", function () {
		clearInterval(positionInterval);
		$("#playBtn").removeClass("pauseBtn").addClass("playBtn")
		$("#stopBtn").attr("disabled", true);
	});
	$("#playPauseBtn").attr("disabled", false);
	$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
	$("#playBtn").click(function (event) {
		if (instance.playState == createjs.Sound.PLAY_FINISHED) {
			instance.play();
			$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
			trackTime();
			return;
		} else {
			instance.paused ? instance.paused = false : instance.paused = true;
		}

		if (instance.paused) {
			$("#playBtn").removeClass("pauseBtn").addClass("playBtn");
		} else {
			$("#playBtn").removeClass("playBtn").addClass("pauseBtn");
		}
	});
	$("#stopBtn").click(function (event) {
		instance.stop();
		//console.log("stop");
		clearInterval(positionInterval);
		$("#playBtn").removeClass("pauseBtn").addClass("playBtn");
		$("#thumb").css("left", 0);
	});
	$("#stopBtn").attr("disabled", false);

	trackTime();

	// http://forums.mozillazine.org/viewtopic.php?f=25&t=2329667
	$("#thumb").mousedown(function (event) {
		//console.log("mousedown");
		var div = $();
		$("#player").append($("<div id='blocker'></div>"));
		seeking = true;
		$("#player").mousemove(function (event) {
			// event.offsetX is not supported by FF, hence the following from http://bugs.jquery.com/ticket/8523
			if (typeof event.offsetX === "undefined") { // || typeof event.offsetY === "undefined") {
				var targetOffset = $(event.target).offset();
				event.offsetX = event.pageX - targetOffset.left;
				//event.offsetY = event.pageY - targetOffset.top;
			}
			$("#thumb").css("left", Math.max(0, Math.min($("#track").width() - $("#thumb").width(), event.offsetX - $("#track").position().left)));
		})
		$("#player").mouseup(function (event) {
			//console.log("mouseup");
			seeking = false;
			$(this).unbind("mouseup mousemove");
			var pos = $("#thumb").position().left / $("#track").width();
			instance.position = (pos * instance.duration);
			$("#blocker").remove();
		});
	});
}

var dragOffset;
function trackTime() {
	positionInterval = setInterval(function (event) {
		if (seeking) {
			return;
		}
// 		$("#thumb").css("left", instance.getPosition() / instance.getDuration() * $("#track").width());
	}, 30);
}

</script>
</head>
<body>
<div> ${sessionScope.totalCount}</div>
<div> ${sessionScope.todayCount}</div>

<audio>
  <!-- 파이어폭스와 크롬을 위한 WebM 포맷 -->
<!--   <source src="foo" type='video/webm; codecs="vp8, vorbis"'> -->
  <!-- IE와 사파리를 위한 H.264 포맷 -->
  <source src="bar" type='video/mp4; codecs="av1.42E01E, mp4a.40.2"'>
</audio>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-envelope fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">${dashboardInfo.messageCnt}</div>
                            <div>New Message!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-edit fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">${dashboardInfo.boardCnt}</div>
                            <div>New Board!</div>
                        </div>
                    </div>
                </div>
                <a href="${pageContext.request.contextPath}/board/developBoardList">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
		<div id="content" class="content" style="position: absolute; height: auto">
			<div id="player">
				<div id="playBtn" class="button playBtn"></div>
				<div id="stopBtn" class="button stopBtn"></div>
				<div id="labels">
					<label id="song">Song: <strong>Pirates Love Daisies</strong></label><br/>
					<label id="artist">Artist: <strong>Washingtron</strong></label><br/><br/>
					<label id="loading">Waiting...</label>
				</div>
				<div id="track">
					<div id="progress"></div>
					<div id="thumb"></div>
				</div>
			</div>
		</div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a href="#">Action</a>
                                </li>
                                <li><a href="#">Visit</a>
                                </li>
                                <li><a href="#">Members</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="${pageContext.request.contextPath}/admin/userList">UserList link</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
<!--                     <div id="morris-area-chart"></div> -->
					<div id="chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bell fa-fw"></i> Notifications Panel
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="list-group">
                        <a href="${pageContext.request.contextPath}/api/map" class="list-group-item">
                            <i class="fa fa-gears fa-fw"></i> API
                            <span class="pull-right text-muted small"><em># view api</em>
                            </span>
                        </a>
<!--                         <a href="#" class="list-group-item"> -->
<!--                             <i class="fa fa-desktop fa-fw"></i> UI -->
<!--                             <span class="pull-right text-muted small"><em>12 minutes ago</em> -->
<!--                             </span> -->
<!--                         </a> -->
                        <a href="${pageContext.request.contextPath}/server/state" class="list-group-item">
                            <i class="fa fa-desktop fa-fw"></i> Server
                            <span class="pull-right text-muted small"><em># view server</em>
                            </span>
                        </a>
                        <a href="${pageContext.request.contextPath}/board/developBoardList" class="list-group-item">
                            <i class="fa fa-edit fa-fw"></i> Board
                            <span class="pull-right text-muted small"><em id="boardTime">27 minutes ago</em>
                            </span>
                        </a>
                        <a href="${pageContext.request.contextPath}/database/mongo"" class="list-group-item">
                            <i class="fa fa-save fa-fw"></i> Database
                            <span class="pull-right text-muted small"><em>43 minutes ago</em>
                            </span>
                        </a>
                        <a href="${pageContext.request.contextPath}/admin/labelList" class="list-group-item">
                            <i class="fa fa-sitemap fa-fw"></i> Admin
                            <span class="pull-right text-muted small"><em>11:32 AM</em>
                            </span>
                        </a>
                        <a href="${pageContext.request.contextPath}/chatting" class="list-group-item">
                            <i class="fa fa-desktop fa-fw"></i> Chatting
                            <span class="pull-right text-muted small"><em># view chatting</em>
                            </span>
                        </a>
                        <a href="${pageContext.request.contextPath}/develop" class="list-group-item">
                            <i class="fa fa-book fa-fw"></i> Develop
                            <span class="pull-right text-muted small"><em>11:13 AM</em>
                            </span>
                        </a>
                    </div>
                    <!-- /.list-group -->
                    <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel .chat-panel -->
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->
</div>
</body>
</html>
