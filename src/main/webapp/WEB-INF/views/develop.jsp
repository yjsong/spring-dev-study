<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Develop</title>
<script type="text/javascript">
$(document).ready(function(){
});
</script>
</head>
<body>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Develop List</h1>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div>
    	<h4>spring boot tiles example</h4>
    	<a href="https://gitlab.com/yjsong/tiles-demo">GIT > https://gitlab.com/yjsong/tiles-demo</a><br>
    	<a href="http://yjsong.ze.am:8080/tiles-demo/home">URL > http://yjsong.ze.am:8080/tiles-demo/home</a>
    </div>
</div>
<!-- /#page-wrapper -->
</body>
</html>
