<%--
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.RandomAccessFile"%>
<%@page import="java.io.File"%>
<%@page import="javax.sql.DataSource"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="com.sun.management.OperatingSystemMXBean" %>
<%@page import="java.lang.management.ManagementFactory" %>
<%@page import="com.zinnaworks.smtdv.common.Common" %>
<%@page import="com.zinnaworks.util.DBConn" %>

<%@page import="org.apache.log4j.Logger"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<%! 
	private String toMB(long size){
		return (int)(size/(1024*1024))+"(MB)"; 
	}
	
	private String toPercent(long usize, long tsize){		
		double useable = usize/(1024*1024);
		double total 	= tsize/(1024*1024);
		
		String pattern = "###.##";
		DecimalFormat dformat = new DecimalFormat(pattern); 
		String percentage  = dformat.format((useable/total)*100);
	
		return percentage+"%";
	}
	
	public static String executeCommand(String[] commandArr){
// 		 System.out.println("Linux command: " + java.util.Arrays.toString(commandArr));
		 	String excute_result = "";
			try {
			  ProcessBuilder pb = new ProcessBuilder(commandArr);
			  pb.redirectErrorStream(true);
			  Process proc = pb.start();
			  BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));             
			
			  String line;             
			  while ((line = in.readLine()) != null) {
			      excute_result = line;
			  }
			
			  proc.destroy();
			  
			} catch (Exception x) {
			  x.printStackTrace();
			}
		return excute_result;
	}
%>

<%
	Logger log = Logger.getLogger(getClass().getSimpleName()); 

	String callback = request.getParameter("callback");
	
	WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest)request).getSession().getServletContext());
	Common common = (Common)wac.getBean("common");
	
 	Map<String, Object> result = new HashMap<String, Object>();
	Map<String, Object> subMap = new HashMap<String, Object>();
	
	String logPath = common.getLogPath();
	String[] hostArr = {"/bin/sh", "-c", "hostname"};
	String hostName = executeCommand(hostArr);
	
// 	log.info("---- Get TPS ----");
	
	Date date = new Date();
	
	Calendar cal = Calendar.getInstance();
	cal.setTime(date);
	cal.add(Calendar.SECOND, -10);
	
	Date newDate = new Date(cal.getTimeInMillis());
	
	String inDate   = new java.text.SimpleDateFormat("yyyy-MM-dd").format(newDate);
	String inTime   = new java.text.SimpleDateFormat("HH:mm:ss").format(newDate);
	
// 	log.info("date : " + inDate);
// 	log.info("time : " + inTime);
	
	String hr = new java.text.SimpleDateFormat("HH").format(newDate);
	
	String tpsCallStr = logPath + hostName +"_access_log.";
	String[] tpsCmdArr = {"/bin/sh", "-c", "cat " + tpsCallStr + inDate +"." + hr +".log | grep " + inTime +" -c"};

	subMap = new HashMap<String, Object>();

// 	System.out.println("tps count ::" + executeCommand(tpsCmdArr));
	
	result.put("tps", executeCommand(tpsCmdArr));
	
// 	log.info("---- End TPS ----");
	
	
// 	log.info("---- Start Get Disk ----");

	File root = null;
	root = new File("/");
// 	System.out.println("Total Space : " + toMB(root.getTotalSpace()));
// 	System.out.println("Useable Space : " + toMB(root.getUsableSpace()));
// 	System.out.println("Useable Space Percentage : " + toPercent(root.getUsableSpace(), root.getTotalSpace()));
	
	subMap.put("total_space", toMB(root.getTotalSpace()));
	subMap.put("useable_space", toMB(root.getUsableSpace()));
	subMap.put("useable_disk_percentage", toPercent(root.getUsableSpace(), root.getTotalSpace()));
	
	result.put("space",subMap);
	
// 	log.info("---- End Get Disk ----");

// 	log.info("---- Start Get OS ----");
	
	OperatingSystemMXBean osbean = ( OperatingSystemMXBean ) ManagementFactory.getOperatingSystemMXBean( );
// 	System.out.println("OS Name: " + osbean.getName( ) );
// 	System.out.println("OS Arch: " + osbean.getArch( ) );
// 	System.out.println("Available Processors: " + osbean.getAvailableProcessors());
// 	System.out.println("TotalPhysicalMemorySize: " + toMB(osbean.getTotalPhysicalMemorySize()) );
// 	System.out.println("FreePhysicalMemorySize: " + toMB(osbean.getFreePhysicalMemorySize()) );
// 	System.out.println("TotalSwapSpaceSize: " + osbean.getTotalSwapSpaceSize());
// 	System.out.println("FreeSwapSpaceSize: " + osbean.getFreeSwapSpaceSize());
// 	System.out.println("CommittedVirtualMemorySize: " + osbean.getCommittedVirtualMemorySize());
// 	System.out.println("SystemLoadAverage: " + osbean.getSystemLoadAverage());
	
	subMap = new HashMap<String, Object>();
	
	subMap.put("total_memory_size", toMB(osbean.getTotalPhysicalMemorySize()));
	subMap.put("free_memory_size", toMB(osbean.getFreePhysicalMemorySize()));
	subMap.put("use_memory_percentage", toPercent(osbean.getFreePhysicalMemorySize(),osbean.getTotalPhysicalMemorySize()));
	
	result.put("memory", subMap);

// 	log.info("---- End Get OS ----");
	
 	
 	String dbFlag = "false";
	
 	if(DBConn.getConnection(common, "cms") != null){
 		dbFlag = "true";
 	}
 	
 	DBConn.close();
 	
	result.put("db_connection", dbFlag);

 	dbFlag = "false";
 	
 	if(DBConn.getConnection(common, "sms") != null){
 		dbFlag = "true";
 	}
 	
 	DBConn.close();

	result.put("sms_db_connection", dbFlag);

	dbFlag = "false";
	
	if(DBConn.redis(common) != false){
		dbFlag = "true";		
	}
	
	result.put("redis", dbFlag);
	
	DBConn.redisClose();
		
   	float cpuUsage = 0;
   	try {
        RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
        String load = reader.readLine();

        String[] toks = load.split(" +");  // Split on one or more spaces

        long before_user = Long.parseLong(toks[1]);
        long before_nice = Long.parseLong(toks[2]);
        long before_system = Long.parseLong(toks[3]);
        long before_idle = Long.parseLong(toks[4]) ;

	    try {
	         Thread.sleep(1000);
	    } catch (Exception e) {}

	    reader.seek(0);
	    load = reader.readLine();
	    reader.close();

	    toks = load.split(" +");

	    long after_user = Long.parseLong(toks[1]);
	    long after_nice = Long.parseLong(toks[2]);
	    long after_system = Long.parseLong(toks[3]);
	    long after_idle = Long.parseLong(toks[4]) ;

	    long user = after_user - before_user;
	    long nice = after_nice - before_nice;
	    long system = after_system - before_system;
	    long idle = after_idle - before_idle;
	    
	    //System.out.println("user : " + user + ", nice : " + nice + ", system : " + system + ", idle : " + idle);
	    
	    cpuUsage =  (float)(user*100) / (user+nice+system+idle);
    } catch (IOException ex) {
        ex.printStackTrace();
    }
   	
   	String pattern = "###.##";
    DecimalFormat dformat = new DecimalFormat(pattern);
    String percentage  = dformat.format(cpuUsage);

   	result.put("cpu_usage", percentage+"%" );
   	
	request.setAttribute("xResult", result);
	
	RequestDispatcher dispatcher = null;
		
	if( callback == null ){
		out.clear();
		dispatcher = request.getRequestDispatcher("json.jsp");
	}
	else{
		request.setAttribute("xName", callback );
		
		dispatcher = request.getRequestDispatcher("jsonSys.jsp");
	}
	
	dispatcher.forward(request, response);
%> 
--%>