<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Movie House Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        html, body {
            height: 90%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }
    </style>
</head>
<body>
<div id="map"></div>
</body>
<!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD109SU7yACmNd2lE4BLi1Pw3RrsMTDG_8"></script>
<script>
    var googleMap = {
//         movieHouseList : ${movieHouseList},
 		movieHouseList : null,
        map : null,
        infoWindow : null,
        markers : [],
        init : function() {
            var that = this;

            this.infoWindow = new google.maps.InfoWindow;

            var myLatLng = {lat: 37.56987771568424, lng: 126.99179028418783};
            this.map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 15
            });

            this.checkNearBy(myLatLng.lat, myLatLng.lng);

            google.maps.event.addListener(that.map, 'dragend', function() {
                that.dragMap();
            });
        },

        deg2rad : function(deg){
            return deg * Math.PI / 180;
        },

        dragMap : function() {
            this.clearLocations();
            var pos = this.map.getCenter();

            this.checkNearBy(pos.lat(), pos.lng());
        },

        checkNearBy : function(lat, lng) {
            for(var i in this.movieHouseList) {
                var dist = 6371 * Math.acos(Math.cos(this.deg2rad(lat)) * Math.cos(this.deg2rad(this.movieHouseList[i].latitude)) * Math.cos(this.deg2rad(this.movieHouseList[i].longitude) - this.deg2rad(lng)) + Math.sin(this.deg2rad(lat)) * Math.sin(this.deg2rad(this.movieHouseList[i].latitude)));

                if(dist < 2) {
                    this.createMarkers(this.movieHouseList[i]);
                }
            }
        },

        createMarkers : function(movieHouseInfo) {
            var that = this;

            var marker = new google.maps.Marker({
                position: {lat: Number(movieHouseInfo.latitude), lng: Number(movieHouseInfo.longitude)},
                map: that.map,
                label: function() {
                    switch (movieHouseInfo.movieHouseType) {
                        case '0' : return '독';
                        case '1' : return 'C';
                        case '2' : return '롯';
                        case '4' : return '메';
                    }
                }()
            });

            marker.addListener('click', function () {
                that.infoWindow.setContent(that.getInfoWindowContent(movieHouseInfo));
                that.infoWindow.open(that.map, marker);
            });

            that.markers.push(marker);
        },

        getInfoWindowContent : function(info) {
            var content = '<div id="content">' +
                    '<p id="name"><b>' + info.movieHouseName + '</b></p>' +
                    '<p id="address">' + info.address + '</p>' +
                    (info.tel ? '<p id="tel">전화번호 : <a href="' + info.tel + '">' + info.tel + '</a></p>' : '') +
                    (info.homePage ? '<p id="homePage"><a href="' + info.homePage +'">' + info.homePage + '</a></p>' : '') +
                    (info.movieHouseIntroduce ? '<p id="introduce">' + info.movieHouseIntroduce + '</p>' : '') +
                    (info.transportInformation ? '<p id="transport"><b>오시는 길</b></br>' + info.transportInformation + '</p>' : '') +
                    (info.parkingInformation ? '<p id="parking"><b>주차 안내</b></br>' + info.parkingInformation + '</p>' : '') +
                    '</div>'
            return content;
        },

        clearLocations : function() {
            for(var i in this.markers) {
                this.markers[i].setMap(null);
            }

            this.markers = [];
        }
    }

    $(document).ready(function() {
        googleMap.init();
    })
</script>
</html>