<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Captcha</title>
<script type="text/javascript">
$(document).ready(function(){
	$("#confirm_btn").click(function(){
		
		var url = contextRoot + "/api/captchaCompareAjax";
		var params = {
				captchaText : $("#captchaText").val()
		};

		fn_ajax(url, params, captchaSuc);
	});
});

function captchaSuc(data){
	var result = data.result;
	if(result){
		alert("맞음");
	}else{
		alert("틀림");
		location.reload(true);
	}
}
</script>
</head>
<body>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">CAPTCHA</h1>
            </div>
           	<h4>NAVER CAPTCHA</h4>
               <img src="${url}">
			<input type="text" id="captchaText" >
			<button id="confirm_btn">확인</button>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>
