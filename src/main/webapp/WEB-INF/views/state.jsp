<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>State Check</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//서버 상태 체크
	serverStateCheck();
	
	//10초마다 서버체크 실행
	setInterval(function() {serverStateCheck();}, 10000);
	
});

//서버 상태 체크
function serverStateCheck(){
	var url = contextRoot + "/server/serverStateCheckAjax";

	fn_ajax(url, "", serverStateCheckSuc);
}

function serverStateCheckSuc(data){
	console.log("data : " + JSON.stringify(data));
	var keys = Object.keys(data);
	var trrArray = [];
	var temp = 0;
	var state;
	var stateColor;
	
	$("#serverList").empty();
	
	if(keys.length != null && keys.length > 0){
		for(var i=0; i<keys.length; i++){
			state = "OFF";
			stateColor = "red";
			if(data[keys[i]][0].result == "Y"){
				state = "ON";
				stateColor = "blue";
			}
			trrArray[temp++] = "<tr>";
			trrArray[temp++] = "	<td>"+(i+1)+"</td>";
			trrArray[temp++] = "	<td>"+data[keys[i]][0].hostname+"</td>";
			trrArray[temp++] = "	<td>"+keys[i]+"</td>";
			trrArray[temp++] = "	<td style='color:" + stateColor +"'>"+state+"</td>";
			trrArray[temp++] = "</tr>";
		}
	}else{
		trrArray[temp++] = "<tr>";
		trrArray[temp++] = "	<td align='center' colspan='4'>조회결과가 없습니다.</td>";
		trrArray[temp++] = "</tr>";
	}
	$("#serverList").append(trrArray.join(''));
}
</script>
</head>
<body>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
                <h1 class="page-header">State Check</h1>
            </div>
            <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Server State Check Table
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>hostname</th>
                                            <th>IP</th>
                                            <th>state</th>
                                        </tr>
                                    </thead>
                                    <tbody id="serverList">
                                    	<!-- 
                                        <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                         -->
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>
