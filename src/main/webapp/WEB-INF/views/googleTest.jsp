<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
<script type="text/javascript">
  (function() {
    var po = document.createElement('script');
    po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
  })();

  function render() {
    gapi.signin.render('btn_google', {
      'callback': 'signinCallback',
      'clientid': '458666073437-jne60nl5ful60e8ab99939mmviobfidf.apps.googleusercontent.com',
      'cookiepolicy': 'single_host_origin',
      'scope': 'https://www.googleapis.com/auth/plus.login'
    });
  }
  function signinCallback(authResult) {
    // Respond to signin, see https://developers.google.com/+/web/signin/
	  gapi.client.load('oauth2', 'v2', function() {
			var request = gapi.client.oauth2.userinfo.get();		//사용자 정보 요청
			request.execute(returnGoogleUserInfo);				//사용자 정보 조회 후 콜백함수 설정
		});
  }
  
  function returnGoogleUserInfo(res){
	  console.log(res.id);
  }
  </script>
</head>
<body>
<div id="customBtn" class="customGPlusSignIn">
  <span class="icon"></span>
  <span class="buttonText">Google</span>
</div>
<div id="btn_google" class="customGPlusSignIn">
	 <span class="btn_google"></span>
</div>
</body>
</html>