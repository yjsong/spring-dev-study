<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Label</title>
<script src="${pageContext.request.contextPath}/resources/jstree/dist/jstree.min.js" type="text/javascript"></script>
<link href="${pageContext.request.contextPath}/resources/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	
	$("#demo1").jstree({
	    "ui": {
	        "select_limit": -1,
	        "select_multiple_modifier": "alt", //중복 선택시 사용할 키
	        "selected_parent_close": "select_parent",
	        "initially_select": ["phtml_2"]
	    },
	    "core": { "initially_open": ["phtml_1"] },
	    "plugins": ["themes", "html_data", "ui"]
	});

});
</script>
</head>
<body>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Label</h1>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    
    <div id='demo1' class='demo'>
        <ul>
            <li id="phtml_1">
                <a href="#">Root node 1</a>
                <ul>
                    <li id="phtml_2">
                        <a href="#">Child node 1</a>
                    </li>
                    <li id="phtml_3">
                        <a href="#">Child node 2</a>
                    </li>
                </ul>
            </li>
            <li id="phtml_4">
                <a href="#">Root node 2</a>
            </li>
        </ul>
    </div>

</div>
<!-- /#page-wrapper -->
</body>
</html>
