<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>error</title>
<jsp:include page="/layout/Common.jsp" flush="false"/>
<script type="text/javascript">
function moveMain()
{
	location.href=contextRoot + "/home";
}
</script>
</head>
<body>
<h1>
404 Page Not Found
</h1>
<hr />
<h3 class="lighter smaller">We looked everywhere but we couldn't find it!</h3>
<div>
	<h4 class="smaller">Try one of the following:</h4>
</div>
<hr />
<div>
	<a href="#" class="btn btn-outline btn-default" onclick="javascript:history.go(-1); return false;">
		Go Back
	</a>

	<a href="#" class="btn btn-outline btn-primary" onclick="javascript:moveMain();"> 
		Site Home
	</a>
</div>
</body>
</html>