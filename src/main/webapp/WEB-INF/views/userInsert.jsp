<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<html>
<head>
	<title>Develop</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//캡챠 이미지 가져오기
	getCaptchaUrl();
	
	//가입 버튼 클릭시
	$("#insert_btn").click(function(){
		
		var userId = $("#joinId").val();
		var userNm = $("#joinName").val();
		var userEmail = $("#joinEmail").val();
		var userCellNum = $("#joinCellNum").val();
		var userPass = $("input[name=joinPass]").val();
		var captchaText = $("#captchaText").val();
		
		var params = {
				userId		:	userId,
				userNm		:	userNm,
				userEmail	:	userEmail,
				userCellNum	:	userCellNum,
				userPass	:	userPass,
				captchaText	:	captchaText
		};
		
		if(joinCheck(params)){
			var url = contextRoot + "/user/userInsertAjax";
			fn_ajax(url, params, joinCheckSuc);
		}
	});
	
	//취소 버튼 클릭시
	$("#cancel_btn").click(function(){
		parent.closeLayerPopup();
	});
});

//캡챠 이미지 가져오기
function getCaptchaUrl(){
	var url = contextRoot + "/api/captchaAjax";
	fn_ajax(url, "", function(data){
		$("#captchaUrl").attr("src", data.result);
	});
}

function joinCheck(data){
	
	if(data.userId == null || data.userId == ""){
		alert("아이디를 입력하세요.");
		$("#joinId").focus();
		return false;
	}else if(data.userNm == null || data.userNm == ""){
		alert("이름을 입력하세요.");
		$("#joinName").focus();
		return false;
	}else if(!($.isNumeric(data.userCellNum)) && data.userCellNum !== ""){
		alert("숫자만 입력하세요.");
		$("#joinCellNum").focus();
		return false;
	}
	var pass = "";
	var exit= false;
	$("input[name=joinPass]").each(function(idx){
		if($(this).val() == null || $(this).val() == ""){
			alert("비밀번호를 입력하세요.");
			$(this).focus();
			exit = true;
			return false;
		}else if(idx !=0 ){
			if(pass != $(this).val()){
				alert("비밀번호가 일치하지 않습니다.");
				$(this).focus();
				exit = true;
				return false;
			}
		}
		pass = $(this).val();
	});
	if(exit){ return false;} 
	if(data.captchaText == null || data.captchaText == ""){
		alert("자동가입방지 문자를 입력하세요.");
		$("#captchaText").focus();
		return false;
	}
	return true;
}

function joinCheckSuc(data){
	var result = data.result;
	if(result == "Y"){
		alert("가입 완료 되었습니다.");
		parent.closeLayerPopup();
	}else if(result == "C"){
		alert("자동가입방지 문자가 일치하지않습니다.");
		getCaptchaUrl();
		$("#captchaText").focus();
		return;
	}else if(result == "D"){
		alert("사용 중인 아이디입니다.");
		getCaptchaUrl();
		$("#joinId").focus();
		return;
	}else{
		alert("가입 실패");
		getCaptchaUrl();
		return;
	}
}
</script>
</head>
<body style="overflow: hidden;">
	<div class="row">
    	<div class="panel panel-default">
        	<div class="panel-heading">
                Sign-Up
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive" style="overflow: hidden;">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
							<td width="30%">user id</td>
							<td><input type="text" id="joinId" size="28"></td>
						</tr>
						<tr>
							<td width="30%">user name</td>
							<td><input type="text" id="joinName" size="28"></td>
						</tr>
						<tr>
							<td>eamil</td>
							<td><input type="text" id="joinEmail" size="28"></td>
						</tr>
						<tr>
							<td>phone number</td>
							<td><input type="text" id="joinCellNum" size="28"></td>
						</tr>
						<tr>
							<td>password</td>
							<td><input type="password" name="joinPass" size="28"></td>
						</tr>
						<tr>
							<td>password confirmation</td>
							<td><input type="password" name="joinPass" size="28"></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center;">
								<p><img id="captchaUrl" src=""></p>
								<p><input type="text" id="captchaText" style="margin-left: 10px;"></p>
							</td>
						</tr>
						<tr style="border: 0" height="30">
							<td colspan="4" align="center">
								<a href="javascript:;" id="insert_btn" class="btn btn-outline btn-primary">가입</a>
								<a href="javascript:;" id="cancel_btn" class="btn btn-outline btn-info">취소</a>
							</td>
						</tr>
					</table>
				</div>
	           <!-- /.table-responsive -->
			</div>
	        <!-- /.panel-body -->
		</div>
	   	<!-- /.panel -->
	</div>
    <!-- /.row -->
</body>
</html>
