<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<jsp:useBean id="commonUtil" class="com.study.common.CommonUtil" scope="page" />
<html>
<head>
	<title>Chatting</title>
<script type="text/javascript">
$(document).ready(function(){
	$("#btn-chat").click(function(){
		send();
	});
});

// var webSocket = new WebSocket('ws://127.0.0.1:8080/controller/broadcasting');
var webSocket = new WebSocket('ws://58.229.163.39:8080/spring-dev-study/broadcasting');
webSocket.onerror = function(event) {
	onError(event)
};
webSocket.onopen = function(event) {
	onOpen(event)
};
webSocket.onmessage = function(event) {
	onMessage(event)
};

function onMessage(event) {
	console.log(event);
// 	$("#messageWindow").scrollTop($("#messageWindow")[0].scrollHeight);
// 	$("#messageWindow").append("상대 : "+event.data + "<br>");

	var data = $.parseJSON(event.data);
	
	var trArray = [];
	var temp = 0;

	trArray[temp++] = '<li class="left clearfix">';
	trArray[temp++] = '	<span class="chat-img pull-left">';
	trArray[temp++] = '		<img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">';
	trArray[temp++] = '	</span>';
	trArray[temp++] = '	<div class="chat-body clearfix">';
	trArray[temp++] = '		<div class="header">';
	trArray[temp++] = '			<strong class="primary-font"> '+data.user+' </strong>';
	trArray[temp++] = '				<small class="pull-right text-muted">';
	trArray[temp++] = '					<i class="fa fa-clock-o fa-fw"></i> '+data.date+'';
	trArray[temp++] = '				</small>';
	trArray[temp++] = '		</div>';
	trArray[temp++] = '	<p>';
	trArray[temp++] = '		'+data.msg+'';
	trArray[temp++] = '	</p>';
	trArray[temp++] = '	</div>';
	trArray[temp++] = '</li>';
	
	$("#messageWindow").append(trArray.join(''));
	
	$("#panel-body").scrollTop($("#panel-body")[0].scrollHeight);
}

function onOpen(event) {
// 	$("#messageWindow").append("연결 성공<br>");
	console.log("연결 성공");
}

function onError(event) {
	alert(event.data);
}

function send() {
// 	$("#messageWindow").append("나 : "+ $("#btn-input").val() + "<br>");
	
	var trArray = [];
	var temp = 0;
	
	var now = new Date();  
	var nowTime = now.getFullYear() + "년 " 
				+ (now.getMonth()+1) + "월 " 
				+ now.getDate() + "일 " 
				+ now.getHours() + "시 " 
				+ now.getMinutes() + "분 " 
				+ now.getSeconds() + "초";
	
	
	trArray[temp++] = '<li class="right clearfix">';
	trArray[temp++] = '	<span class="chat-img pull-right">';
	trArray[temp++] = '		<img src="http://placehold.it/50/FA6F57/fff" alt="User Avatar" class="img-circle">';
	trArray[temp++] = '	</span>';
	trArray[temp++] = '	<div class="chat-body clearfix">';
	trArray[temp++] = '		<div class="header">';
	trArray[temp++] = '			<small class=" text-muted">';
	trArray[temp++] = '				<i class="fa fa-clock-o fa-fw"></i> '+nowTime+'</small>';
	trArray[temp++] = '			<strong class="pull-right primary-font"> ${commonUtil.getLoginUser().getUserId()} </strong>';
	trArray[temp++] = '		</div>';
	trArray[temp++] = '		<p>';
	trArray[temp++] = '			'+$("#btn-input").val()+'';
	trArray[temp++] = '		</p>';
	trArray[temp++] = '	</div>';
	trArray[temp++] = '</li>';
	                
	$("#messageWindow").append(trArray.join(''));

	var obj = {
			"user" : '${commonUtil.getLoginUser().getUserId()}',
			"date" : nowTime,
			"msg" : $("#btn-input").val()
	}
// 	webSocket.send('${commonUtil.getLoginUser().getUserId()}',$("#btn-input").val());
	var jsonString = JSON.stringify(obj);
	webSocket.send(jsonString);
	$("#btn-input").val("");
	
	$("#panel-body").scrollTop($("#panel-body")[0].scrollHeight);
}

function refreshChat(){
// 	var currentLocation = window.location;
// 	$("#messageWindow").fadeOut('slow').load(currentLocation + ' #messageWindow').fadeIn("slow");
	$("#messageWindow").load(location.href + " #messageWindow>*", "");
}

function signOutChat(){
// 	refreshChat();
	$("#messageWindow").append("접속 종료");
	$("#btn-dropdown").attr("disabled",true);
	$("#btn-input").attr("disabled",true);
	$("#btn-chat").attr("disabled",true);
}
</script>
</head>
<body>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Chatting</h1>
            </div>
        </div>
        
        <div class="chat-panel panel panel-default">
	        <div class="panel-heading">
	            <i class="fa fa-comments fa-fw"></i> Chat
	            <div class="btn-group pull-right">
	                <button type="button" id="btn-dropdown" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
	                    <i class="fa fa-chevron-down"></i>
	                </button>
	                <ul class="dropdown-menu slidedown">
	                    <li>
	                        <a href="javascript:refreshChat();">
	                            <i class="fa fa-refresh fa-fw"></i> Refresh
	                        </a>
	                    </li>
<!-- 	                    <li> -->
<!-- 	                        <a href="#"> -->
<!-- 	                            <i class="fa fa-check-circle fa-fw"></i> Available -->
<!-- 	                        </a> -->
<!-- 	                    </li> -->
<!-- 	                    <li> -->
<!-- 	                        <a href="#"> -->
<!-- 	                            <i class="fa fa-times fa-fw"></i> Busy -->
<!-- 	                        </a> -->
<!-- 	                    </li> -->
<!-- 	                    <li> -->
<!-- 	                        <a href="#"> -->
<!-- 	                            <i class="fa fa-clock-o fa-fw"></i> Away -->
<!-- 	                        </a> -->
<!-- 	                    </li> -->
	                    <li class="divider"></li>
	                    <li>
	                        <a href="javascript:signOutChat();">
	                            <i class="fa fa-sign-out fa-fw"></i> Sign Out
	                        </a>
	                    </li>
	                </ul>
	            </div>
	        </div>
	        <div class="panel-body" id="panel-body">
	        	<ul class="chat" id="messageWindow"></ul>
	        </div>
	        <div class="panel-footer">
	            <div class="input-group">
	                <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
	                <span class="input-group-btn">
	                    <button class="btn btn-warning btn-sm" id="btn-chat">
	                        Send
	                    </button>
	                </span>
	            </div>
	        </div>
	    </div>
	    
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>