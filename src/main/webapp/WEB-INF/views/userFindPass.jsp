<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Blank</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//확인 버튼 클릭시
	$("#confirm_btn").click(function(){
		if(passCheck()){
			var params = {
					userEmail	:	$("#userEmail").val()
			}
			var url = contextRoot + "/user/userFindPassAjax";
			fn_ajax(url, params, emailChkSuc);
		}
	});
	
	//취소 버튼 클릭시
	$("[name=cancel_btn]").click(function(){
		parent.closeLayerPopup();
	});
});

function passCheck(){
	if($("#userEmail").val() == null || $("#userEmail").val() == ""){
		alert("이메일을 입력하세요.");
		$("#userEmail").focus();
		return false;
	}
	return true;
}

function emailChkSuc(data){
	var result = data.result;
	if(result == "Y"){
		alert("인증번호를 발송하였습니다.");
	}else{
		alert("인증번호 발송 실패");
		return;
	}
}
</script>
</head>
<body>
	<div class="row" id="defaultView">
       	<div class="col-lg-4" style="width: 370px;">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                 Reset Password
	            </div>
	            <div class="panel-body" style="text-align: left;">
	                <p><h4><b>Email</b></h4>(가입정보 이메일과 일치 해야합니다.)</p>
	                <p>
	                	<input type="text" id="userEmail">&nbsp;
	                	<a href="javascript:;" id="confirm_btn" class="btn btn-primary">확인</a>
						<a href="javascript:;" name="cancel_btn" class="btn btn-outline btn-default">취소</a>
					</p>
	            </div>
	            <!-- /.panel-body -->
	        </div>
	        <!-- /.panel -->
	    </div>
	   	<!-- /.panel -->
	</div>
</body>
</html>
