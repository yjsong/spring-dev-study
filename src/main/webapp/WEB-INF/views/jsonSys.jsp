<%@page contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Map"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="org.codehaus.jackson.JsonFactory"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.BufferedOutputStream"%>
<%
	out.clear();

 	ObjectMapper om = new ObjectMapper();
 	Map<String, String> r = (Map<String, String>)request.getAttribute("xResult");
 	String nm = (String)request.getAttribute("xName");
 	String rJson = om.writeValueAsString(r);
 	
 	out.write(nm + "("+rJson+")");
 	out.flush();

// 	out.clear();
// 	out = pageContext.pushBody();
	
// 	ObjectMapper om = new ObjectMapper();
	
// 	byte[] r = om.writeValueAsBytes(request.getAttribute("xResult"));
// 	response.setContentLength(r.length);
	
// 	BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
// 	bos.write(r);
// 	bos.flush();
// 	bos.close();
%>