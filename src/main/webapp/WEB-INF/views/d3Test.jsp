<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <link href="${pageContext.request.contextPath}/resources/c3/css/c3.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div id="chart"></div>

    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/resources/c3/js/c3.js"></script>
    <script>
      var chart = c3.generate({
        data: {
        	x: "date",
          columns: [
			["date", "2017-07-05", "2017-07-06","2017-07-07","2017-07-08","2017-07-09", "2017-07-10"],
            ['members', 18, 18, 18, 18, 18, 19],
            ['visit', 15, 20, 10, 40, 15, 3]
          ],
          axes: {
            data1: 'y',
          }
        },
        axis: {
          x: {
//             label: 'X'
        	  type: "category"
          },
          y: {
            label: {
              text: 'Y Axis Label',
              position: 'outer-middle'
            }
          },
        },
        tooltip: {
//          enabled: false
        },
        zoom: {
//          enabled: true
        },
        subchart: {
//          show: true
        }
      });


    </script>
  </body>
</html>
