<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Profile</title>
<script type="text/javascript">
$(document).ready(function() {
	
	//변경 버튼 클릭시
	$("#modify_btn").click(function(){
		
		var userKey = $("#userKey").val();
		var userNm = $("#userNm").val();
		var userEmail = $("#userEmail").val();
		var userCellNum = $("#userCellNum").val();
		var userPass = $("input[name=userPass]").val();
		
		var params = {
				userKey		:	userKey,
				userNm		:	userNm,
				userEmail	:	userEmail,
				userCellNum	:	userCellNum,
				userPass	:	userPass
		};
		
		if(updateCheck(params)){
			var url = contextRoot + "/user/userInsertAjax";
			fn_ajax(url, params, updateCheckSuc);
		}
		
	});
});

function updateCheck(data){
	if(data.userNm == null || data.userNm == ""){
		alert("이름을 입력하세요.");
		$("#userNm").focus();
		return false;
	}else if(!($.isNumeric(data.userCellNum)) && data.userCellNum !== ""){
		alert("숫자만 입력하세요.");
		$("#userCellNum").focus();
		return false;
	}
	var pass = "";
	var exit= false;
	$("input[name=userPass]").each(function(idx){
		if($(this).val() == null || $(this).val() == ""){
			alert("비밀번호를 입력하세요.");
			$(this).focus();
			exit = true;
			return false;
		}else if(idx !=0 ){
			if(pass != $(this).val()){
				alert("비밀번호가 일치하지 않습니다.");
				$(this).focus();
				exit = true;
				return false;
			}
		}
		pass = $(this).val();
	});
	if(exit){ return false;} 
	return true;
}

function updateCheckSuc(data){
	var result = data.result;
	if(result == "Y"){
		alert("변경 완료 되었습니다.");
		location.reload();
	}else{
		alert("변경 실패");
		return;
	}
}
</script>
</head>
<body>
<input type="hidden" id="userKey" value="${userInfo.userKey}">
	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">User Profile</h1>
				</div>
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">User Profile</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<tr>
										<td width="30%">user id</td>
										<td width="">
											${userInfo.userId}
											<c:if test="${userInfo.snsType eq 'N'}">
											<span><span class="logo_naver"></span>네이버 아이디로 로그인 중입니다.</span>
											</c:if>
											<c:if test="${userInfo.snsType eq 'K'}">
											<span><span class="logo_kakao"></span>카카오 아이디로 로그인 중입니다.</span>
											</c:if>
											<c:if test="${userInfo.snsType eq 'F'}">
											<span><span class="logo_facebook"></span>페이스북 아이디로 로그인 중입니다.</span>
											</c:if>
											<c:if test="${userInfo.snsType eq 'G'}">
											<span><span class="logo_google"></span>구글 아이디로 로그인 중입니다.</span>
											</c:if>
										</td>
									</tr>
									<tr>
										<td>user name</td>
										<td><input type="text" id="userNm" value="${userInfo.userNm}"></td>
									</tr>
									<tr>
										<td>email</td>
										<td><input type="text" id="userEmail" value="${userInfo.userEmail}"></td>
									</tr>
									<tr>
										<td>phone number</td>
										<td><input type="text" id="userCellNum" value="${userInfo.userCellNum}"></td>
									</tr>
									<tr>
										<td>sign-up date</td>
										<td>${userInfo.joinDate}</td>
									</tr>
									<tr>
										<td>password</td>
										<td><input type="password" name="userPass" size="30"></td>
									</tr>
									<tr>
										<td>password confirmation</td>
										<td><input type="password" name="userPass" size="30"></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><a href="javascript:;"
											id="modify_btn" class="btn btn-outline btn-primary">변경</a> <a
											href="javascript:history.go(-1);" id="cancel_btn"
											class="btn btn-outline btn-info">취소</a></td>
									</tr>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#page-wrapper -->
</body>
</html>
