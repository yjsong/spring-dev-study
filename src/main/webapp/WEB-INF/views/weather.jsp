<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Weather</title>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#weather_btn").click(function(){
		var params = {
				city	:	$("#city").val(),
				county	:	$("#county").val(),
				village	:	$("#village").val()
		};
		
		if(weatherCheck(params)){
			var url = contextRoot + "/weahter/skplanetxWeatherInfoAjax";
			fn_ajax(url, params, setWeatherInfo);
		}

	});
	
	$("#weather_btn").trigger("click");
	
});

function weatherCheck(data){
	if(data.city == null || data.city == ""){
		alert("시(특별, 광역), 도를 입력하세요.");
		$("#city").focus();
		return false;
	}else if(data.county == null || data.county == ""){
		alert("시, 군, 구를 입력하세요.");
		$("#county").focus();
		return false;
	}else if(data.village == null || data.village == ""){
		alert("읍, 면, 동을 입력하세요.");
		$("#village").focus();
		return false;
	}
	return true;
}

function setWeatherInfo(data){
	
	var jsonData = JSON.parse(JSON.stringify(data));
	
	var result = jsonData.result;
	
	if(result == "Y"){
		var timeRelease = jsonData.weather.hourly[0].timeRelease;
		var tname = jsonData.weather.hourly[0].sky.name;
		var tc = jsonData.weather.hourly[0].temperature.tc;
		var tmin = jsonData.weather.hourly[0].temperature.tmin;
		var tmax = jsonData.weather.hourly[0].temperature.tmax;
		var tcode = jsonData.weather.hourly[0].sky.code;

		$("#timeRelease").html("발표 시간 : "+timeRelease);
		$("#tname").html("날씨 상태 : "+tname);
		$("#tc").html("현재 기온 : "+tc);
		$("#tmin").html("오늘 최저 기온 : "+tmin);
		$("#tmax").html("오늘 최고 기온 : "+tmax);
		
		for(var i=1; i<15; i++){
			
			if(tcode.charAt(tcode.length-1) == i){
				$("#tcode").attr("src", "${pageContext.request.contextPath}/resources/icon/"+tcode+".png");
				//${pageContext.request.contextPath}/resources/icon/Cloudy.png
				break;
			}
			
		}
	}else{
		alert("날씨 정보가 없습니다. 지역을 확인 해주세요.");
		$("#timeRelease").empty();
		$("#tname").empty();
		$("#tc").empty();
		$("#tmin").empty();
		$("#tmax").empty();
		$("#tcode").attr("src", "");
	}
	
}
</script>
</head>
<body>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Weather</h1>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <div class="col-lg-6" style="width: 60%;">
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            skplanetx Weather Table
	        </div>
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	            <div class="table-responsive">
	                <table class="table table-striped table-bordered table-hover">
	                    <tbody>
	                        <tr>
	                            <td>시(특별, 광역), 도</td>
	                            <td><input type="text" id="city" style="width: 100%;" autofocus onkeydown="javascript:if(event.keyCode==13){$('#weather_btn').trigger('click');}" value="서울"></td>
	                            <td rowspan="3" align="center" style="vertical-align: middle;">
	                            	<button id="weather_btn" type="button" class="btn btn-primary btn-lg" style="width:100%;">검색</button>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>시, 군, 구</td>
	                            <td><input type="text" id="county" style="width: 100%;" onkeydown="javascript:if(event.keyCode==13){$('#weather_btn').trigger('click');}" value="중구"></td>
	                        </tr>
	                        <tr>
	                            <td>읍, 면, 동</td>
	                            <td><input type="text" id="village" style="width: 100%;" onkeydown="javascript:if(event.keyCode==13){$('#weather_btn').trigger('click');}" value="충무로3가"></td>
	                        </tr>
	                        <tr>
	                            <td>현재 날씨 상태</td>
	                            <td colspan="2">
	                            	<img id="tcode" src="">
									<div id="timeRelease"></div>
									<div id="tname"></div>
									<div id="tc"></div>
									<div id="tmin"></div>
									<div id="tmax"></div>
	                            </td>
	                        </tr>
	                    </tbody>
	                </table>
	            </div>
	            <!-- /.table-responsive -->
	        </div>
	        <!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
	</div>
   	
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>
