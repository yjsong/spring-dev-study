<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<h1>spring-dev-study</h1>
<h3><a href="http://yjsong.ze.am:8080/spring-dev-study/home">Portfolio Link(Click)</a></h3>
<li>0. 개발환경
<ul>
<li>Ubuntu 16.04</li>
<li>java 1.8</li>
<li>Tomcat 7.x</li>
<li>Spring 4.3.1.RELEASE</li>
<li>Maven 3.3.9</li>
<li>Mysql 5.7.18</li>
</ul>
<li><a href="#1-login">1. Login</a>
<ul>
<li><a href="#1-1-spring-security">1-1. spring security</a></li>
<li><a href="#1-2-smtp">1-2. SMTP(google, naver)</a></li>
<li><a href="#1-3-captcha">1-3. CAPTCHA(naver)</a></li>
<li><a href="#1-4-social-login">1-4. social login(kakao, naver, facebook, google)</a></li>
<li><a href="#1-5-cookie">1-5. cookie(Remember ID)</a></li>
</ul>
</li>
<br>
<li><a href="#2-dashboardhome">2. DashBoard(Home)</a>
<ul>
<li><a href="#2-1-tiles">2-1. tiles</a></li>
<li><a href="#2-2-c3jsd3js">2-2. c3.js(d3.js)</a></li>
<li><a href="#2-3-soundjs">2-3. soundjs</a></li>
<li><a href="#2-4-visitor-counter">2-4. visitor counter(HttpSessionListener)</a></li>
</ul>
</li>
<br>
<li><a href="#3-menu">3. Menu</a>
<ul>
<li><a href="#3-1-mapgoogle-naver">3-1. map(google, naver)</a></li>
<li><a href="#3-2-weatherskplanetx">3-2. weather(skplanetx)</a></li>
<li><a href="#3-3-server-state-check">3-3. server state check(Thread)</a></li>
<li><a href="#3-4-board">3-4. board(CRUD, search, paging)</a></li>
<li><a href="#3-5-chattingwebsocket">3-5. chatting(websocket)</a></li>
</ul>
</li>

<h2>1. Login</h2>
<p>로그인 관련 코드 설명</p>
<h3>1-1. spring security</h3>
<ul>
<li><h4>사용자 인증 및 권한 확인 후 리소스 접근 허용</h4></li>
<li>공식 사이트 : <a href="http://projects.spring.io/spring-security">http://projects.spring.io/spring-security</a></li>
<li>login.jsp, security-context.xml, com.study.security package 소스 참고</li>
</ul>
<h3>1-2. SMTP</h3>
<ul>
<li><h4>이메일 가입 여부 체크 후 비밀번호 초기화 인증 메일 발송</h4></li>
<li>SMTP(Simple mail Transfer protocol)메시지를 교환하기 위한 프로토콜 규약</li>
<li>UserServiceImpl.java 소스 참고(google, naver SMTP 사용)</li>
</ul>
<h3>1-3. CAPTCHA</h3>
<ul>
<li><h4>회원가입시 자동가입방지를 위한 테스트</h4></li>
<li>
	CAPTCHA(Completely Automated Public Turing test to tell Computers and Humans Apart)<br>
	웹사이트에서 사람이 접근하려고 하는 것인지 봇이 접근하는 것인지 판단하기 위하여 사용되는 테스트
</li>
<li>ApiServiceImpl.java 소스 참고(naver API 사용)</li>
</ul>
<h3>1-4. social login</h3>
<ul>
<li><h4>별도의 회원가입 없이 카카오, 네이버, 페이스북, 구글 계정으로 로그인</h4></li>
<li>카카오 : <a href="https://developers.kakao.com/docs/js/kakaologin">https://developers.kakao.com/docs/js/kakaologin</a></li>
<li>네이버 : <a href="https://developers.naver.com/docs/login/api">https://developers.naver.com/docs/login/api</a></li>
<li>페이스북 : <a href="https://developers.facebook.com/docs/facebook-login/web">https://developers.facebook.com/docs/facebook-login/web</a></li>
<li>구글 : <a href="https://console.developers.google.com/apis/library">https://console.developers.google.com/apis/library</a></li>
<li>배달통(스크립트 소스 참고) : <a href="https://www.bdtong.co.kr/member/member_login.php">https://www.bdtong.co.kr/member/member_login.php</a></li>
<li>login.jsp 소스 참고</li>
</ul>
<h3>1-5. cookie</h3>
<ul>
<li><h4>Remember Me 체크시 로그인하는 아이디 저장</h4></li>
<li>cookie : 브라우저가 사용자의 컴퓨터에 저장한 데이터, 웹사이트를 방문할 때마다 읽히고 수시로 새로운 정보로 바뀜</li>
<li>login.jsp 소스 참고</li>
</ul>
<br>
<h2>2. DashBoard(Home)</h2>
<p>대시보드 관련 코드 설명</p>
<h3>2-1. tiles</h3>
<ul>
<li><h4>반복적으로 사용하는 상단, 하단, 메뉴 부분을 tiles를 사용하여 구성</h4></li>
<li>tiles : 페이지 레이아웃을 위한 아파치 프레임워크로 여러개의 View를 조합하여 하나의 화면을 구성</li>
<li>tiles.xml, layout 하위 파일 소스 참고</li>
</ul>
<h3>2-2. c3.js(d3.js)</h3>
<ul>
<li><h4>일일 방문자 수와 총 가입자 수를 차트로 표현</h4></li>
<li>c3.js : d3.js(Data Drivened Document 데이터 시각화 프레임워크)를 이용한 차트 라이브러리</li>
<li>home.jsp 소스 참고</li>
</ul>
<h3>2-3. soundjs</h3>
<ul>
<li><h4>홈 화면 사운드 플레이어 재생</h4></li>
<li>home.jsp 소스 참고</li>
</ul>
<h3>2-4. visitor counter</h3>
<ul>
<li><h4>최초 세션 접속시 방문자 카운터</h4></li>
<li>HttpSessionListener를 구현(Implements)하여 세션이 생성 되는 시점과 사라지는 시점을 감지</li>
<li>SessionListener.java 소스 참고</li>
</ul>
<br>
<h2>3. Menu</h2>
<p>메뉴 관련 코드 설명</p>
<h3>3-1. map(google, naver)</h3>
<ul>
<li><h4>원하는 위치를 지도에 마커 표시(추가 업데이트 예정)</h4></li>
<li>네이버 : <a href="https://navermaps.github.io/maps.js/docs/tutorial-0-Getting-Started.html">https://navermaps.github.io/maps.js/docs/tutorial-0-Getting-Started.html</a></li>
<li>구글 : <a href="https://developers.google.com/maps/documentation/javascript/tutorial?hl=ko">https://developers.google.com/maps/documentation/javascript/tutorial?hl=ko</a></li>
<li>map.jsp 소스 참고</li>
</ul>
<h3>3-2. weather(skplanetx)</h3>
<ul>
<li><h4>검색하는 위치의 날씨 상태 제공(city, county, village 입력)</h4></li>
<li>skplanetx : <a href="https://developers.skplanetx.com/apidoc/kor/weather/information/">https://developers.skplanetx.com/apidoc/kor/weather/information</a></li>
<li>weather.jsp 소스 참고</li>
</ul>
<h3>3-3. server state check</h3>
<ul>
<li><h4>10초 주기로 context.properties에 입력된 서버 상태 체크(Thread)</h4></li>
<li>state.jsp, sysInfo.jsp 소스 참고</li>
</ul>
<h3>3-4. board</h3>
<ul>
<li><h4>게시판 CRUD, 검색, 페이징, 비밀글 기능(추가 업데이트 예정)</h4></li>
<li>developBoardList.jsp, developBoardView.jsp 소스 참고</li>
</ul>
<h3>3-5. chatting(websocket)</h3>
<ul>
<li><h4>접속 사용자 간 실시간 멀티 채팅</h4></li>
<li>websocket : 웹 브라우저와 웹 서버 간의 양방향 통신을 지원하기 위한 표준, 클라이언트와 서버가 실시간으로 메시지를 주고 받음</li>
<li>chatting.jsp, Broadsocket.java 소스 참고</li>
</ul>