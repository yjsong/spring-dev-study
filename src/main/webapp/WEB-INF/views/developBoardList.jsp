<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Develop</title>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	//시작일 달력 셋팅
	$("#startDate").datepicker({ dateFormat: 'yy-mm-dd' });
	//종료일 달력 셋팅
	$("#endDate").datepicker({ dateFormat: 'yy-mm-dd' });
	
	//시작일, 종료일 디폴트 : 접속한 날짜 기준
// 	$('#startDate').val($.datepicker.formatDate('yy-mm-dd', new Date()));
// 	$('#endDate').val($.datepicker.formatDate('yy-mm-dd', new Date()));
	
	//게시판 목록 조회
	goBoardList(1);
	
	//게시판 목록 조회
	$("#search_btn").click(function(){
		
		//날짜 형식 체크
// 		if(dateCheck()){
			goBoardList(1);
// 		}
	});
	
	//글쓰기 버튼 클릭시
	$("#write_btn").click(function(){
		var url = contextRoot + "/board/developBoardView";
		//수정 팝업 open
		openLayerPopup(url);
	});
	
	//일괄 등록 버튼 클릭시
	$("#excelWrite_btn").click(function(){
		$("#file").trigger("click");
	});
});

function dateCheck(){
	
	//디폴트 : 접속 날짜 (오늘), 입력 x : 전체 조회
	var startDate = $("#startDate").val().trim();
	var endDate = $("#endDate").val().trim();
	
	if(startDate == null || startDate == ""){
		alert("시작일을 입력하세요.");
		$("#startDate").focus();
		return false;
	}else if(endDate == null || endDate == ""){
		alert("종료일을 입력하세요.");
		$("#endDate").focus();
		return false;
	}
	return true;

}

//게시판 목록 조회
function goBoardList(page){
	
	var startDate = $("#startDate").val().trim();
	var endDate = $("#endDate").val().trim();
	
	var searchItem = $("#searchItem").val(); 			//검색항목
	var searchValue = $("#searchValue").val();		   //검색어
	
	var url = contextRoot + "/board/developBoardListAjax";
	var params = {
			page : page,
			rec : 10,
			startDate	:	startDate,
			endDate		:	endDate,
			searchItem : searchItem,
			searchValue : searchValue
	};
	
	fn_ajax(url, params, setBoardList);
}

function setBoardList(data){
	
	var page = data.page;
	var rec = data.rec;
	var records = data.records;
	
	var trArray = [];
	var temp = 0;
	var tempList = data.developBoardList;
	
// 	$("#recordCnt").html("총 "+records+"개");
	
	$("#boardList").empty();
	$("#pagenation").empty();
	
	if(tempList != null && tempList.length > 0){
		
		for(var i=0; i<tempList.length; i++){
			
	        trArray[temp++] = "<tr class='odd gradeX'>";
	        trArray[temp++] = "		<td>"+tempList[i].rnum+"</td>";
	        
	        if(tempList[i].boardPwd != null && tempList[i].boardPwd != ""){
	        	trArray[temp++] = "		<td><p class='fa fa-lock'> <a href='javascript:goView(\"" + tempList[i].boardNo + "\")'>" + tempList[i].boardTitle + "</a></p></td>";
	        }else{
		        trArray[temp++] = "		<td><a href='javascript:goView(\"" + tempList[i].boardNo + "\")'>" + tempList[i].boardTitle + "</a></td>";
	        }
	        trArray[temp++] = "		<td>"+tempList[i].updateUserId+"</td>";
	        trArray[temp++] = "		<td>"+tempList[i].updateDate+"</td>";
	        trArray[temp++] = "		<td>"+tempList[i].readCount+"</td>";
	        trArray[temp++] = "</tr>";
		}
		
		setPageNav({
			dataCount : records,								// 총 데이터 건 수
			pageNo : page,										// 현재 페이지 번호
			pageSize : rec,										// 한 페이지당 출력할 데이터 건 수
			pageGroupSize : 10,									// 페이지 번호 표시 범위
			pageNavList : $("#pagenation"),						// 페이지 네비게이션 표시할 DOM Element
			makePageLink : function(p_pageNo) {					// Page Link 문자열 구성할 function
				return "javascript:goBoardList(" + p_pageNo + ");";
			}
		});
		
	}else{
		trArray[temp++] = "<tr>";
		trArray[temp++] = "<td colspan='5' align='center'>데이터가 없습니다.</td>";
		trArray[temp++] = "</tr>";
	}
	$("#boardList").append(trArray.join(''));
	
	
	//bootstrap table(show entries, search, paging)
// 	$('#dataTables-example').DataTable({
// 		responsive: true
// 	});
	
}

function goView(boardNo){
	var url = contextRoot + "/board/developBoardView?boardNo="+boardNo;
	//수정 팝업 open
	openLayerPopup(url);
}

function getExcelForm(){
	var file = $('#file');
// 	alert("file > "+ $('#file').val().replace("C:\\fakepath\\", ""));
	uploadExcel(file, "old");
	file.val("");
}

</script>
</head>
<body>
 <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Develop Table</h1>
            </div>
        </div>
        <div class="row">
	        <div class="col-lg-6">
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                	develop table(게시판 CRUD, 검색, 페이징, 비밀글 기능 구현)
<!-- 	                    develop table(게시판 CRUD, 검색, 페이징, 답변 기능 구현) -->
	                </div>
	                <!-- /.panel-heading -->
<!-- 	                <h4 id="recordCnt"></h4> -->
					<div style="padding-left:25px; margin-top: 10px;">
						<input type="text" placeholder="시작일" id="startDate"/> - 
               			<input type="text" placeholder="종료일" id="endDate"/>
            		</div>
					<div style="margin-left: 10px; margin-top: 10px;" >
						<div class="col-xs-2">
		                	<select id="searchItem" class="form-control">
								<option value="update_user">아이디</option>
								<option value="board_title">제목</option>
								<option value="board_content">내용</option>
							</select>
	              	  	</div>
                		<input type="text" id="searchValue">
                		<a href="javascript:;" id="search_btn" class="btn btn-primary" style="margin-left: 10px;">검색</a>
                		<a href="javascript:;" id="write_btn" class="btn btn-outline btn-default">글쓰기</a>
                		<!-- 
						<a href="javascript:;" id="excelDown_btn" class="btn btn-outline btn-default">템플릿 다운로드</a>
                		<input type="file" size="30" id="file" name="file" style="display:none;" onchange="getExcelForm();" />
                		<a href="javascript:;" id="excelWrite_btn" class="btn btn-outline btn-default">일괄 등록</a>
	                	-->
	                </div>
	                <div class="panel-body">
	                    <div class="table-responsive">
	                        <table class="table table-striped table-bordered table-hover">
	                            <thead>
	                                <tr>
	                                    <th width="5%">No</th>
	                                    <th width="20%">Title</th>
	                                    <th width="10%">Id</th>
	                                    <th width="10%">Date</th>
	                                    <th width="5%">Read</th>
	                                </tr>
	                            </thead>
	                            <tbody id="boardList">
	                            	<!--
	                                <tr>
	                                    <td>1</td>
	                                    <td>Mark</td>
	                                    <td>Otto</td>
	                                    <td>@mdo</td>
	                                </tr>
	                                <tr>
	                                    <td>2</td>
	                                    <td>Jacob</td>
	                                    <td>Thornton</td>
	                                    <td>@fat</td>
	                                </tr>
	                                <tr>
	                                    <td>3</td>
	                                    <td>Larry</td>
	                                    <td>the Bird</td>
	                                    <td>@twitter</td>
	                                </tr>
	                                -->
	                            </tbody>
	                        </table>
	                    </div>
	                    <!-- /.table-responsive -->
<!-- 	                    <div id="pagenation" style="text-align: center;"></div> -->
	                    <nav style="text-align:center;" >
						  <ul class="pagination" id="pagenation">
<!-- 						    <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li> -->
<!-- 						    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li> -->
						  </ul>
						</nav>
	                </div>
	                <!-- /.panel-body -->
	            </div>
	            <!-- /.panel -->
	        </div>
	        <!-- /.col-lg-6 -->
	    </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>
