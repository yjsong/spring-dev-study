<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Login</title>
<link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
<!-- onload=render 셋팅을 하지않으면 구글로그인 버튼 두번눌러야함 -->
<script src="https://apis.google.com/js/client:platform.js?onload=renderGoogleSignin" async defer></script>
<script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.2.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	//최초 쿠키에 userId라는 쿠키값이 존재하면
    var userId = $.cookie('userId');
    if(userId != undefined) {
        //아이디에 쿠키값을 담는다
        $("#userId").val(userId);
        //아이디저장 체크박스 체크를 해놓는다
        $("#rememberCheck").prop("checked",true);
    }
    
    //회원가입 버튼 
    $("#join_btn").click(function(){
    	var url = contextRoot + "/user/userInsert";
		//가입 팝업 open
		openLayerPopup(url);
    });
    
    //비밀번호 찾기
    $("#findPass_btn").click(function(){
    	var url = contextRoot + "/user/userFindPass";
		//비밀번호 찾기 팝업 open
		openLayerPopup(url);
    });
    
	//로그인 버튼
	$("#loginButton").click(function (){
		if(loginCheck()){
            if($("#rememberCheck").prop("checked")) {
                $.cookie('userId', $("#userId").val());
            } else {
                $.removeCookie("userId");	//아이디저장 미체크면 쿠키에 정보가 있던간에 삭제
            }
			$("#invoice_form").submit();
		}
	});
	
	//================카카오 로그인 관련 함수 시작================
	/**
	* 카카오 로그인 창을 보여주고 로그인을 진행한다.
	* @param callback - 로그인 성공시 호출될 콜백 함수. 파라미터로 UserInfo 객체를 받아야 한다.
	*/
	$("#btn_kakao").click(function(){
		// 로그인 창을 띄웁니다.
		Kakao.Auth.login({
			success: function(authObj) {				//로그인에 성공할 경우
//	 			login_succeeded('Kakao');
				requestKakaoInfo();						//사용자 정보를 요청한다.
			},
			fail: function(err) {							//로그인에 실패한경우
// 				login_failed('Kakao', 'login error');
				//없어도 된다. 사용자가 창을 닫거나 동의하지 않는 경우가 대부분.
				//아이디, 비번 불일치는 해당 페이지에 표시 된다.
			},
			persistAccessToken : false,				//AccessToken 저장안함. 일반적이 OAuth 인증일 경우 저장 필요함.
			persistRefreshToken : false				//RefreshToken 저장안함. 일반적이 OAuth 인증일 경우 저장 필요함.
		});	
	});
	
	
	/**
	 * 네이버 로그인 API 연동
	 * @returns {boolean}
	 */
	$("#btn_naver").click(function(){
		var sKey = naverApiKey
	        ,loc = location || document.location
	        ,protocol = loc.protocol || 'https:'
	        ,hostname 	= loc.hostname || loc.host
	        ,domain 	= protocol +'//' + hostname
// 	        ,sReturn = encodeURIComponent('http://127.0.0.1:8080/controller/login/naver')
// 	        ,sReturn = encodeURIComponent('http://58.229.163.39:8080/spring-dev-study/login/naver')
			,sReturn = encodeURIComponent('http://yjsong.ze.am:8080/spring-dev-study/login/naver')
	        ,oPopup = openPop('https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id='+sKey+'&redirect_uri='+sReturn);
	    if( null === oPopup )
	    {
	        return false;
	    }
	});
	 
	 $("#btn_facebook").click(function(){
		 FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				// connected
				FBlogin();
			} else if (response.status === 'not_authorized') {
				// not_authorized
				FBlogin();
			} else {
				// not_logged_in
				FBlogin();
			}
		});
	 });
});

function loginCheck(){
	if($("#userId").val() == null || $("#userId").val() == ""){
		alert("아이디를 입력하세요.");
		$("#userId").focus();
		return false;
	}else if($("#userPass").val() == null || $("#userPass").val() == ""){
		alert("비밀번호를 입력하세요.");
		$("#userPass").focus();
		return false;
	}
	return true;
}

/**
 * 사용자 정보 클래스
 */
function UserInfo() {
	this.id;
	this.pw;
	this.name;
	this.gender;
	this.img;
	this.snsCode;
	/*
	this.toJson = function() {
		var json = '{"id":"'+this.id+'", "pw":"'+this.pw+'", "name":"'+this.name+'", "gender":"'+this.gender+'", "img":"'+this.img+'", "snsCode":"'+this.snsCode+'"}';

		return JSON.parse(json);
	}

	this.joinJson = function() {
		var json = '{"m_usrid":"'+this.id+'", "m_password":"'+this.pw+'", "m_name":"'+this.name+'", "gender":"'+this.gender+'", "img_name":"'+this.img+'", "snsCode":"'+this.snsCode+'"}';

		return JSON.parse(json);
	}
	*/
}


/**
 * 카카오 유저의 사용자 정보를 조회한다.
 */
function requestKakaoInfo() {
	Kakao.API.request({
		url: '/v1/user/me',												//요청 종류, 사용자 정보 요청
		success: function(res) {										//사용자 정보 조회에 성공
			returnUserInfo(res, LOGIN_TYPE_KAKAO);						//사용자 정보 반환
		},
		fail: function(error) {
			alert("사용자 정보 조회에 실패했습니다.\n"+"("+error.error_description+")");
		}
	});
}

//================구글 로그인 관련 함수 시작================
/**
 * 페이지가 로딩되고 구글 JS 파일이 로딩되면 구글 로그인 버튼을 초기화한다.
 */
function renderGoogleSignin(gubun) {

	if(gubun != 'B'){
		isGoogleInit = true;
	}
	
	/**구글+ api 키 값*/
	var googleApiKey = "458666073437-jne60nl5ful60e8ab99939mmviobfidf.apps.googleusercontent.com";
	 
	gapi.signin.render('btn_google',				//커스텀 버튼 id 값
		{ 
			'callback': 'googleSigninCallback', 	//로그인 콜백 함수 설정
			'clientid': googleApiKey, 				//api 키 설정
			'immediate': false,						//로그아웃 가능하도록 설정
			'cookiepolicy': 'single_host_origin',	//쿠키 저장 정책. 로그아웃되려면 single_host_origin로 설정
			'requestvisibleactions': 'http://schemas.google.com/AddActivity',
			'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'	//접근 범위, 유저프로필과 이메일
	});
}

/**
 * 구글 계정으로 로그인 결과를 받을 콜백 함수
 */
function googleSigninCallback(authResult) {

	if (authResult['access_token']) {				//로그인 성공
		if(isGoogleInit) {								//버튼 렌더로 인해 자동으로 콜백 불린 경우 자동 로그아웃
			isGoogleInit = false;					//다음 부터는 정상처리 되도록 플래그 변경.
			logout(LOGIN_TYPE_GOOGLE);		//로그아웃 처리
			return;
		}

		googleSigninCallback = authResult['access_token'];
		// 승인 성공
		gapi.client.load('oauth2', 'v2', function() {
			var request = gapi.client.oauth2.userinfo.get();		//사용자 정보 요청
			request.execute(returnGoogleUserInfo);				//사용자 정보 조회 후 콜백함수 설정
		});

	} 
	else if (authResult['error']) {
		if(isGoogleInit) isGoogleInit = false;
		//가능한 오류 코드:
		//"access_denied" - 사용자가 앱에 대한 액세스 거부
		//"immediate_failed" - 사용자가 자동으로 로그인할 수 없음. 승인한적이 없으면 처음에 자동으로 발생
	}
	/*
	// 승인 성공
	gapi.client.load('oauth2', 'v2', function() {
		var request = gapi.client.oauth2.userinfo.get();		//사용자 정보 요청
		request.execute(returnGoogleUserInfo);				//사용자 정보 조회 후 콜백함수 설정
	});
	*/
}

/**
 * 구글 사용자 정보를 콜백함수를 통해서 반환한다.
 * @param res - 사용자 정보 JSON
 */
function returnGoogleUserInfo(res) {
	disconnectUser(googleAccessToken);
	returnUserInfo(res, LOGIN_TYPE_GOOGLE);					//구글 프로필 정보를 배달통에서 필요한 정보만 가공하여 처리
}

function disconnectUser(access_token) {
  var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' +
      access_token;

  // 비동기 GET 요청을 수행합니다.
  $.ajax({
    type: 'GET',
    url: revokeUrl,
    async: false,
    contentType: "application/json",
    dataType: 'jsonp',
    success: function(nullResponse) {
      // 사용자가 연결 해제되었으므로 작업을 수행합니다.
      // 응답은 항상 정의되지 않음입니다.
    },
    error: function(e) {
      // 오류 처리
	  login_failed('Google+', 'login error');
      // console.log(e);
      // 실패한 경우 사용자가 수동으로 연결 해제하게 할 수 있습니다.
      // https://plus.google.com/apps
    }
  });
}

/**
 * 로그아웃 시킨다.
 * @param snsType - SNS 종류, 카카오 / 구글
 */
function logout(snsType) {
	
	if(snsType == LOGIN_TYPE_KAKAO) {
		Kakao.Auth.logout();
	}
	else if(snsType == LOGIN_TYPE_GOOGLE) {
		gapi.auth.signOut();
	}
}
//================구글 로그인 관련 함수 종료================
	
function merge(a1, a2, a3) {
	var o = undefined !== a3 ? a3 : true,
		f;
	for (f in a2)
		if (!a1[f] || o) a1[f] = a2[f];
	return a1
}

function serialize( /* object */ o, key, str) {
	var a = [], str = str || '&';
	for (var p in o) {
		if (o.hasOwnProperty(p)) {
			var v = key ? (key + '[' + p + ']') : p;
			if ('object' === typeof o[p])
				a.push(serialize(o[p], p));
			else
				a.push(encodeURIComponent(v) + '=' + encodeURIComponent(o[p]));
		}
	}
	return a.join(str);
}

function openPop(szUrl, szWindow, oOption) {
	var o = {
			left: 10,
			top: 10,
			width: 400,
			height: 300,
			marginwidth: 0,
			marginheight: 0,
			resizable: 1,
			scrollbars: 'no'
		};

	o = merge(o, oOption || {});

	return window.open(szUrl, szWindow, serialize(o, '', ','));
}

function checkFBLogin() {
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			// connected
			FBlogin();
		} else if (response.status === 'not_authorized') {
			// not_authorized
			FBlogin();
		} else {
			// not_logged_in
			FBlogin();
		}
	});
}

function FBlogin() {
	FB.login(function(response) {
		if (response.authResponse) {
// 			login_succeeded('Facebook');
			// connected	            
			FB.api('/me', function(response) {
				// 1. 현재 회원가입이 된 아이디인지 체크 (회원가입을 확인하는 API호출 with Ajax)
				//location.replace('?c=member&m=memberJoinWithFB&pass='+user_id+'&m_name='+user_name+'&email='+user_email+'&gender='+user_gender+'&m_birth_year='+m_birth_year+'&m_birth_month='+m_birth_month+'&m_sex='+user_gender+'&f_filename='+f_file_name);
				returnUserInfo(response, LOGIN_TYPE_FACEBOOK);
			});

		} 
		else {
// 			login_failed('Facebook', 'login error');
			//로그인 정보 수집 실패 시 처리!!!
		}
	}, { perms:'email,user_birthday'});
}

//================카카오/구글 로그인 공통으로 사용하는 함수 시작================
/**
 * 사용자 정보를 콜백함수를 통해서 반환한다.
 * @param res - 사용자 정보 JSON
 * @param snsType - SNS 종류
 */
function returnUserInfo(res, snsType) {
	var info = new UserInfo();
	info.snsType = snsType;	//snsCode값을 저장
	
	if(snsType == LOGIN_TYPE_KAKAO) {
		info.userId 	= res.id + "@kakao.login";		//이메일 형식의 id 저장
		info.userPass 	= res.id;						//비밀번호는 kakao id값으로
		info.userNm		= res.properties.nickname;		//이름 설정
		info.snsType	= LOGIN_TYPE_KAKAO;
		sessionStorage.setItem('LOGIN_TYPE', LOGIN_TYPE_KAKAO);	//sessionStorage -> js session 값 생성
		
	}else if( snsType == LOGIN_TYPE_NAVER ) {
		info.userId 	= res.id + "@naver.login";
		info.userPass 	= res.id;
		info.userNm 	= res.name;
		info.snsType	= LOGIN_TYPE_NAVER;
		sessionStorage.setItem('LOGIN_TYPE', LOGIN_TYPE_NAVER);
		
	}else if(snsType == LOGIN_TYPE_FACEBOOK) {
		info.userId    = res.id + "@facebook.login";
		info.userPass  = res.id;
		info.userNm    = res.name;
// 		info.gender = res.gender == "male" ? "M" : (res.gender == "female" ? "W":"");
// 		info.img = 'http://graph.facebook.com/'+info.pw+'/picture?type=square';
		info.snsType	= LOGIN_TYPE_FACEBOOK;
		sessionStorage.setItem('LOGIN_TYPE', LOGIN_TYPE_FACEBOOK);
		
	}else if(snsType == LOGIN_TYPE_GOOGLE) {
		info.userId		= res.email;								//이메일 형식의 id 저장
		info.userPass 	= res.id;									//비밀번호는 구글 id값으로
		info.userNm 	= res.name;									//이름 설정
// 		info.gender		= res.gender == "male" ? "M" : (res.gender == "female" ? "W":"");
// 		info.img 		= res.picture;								//이미지 저장
		info.snsType	= LOGIN_TYPE_GOOGLE;
		sessionStorage.setItem('LOGIN_TYPE', LOGIN_TYPE_GOOGLE);
	}

	$("#userId").val(info.userId);
    $("#userPass").val(info.userPass);

    if(info.userId != null && info.userId != ""){
	    var url = contextRoot + "/user/userSnsCheckAjax";
		fn_ajax(url, info, joinCheckSuc);
    }
}


function joinCheckSuc(data){
	var result = data.result;
	if(result == "Y" || result == "D"){
		document.forms["invoice_form"].submit();
	}
}
</script>
</head>
<body>
<form id="invoice_form" name="invoice_form" action="${pageContext.request.contextPath}/loginProcess" method="post">
	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
<!--                         <h3 class="panel-title">Please Sign In</h3> -->
                        <h3 class="panel-title">ID : user  PWD : 1111</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" id="userId" name="userId" type="email" autofocus onkeydown="javascript:if(event.keyCode==13){$('#loginButton').trigger('click');}">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="userPass" name="userPass" type="password" value="" onkeydown="javascript:if(event.keyCode==13){$('#loginButton').trigger('click');}">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" id="rememberCheck" value="Remember Me">Remember Me
                                    </label>
                                    <a href="javascript:;" id="findPass_btn" style="margin-left: 65px;">Forgot your password?</a>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="javascript:;" id="loginButton" class="btn btn-lg btn-warning btn-block">Login</a>
                                <a href="javascript:;" id="join_btn" class="btn btn-lg btn-info btn-block">Sign-Up</a>
                                
                                <!-- 배달통 참고 -->
                                <div class="snsbtnlist">
									<div><a href="javascript:;" id="btn_kakao" class="btn_kakao"></a></div>
									<div><a href="javascript:;" id="btn_naver" class="btn_naver"></a></div>
									<div><a href="javascript:;" id="btn_facebook" class="btn_facebook"></a></div>
									<div id="btn_google" class="customGPlusSignIn">
										<a href="javascript:renderGoogleSignin('B');" class="btn_google"></a>
									</div>
								</div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>
