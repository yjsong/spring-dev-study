<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%-- <p>principal : <sec:authentication property="principal"/></p> --%>
<p>principal.username : <sec:authentication property="principal.userId"/></p>
<%-- <p>principal.password : <sec:authentication property="principal.password"/></p> --%>
<%-- <p>principal.email : <sec:authentication property="principal.email"/></p> --%>
<%-- <p>principal.enabled : <sec:authentication property="principal.enabled"/></p> --%>
<%-- <p>principal.accountNonExpired : <sec:authentication property="principal.accountNonExpired"/></p> --%>
<%-- <sec:authorize url="/user/loginPage" var="t">${t }</sec:authorize>​ --%>
<%-- <sec:authorize access="hasRole('ROLE_USER')" var="u">${u }</sec:authorize> --%>
<%-- <sec:authorize ifnotgranted="hasRole('ROLE_USER')" var="b">${b }</sec:authorize>  --%>
</body>
</html>