<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.RandomAccessFile"%>
<%@page import="java.io.File"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="com.sun.management.OperatingSystemMXBean" %>
<%@page import="java.lang.management.ManagementFactory" %>
<%@page import="org.apache.log4j.Logger"%>
    
<%--hostname, ip --%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>

<%! 
	private String toMB(long size){
		return (int)(size/(1024*1024))+"(MB)"; 
	}
	
	private String toPercent(long usize, long tsize){		
		double useable = usize/(1024*1024);
		double total 	= tsize/(1024*1024);
		
		String pattern = "###.##";
		DecimalFormat dformat = new DecimalFormat(pattern); 
		String percentage  = dformat.format((useable/total)*100);
	
		return percentage+"%";
	}
	
	public static String executeCommand(String[] commandArr){
// 		 System.out.println("Linux command: " + java.util.Arrays.toString(commandArr));
		 	String excute_result = "";
			try {
			  ProcessBuilder pb = new ProcessBuilder(commandArr);
			  pb.redirectErrorStream(true);
			  Process proc = pb.start();
			  BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));             
			
			  String line;             
			  while ((line = in.readLine()) != null) {
			      excute_result = line;
			  }
			
			  proc.destroy();
			  
			} catch (Exception x) {
			  x.printStackTrace();
			}
		return excute_result;
	}
%>

<%
	Logger log = Logger.getLogger(getClass().getSimpleName()); 

	String callback = request.getParameter("callback");
	
 	Map<String, Object> result = new HashMap<String, Object>();
	Map<String, Object> subMap = new HashMap<String, Object>();
	
	File root = null;
	root = new File("/");
	
	subMap.put("total_space", toMB(root.getTotalSpace()));			// 하드디스크 전체 용량
	subMap.put("useable_space", toMB(root.getUsableSpace()));		// 사용가능한 디스크 용량
	subMap.put("useable_disk_percentage", toPercent(root.getUsableSpace(), root.getTotalSpace()));	// 사용 중인 디스크 용량(%)
	
	result.put("space",subMap);
	
	
	OperatingSystemMXBean osbean = ( OperatingSystemMXBean ) ManagementFactory.getOperatingSystemMXBean( );
	
	subMap = new HashMap<String, Object>();
	
	subMap.put("total_memory_size", toMB(osbean.getTotalPhysicalMemorySize()));	// 총 메모리 
	subMap.put("free_memory_size", toMB(osbean.getFreePhysicalMemorySize()));	// 여유 메모리 
	subMap.put("use_memory_percentage", toPercent(osbean.getFreePhysicalMemorySize(),osbean.getTotalPhysicalMemorySize()));	//사용 중인 메모리 용량(%)
	
	result.put("memory", subMap);

   	float cpuUsage = 0;
   	try {
        RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
        String load = reader.readLine();

        String[] toks = load.split(" +");  // Split on one or more spaces

        long before_user = Long.parseLong(toks[1]);
        long before_nice = Long.parseLong(toks[2]);
        long before_system = Long.parseLong(toks[3]);
        long before_idle = Long.parseLong(toks[4]) ;

	    try {
	         Thread.sleep(1000);
	    } catch (Exception e) {}

	    reader.seek(0);
	    load = reader.readLine();
	    reader.close();

	    toks = load.split(" +");

	    long after_user = Long.parseLong(toks[1]);
	    long after_nice = Long.parseLong(toks[2]);
	    long after_system = Long.parseLong(toks[3]);
	    long after_idle = Long.parseLong(toks[4]) ;

	    long user = after_user - before_user;
	    long nice = after_nice - before_nice;
	    long system = after_system - before_system;
	    long idle = after_idle - before_idle;
	    
	    //System.out.println("user : " + user + ", nice : " + nice + ", system : " + system + ", idle : " + idle);
	    
	    cpuUsage =  (float)(user*100) / (user+nice+system+idle);
    } catch (IOException ex) {
        ex.printStackTrace();
    }
   	
   	String pattern = "###.##";
    DecimalFormat dformat = new DecimalFormat(pattern);
    String percentage  = dformat.format(cpuUsage);

    result.put("hostname", InetAddress.getLocalHost().getHostName());
   	result.put("cpu_usage", percentage+"%" );
   	
	request.setAttribute("xResult", result);
	
	RequestDispatcher dispatcher = null;
		
	if( callback == null ){
		out.clear();
		dispatcher = request.getRequestDispatcher("json.jsp");
	}
	else{
		request.setAttribute("xName", callback );
		
		dispatcher = request.getRequestDispatcher("jsonSys.jsp");
	}
	
	dispatcher.forward(request, response);
%>