<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page session="false" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.form.js"></script>
<html>
<head>
	<title>Develop</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//비밀글 체크
	var boardPwd = "${boardData.boardPwd}";
	if(boardPwd != null && boardPwd != ""){
		$("#defaultView").hide();
	}else{
		$("#secretView").hide();
	}
	
	$("#confirm_btn").click(function(){
		var params = {
				boardNo		:	$("#boardNo").val(),
				boardPwd	:	$("#boardPwdConfirm").val()
		}
		var url = contextRoot + "/board/developBoardPwdCheckAjax";
		fn_ajax(url, params, pwdCheckSuc);
	});
	
	//등록 버튼 클릭시 
	$("#modify_btn").click(function(){
		
		var boardNo = $("#boardNo").val();
		var boardTitle = $("#boardTitle").val().trim();
		var boardContent = $("#boardContent").val().trim();
		
		var params = {
				boardTitle		:	boardTitle,
				boardContent	:	boardContent
		};
		
		if(boardNo != null && boardNo != ""){
			params.boardNo = boardNo;
		}
		
		if($(':radio[name="radio"]:checked').val() == "private"){
			params.boardPwd = $("#boardPwd").val();
		}
		
		if(modifyCheck(params)){
			var url = contextRoot + "/board/developBoardModifyAjax";
			fn_ajax(url, params, modifyCheckSuc);
		}
	});
	
	//삭제 버튼 클릭시
	$("#delete_btn").click(function(){
		if(confirm("삭제 하시겠습니까?")){
			var boardNo = $("#boardNo").val();
			var url = contextRoot + "/board/developBoardDeleteAjax";
			var params = {
					boardNo	:	boardNo
			};
			fn_ajax(url, params, deleteCheckSuc);
		}
	});
	
	//취소 버튼 클릭시
	$("[name=cancel_btn]").click(function(){
		parent.closeLayerPopup(parent.goBoardList);
	});
	
	//파일 업로드
});

function pwdCheckSuc(data){
	var result = data.result;
	if(result == "Y"){
		var params = {
				boardNo		:	$("#boardNo").val()
		}
		var url = contextRoot + "/board/developBoardReadCountAjax";
		fn_ajax(url, params, null);
		$("#secretView").hide();
		$("#defaultView").show();
	}else{
		alert("비밀번호가 일치하지 않습니다.");
		$("#boardPwdConfirm").focus();
		return;
	}
}

function modifyCheck(data){
	if(data.boardTitle == null || data.boardTitle == ""){
		alert("제목을 입력하세요.");
		$("#boardTitle").focus();
		return false;
	}else if(data.boardContent == null || data.boardContent == ""){
		alert("내용을 입력하세요.");
		$("#boardContent").focus();
		return false;
	}else if($(':radio[name="radio"]:checked').val() == "private"){
		if($("#boardPwd").val().trim() == null || $("#boardPwd").val().trim() == ""){
			alert("비밀번호를 입력하세요.");
			$("#boardPwd").focus();
			return false;
		}
	}
	return true;
}

function modifyCheckSuc(data){
	if(data == 1){
		alert("등록 완료 되었습니다.");
		parent.closeLayerPopup(parent.goBoardList);
	}else{
		alert("등록 실패");
		return;
	}
}

function deleteCheckSuc(data){
	if(data == 1){
		alert("삭제 완료되었습니다.");
		parent.closeLayerPopup(parent.goBoardList);
	}else{
		alert("삭제 실패");
		return;
	}
}

function getExcelForm(){
	var file = $('#file');
// 	alert("file > "+ $('#file').val().replace("C:\\fakepath\\", ""));
	uploadExcel(file, "old");
	file.val("");
}

</script>
</head>
<body style="overflow: hidden;">
<form name="editForm" id="editForm" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
<input type="hidden" id="boardNo" value="${boardData.boardNo}">
<input type="hidden" name="fileSeq" 	id="fileSeq" value="<c:out value='${fileSeq}'/>" />
	<div class="row" style="position: fixed; top: 30%" id="secretView">
	
		<div class="col-lg-4" style="width: 549px;">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                 Develop View
	            </div>
	            <div class="panel-body" style="text-align: center;">
	                <p>이글은 비밀글입니다. <b>비밀번호를 입력하여 주세요.</b></p>
	                <p>
	                	<input type="password" id="boardPwdConfirm">&nbsp;
	                	<a href="javascript:;" id="confirm_btn" class="btn btn-outline btn-default">확인</a>
						<a href="javascript:;" name="cancel_btn" class="btn btn-outline btn-default">취소</a>
					</p>
	            </div>
	            <!-- /.panel-body -->
	        </div>
	        <!-- /.panel -->
	    </div>
    </div>
	<div class="row" id="defaultView">
    	<div class="panel panel-default">
        	<div class="panel-heading">
                Develop View
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive" style="overflow: hidden;">
                    <table class="table table-striped table-bordered table-hover">
                    	<c:choose>
                    	<c:when test="${not empty boardData.boardNo}">
                        <tr>
							<td>title</td>
							<td><input type="text" id="boardTitle" value="${boardData.boardTitle}"></td>
							<td>name</td>
							<td>${boardData.updateUserId}</td>
						</tr>
						<tr>
							<td>date</td>
							<td>${boardData.updateDate}</td>
							<td>read</td>
							<td>${boardData.readCount}</td>
						</tr>
						</c:when>
						<c:otherwise>
						<tr>
							<td>title</td>
							<td><input type="text" id="boardTitle" value="${boardData.boardTitle}"></td>
							<td>Id</td>
							<td>${loginUserId}</td>
						</tr>
						</c:otherwise>
						</c:choose>
						<tr>
							<td>file</td>
							<td colspan="3">
								<input type="file" id="files" name="files" multiple onchange="javascript:selectUploadFile('editForm');"/>
								<div id="uploadFileListArea"></div>
							</td>
<!-- 							<td colspan="3"><input type="file" size="30" id="file" name="file" onchange="getExcelForm()" /></td> -->
						</tr>
						<tr>
							<td height="250px;">content</td>
							<td colspan="3"><textarea rows="11" cols="54" id="boardContent"> ${boardData.boardContent}</textarea></td>
						</tr>
						<tr>
							<td>visibility&nbsp;level</td>
							<c:choose>
								<c:when test="${not empty boardData.boardPwd}">
								<td>
									<input type="radio" name="radio" id="radio0" value="public">
	               					<label for="radio0">public</label>&nbsp;&nbsp;
	               					<input type="radio" name="radio" id="radio1" value="private"  checked="checked">
	               					<label for="radio1">private</label>
								</td>
								<td>password</td>
								<td><input type="password" id="boardPwd" value="${boardData.boardPwd}"></td>
								</c:when>
								<c:otherwise>
								<td>
									<input type="radio" name="radio" id="radio0" value="public" checked="checked">
	               					<label for="radio0">public</label>&nbsp;&nbsp;
	               					<input type="radio" name="radio" id="radio1" value="private">
	               					<label for="radio1">private</label>
								</td>
								<td>password</td>
								<td><input type="password" id="boardPwd"></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr style="border: 0" height="30">
							<td colspan="4" align="center">
								<c:choose>
                    			<c:when test="${not empty boardData.boardNo}">
									<c:if test="${boardData.updateUser eq loginUserKey}">
									<a href="javascript:;" id="modify_btn" class="btn btn-outline btn-primary">수정</a>
									<a href="javascript:;" id="delete_btn" class="btn btn-outline btn-danger">삭제</a>
									</c:if>
<!-- 									<a href="javascript:;" id="reply_btn" class="btn btn-outline btn-success">답변</a> -->
									<a href="javascript:;" name="cancel_btn" class="btn btn-outline btn-info">취소</a>
								</c:when>
								<c:otherwise>
								<a href="javascript:;" id="modify_btn" class="btn btn-outline btn-primary">등록</a>
								<a href="javascript:;" name="cancel_btn" class="btn btn-outline btn-info">취소</a>
								</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</table>
				</div>
	           <!-- /.table-responsive -->
			</div>
	        <!-- /.panel-body -->
		</div>
	   	<!-- /.panel -->
	</div>
</form>
</body>
</html>
