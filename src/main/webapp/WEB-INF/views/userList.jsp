<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>User List</title>
<script type="text/javascript">
$(document).ready(function(){
	
	//유저 목록 조회 
	goUserList(1);
	
});

//유저 목록 조회
function goUserList(page){
	
// 	var searchItem = $("#searchItem").val(); 			//검색항목
// 	var searchValue = $("#searchValue").val();		   //검색어
	
	var url = contextRoot + "/admin/userListAjax";
	var params = {
			page : page,
			rec : 10
// 			searchItem : searchItem,
// 			searchValue : searchValue
	};
	
	fn_ajax(url, params, setUserList);
}

function setUserList(data){
	
	var page = data.page;
	var rec = data.rec;
	var records = data.records;
	
	console.log("records > "+records);
	
	var trArray = [];
	var temp = 0;
	var tempList = data.userList;
	
// 	$("#recordCnt").html("총 "+records+"개");
	
	$("#userList").empty();
	$("#pagenation").empty();
	
	if(tempList != null && tempList.length > 0){
		
		for(var i=0; i<tempList.length; i++){
			
	        trArray[temp++] = "<tr class='odd gradeX'>";
	        trArray[temp++] = "		<td>"+tempList[i].rnum+"</td>";
	        trArray[temp++] = "		<td><a href='javascript:goMessage(\"" + tempList[i].userKey + "\",\"" + tempList[i].userId + "\")'>"+tempList[i].userId+"</a></td>";
	        trArray[temp++] = "		<td>"+tempList[i].userNm+"</td>";
	        trArray[temp++] = "		<td>"+tempList[i].autrExpl+"</td>";
	        trArray[temp++] = "		<td>"+tempList[i].joinDate+"</td>";
	        trArray[temp++] = "		<td>";
	        trArray[temp++] = "			<a href='javascript:goView(\"" + tempList[i].userKey + "\")'> 수정 </a>";
	        trArray[temp++] = "			<a href='javascript:goView(\"" + tempList[i].userKey + "\")'> 삭제 </a></td>";
	        trArray[temp++] = "		</td>";
	        trArray[temp++] = "</tr>";
		}
		
		setPageNav({
			dataCount : records,								// 총 데이터 건 수
			pageNo : page,										// 현재 페이지 번호
			pageSize : rec,										// 한 페이지당 출력할 데이터 건 수
			pageGroupSize : 10,									// 페이지 번호 표시 범위
			pageNavList : $("#pagenation"),						// 페이지 네비게이션 표시할 DOM Element
			makePageLink : function(p_pageNo) {					// Page Link 문자열 구성할 function
				return "javascript:goUserList(" + p_pageNo + ");";
			}
		});
		
	}else{
		trArray[temp++] = "<tr>";
		trArray[temp++] = "<td colspan='6' align='center'>데이터가 없습니다.</td>";
		trArray[temp++] = "</tr>";
	}
	$("#userList").append(trArray.join(''));
	
	
	//bootstrap table(show entries, search, paging)
// 	$('#dataTables-example').DataTable({
// 		responsive: true
// 	});
	
}

function goMessage(userKey, userId){
	
	var url = contextRoot + "/admin/messageView?userKey="+userKey+"&userId="+userId;

	//메시지 보내기 팝업 open
	openLayerPopup(url);
}

</script>
</head>
<body>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User List</h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    user list table
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%">Id</th>
                                    <th width="15%">Name</th>
                                    <th width="10%">Authority</th>
                                    <th width="20%">Date</th>
                                    <th width="15%">Edit</th>
                                </tr>
                            </thead>
                            <tbody id="userList">
                            	<!--
                                <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                </tr>
                                -->
                            </tbody>
                        </table>
                    </div>
                    <nav style="text-align:center;" >
					  <ul class="pagination" id="pagenation">
					  </ul>
					</nav>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</body>
</html>
