<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<link rel="stylesheet" type="text/css"  href="${pageContext.request.contextPath}/resources/css/common.css"/>
<link rel="stylesheet" type="text/css"  href="${pageContext.request.contextPath}/resources/css/jquery-ui.css"/>

<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">
<!-- Morris Charts CSS -->
<%-- <link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet"> --%>
<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- DataTables CSS -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DataTables Responsive CSS -->
<link href="${pageContext.request.contextPath}/resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
<%-- <script src="${pageContext.request.contextPath}/resources/js/jquery.bpopup.min.js"></script> --%>
<script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.form.js"></script>
 
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bootstrap/dist/js/sb-admin-2.js"></script>

<!-- DataTables JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- layer popup(bpopup) -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.bpopup.min.js"></script>

<!-- jQuery Cookie -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.cookie.js"></script>

<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
<script id="facebook-jssdk" async="" src="//connect.facebook.net/en_US/all.js"></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<script>
var contextRoot = "${pageContext.request.contextPath}";

/**로그인 타입. 네이버*/
var LOGIN_TYPE_NAVER = "N";
var naverApiKey = "xNibYSumGv5F7CqCOnCw";

/**로그인 타입. 카카오*/
var LOGIN_TYPE_KAKAO = "K";
var kakaoApiKey =  "d3d5e1635f968c023d0d8964ba12c1a2";
Kakao.init(kakaoApiKey);		//카카오 api 초기화

/**로그인 타입. 페이스북*/
var LOGIN_TYPE_FACEBOOK = "F";
window.fbAsyncInit = function() { 	//페스북 api 셋팅
	FB.init({ 
		appId : '307385893026225', 
		cookie : true,
		xfbml : true,
		version : 'v2.9'
		// 쿠키가 세션을 참조할 수 있도록 허용 xfbml : true, // 소셜 플러그인이 있으면 처리 version : 'v2.1' // 버전 2.1 사용 
	});
}

/**로그인 타입. 구글*/
var LOGIN_TYPE_GOOGLE = "G";
/**구글 버튼 초기화로 인해 콜백함수가 호출 되는지 체크*/
var isGoogleInit = false;
var googleAccessToken;

/* Google Analytics(방문자 수 체크) 사용 */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-101726452-1', 'auto');
ga('send', 'pageview');


$(document).ready(function(){
	
});

</script>
</head>
<style>        
</style>
<body>
<!-- layer popup -->
<div id="modifyLayer">
<!--     <div class="popupContent"></div> -->
<!--     <div class="b-close"></div> -->
</div>
</body>
</html>