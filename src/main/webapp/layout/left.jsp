 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script>
	var contextRoot = "${pageContext.request.contextPath}";

	$(document).ready(function() {
	});
</script>
<!-- 로딩 이미지  -->
<div class="wrap-loading display-none">
    <div><img src="${pageContext.request.contextPath}/resources/img/loading.gif" /></div>
</div> 
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav in" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<li><a href="${pageContext.request.contextPath}/home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
			<li><a href="#"><i class="fa fa-gears fa-fw"></i> API<span
					class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="${pageContext.request.contextPath}/api/map">Map</a></li>
					<li><a href="${pageContext.request.contextPath}/api/weather">Weather</a></li>
				</ul> <!-- /.nav-second-level --></li>
			<li>
				<a href="tables.html">
					<i class="fa fa-desktop fa-fw"></i> UI<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li>
<%-- 						<a href="${pageContext.request.contextPath}/ui/template" >Template</a> --%>
						<a href="#" >Template</a>
					</li>
				</ul> <!-- /.nav-second-level -->
			</li>
			<li>
				<a href="tables.html">
					<i class="fa fa-wrench fa-fw"></i> Server<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li>
						<a href="${pageContext.request.contextPath}/server/state">State Check</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="tables.html">
					<i class="fa fa-edit fa-fw"></i> Board<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li>
						<a href="${pageContext.request.contextPath}/board/bootstrapBoardList">Bootstrap</a>
					</li>
					<li>
						<a href="${pageContext.request.contextPath}/board/developBoardList">Develop</a>
					</li>
<!-- 					<li> -->
<%-- 						<a href="${pageContext.request.contextPath}/board/anonymousBoardList">Anonymous</a> --%>
<!-- 					</li> -->
				</ul>
			</li>
			<li>
				<a href="tables.html">
					<i class="fa fa-save fa-fw"></i> Database(NoSQL)<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li>
<%-- 						<a href="${pageContext.request.contextPath}/database/mongo">Mongo</a> --%>
						<a href="#">Mongo</a>
					</li>
					<li>
<%-- 						<a href="${pageContext.request.contextPath}/database/redis">Redis</a> --%>
						<a href="#">Redis</a>
					</li>
<!-- 					<li> -->
<%-- 						<a href="${pageContext.request.contextPath}/board/anonymousBoardList">Anonymous</a> --%>
<!-- 					</li> -->
				</ul>
			</li>
			<li>
				<a href="tables.html">
					<i class="fa fa-sitemap fa-fw"></i> Admin<span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level collapse">
					<li>
<%-- 						<a href="${pageContext.request.contextPath}/admin/labelList">Label</a> --%>
						<a href="#">Label</a>
					</li>
					<li>
<%-- 						<a href="${pageContext.request.contextPath}/admin/userList">User</a> --%>
						<a href="#">User</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/chatting"><i class="fa fa-comments fa-fw"></i> Chatting</a>
			</li>
			<li>
<%-- 				<a href="${pageContext.request.contextPath}/develop"><i class="fa fa-book fa-fw"></i> Develop</a> --%>
				<a href="#"><i class="fa fa-book fa-fw"></i> Develop</a>
			</li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>