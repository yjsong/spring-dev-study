<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script>
var contextRoot = "${pageContext.request.contextPath}";

$(document).ready(function(){
	
	goMessageList();
	
	$("#logout_btn").click(function(){
		
		var LOGIN_TYPE = sessionStorage.getItem('LOGIN_TYPE');		////sessionStorage -> js session 값 얻기

		if(LOGIN_TYPE == LOGIN_TYPE_NAVER){
			//네이버는 이용자 보호를 위해 정책상 네이버 이외의 서비스에서 네이버 로그아웃을 수행하는 것을 허용하지 않고 있음(로그아웃은 안되고 사용했던 허용 토큰값 삭제는 가능)
			//토큰값 삭제x, refresh 후 token값을 가지고있는게 맞을듯(db컬럼 추가?)
// 	 		var url = contextRoot + "/login/naverLogoutAjax";
// 	 		fn_ajax(url, "", null);
		}else if(LOGIN_TYPE == LOGIN_TYPE_KAKAO){
			//카카오 로그아웃
			Kakao.Auth.logout();
		}else if(LOGIN_TYPE == LOGIN_TYPE_FACEBOOK){
			//페이스북 로그아웃
			FB.logout();
// 			FB.Event.subscribe('auth.logout', function(response) { //로그아웃 되는 순간 화면을 갱신
// 			    document.location.reload();
// 			});
		}else if(LOGIN_TYPE == LOGIN_TYPE_GOOGLE){
// 			gapi.auth.signOut();	//애매.. home 화면에서 onload 하면서 로그아웃함
		}
	
		sessionStorage.removeItem('LOGIN_TYPE');		////sessionStorage -> js session 값 삭제
		setTimeout("goLogout()", 100);
	});
	
});

function goLogout(){
	location.href=contextRoot + "/j_spring_security_logout";
}

function goMessageList(){
	var url = contextRoot + "/dashInfoAjax";
	var params = {
			readYn : "N"
	}
	fn_ajax(url, "", setMessageList);
}

function setMessageList(data){
	var trArray = [];
	var temp = 0;
	var tempList = data.messageList;
	
	$("#messageList").empty();
	if(tempList != null && tempList.length > 0){
		
		for(var i=0; i<tempList.length; i++){
			trArray[temp++] = '<li>';
			trArray[temp++] = '<a href="#">';
			trArray[temp++] = '	<div>';
			trArray[temp++] = '		<strong>'+tempList[i].fromUser+'</strong>';
			trArray[temp++] = '		<span class="pull-right text-muted">';
			trArray[temp++] = '			<em>'+tempList[i].sentDate+'</em>';
			trArray[temp++] = '		</span>';
			trArray[temp++] = '		</div>';
			trArray[temp++] = '	<div>'+tempList[i].messageContent+'</div>';
			trArray[temp++] = '</a>';
			trArray[temp++] = '</li>';
			trArray[temp++] = '<li class="divider"></li>';
			
			if((i+1) == tempList.length){
				trArray[temp++] = '<li>';
				trArray[temp++] = '	<a class="text-center" href="#">';
				trArray[temp++] = '		<strong>Read All Messages</strong>';
				trArray[temp++] = '		<i class="fa fa-angle-right"></i>';
				trArray[temp++] = '	</a>';
				trArray[temp++] = '</li>';
			}
			
		}
	}else{
		trArray[temp++] = '<li>';
		trArray[temp++] = '<a href="#">';
		trArray[temp++] = '	<div>';
		trArray[temp++] = '		<strong></strong>';
		trArray[temp++] = '		<span class="pull-right text-muted">';
		trArray[temp++] = '			<em></em>';
		trArray[temp++] = '		</span>';
		trArray[temp++] = '		</div>';
		trArray[temp++] = '	<div>새로운 메시지가 없습니다.</div>';
		trArray[temp++] = '</a>';
		trArray[temp++] = '</li>';
		trArray[temp++] = '<li class="divider"></li>';
		trArray[temp++] = '<li>';
		trArray[temp++] = '	<a class="text-center" href="#">';
		trArray[temp++] = '		<strong>Read All Messages</strong>';
		trArray[temp++] = '		<i class="fa fa-angle-right"></i>';
		trArray[temp++] = '	</a>';
		trArray[temp++] = '</li>';
	}
	$("#messageList").append(trArray.join(''));

}


</script>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="${pageContext.request.contextPath}/home">spring-dev-study</a>
	</div>
	<!-- /.navbar-header -->
	
	<ul class="nav navbar-top-links navbar-right">
	    <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	            <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
	        </a>
	        <ul class="dropdown-menu dropdown-messages" id="messageList">
	        <!-- 
	            <li>
	                <a href="#">
	                    <div>
	                        <strong>John Smith</strong>
	                        <span class="pull-right text-muted">
	                            <em>Yesterday</em>
	                        </span>
	                    </div>
	                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <strong>John Smith</strong>
	                        <span class="pull-right text-muted">
	                            <em>Yesterday</em>
	                        </span>
	                    </div>
	                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <strong>John Smith</strong>
	                        <span class="pull-right text-muted">
	                            <em>Yesterday</em>
	                        </span>
	                    </div>
	                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
	                </a>
	            </li>
	            <li class="divider"></li>
	           
	            <li>
	                <a class="text-center" href="#">
	                    <strong>Read All Messages</strong>
	                    <i class="fa fa-angle-right"></i>
	                </a>
	            </li>
	              -->
	        </ul>
	        <!-- /.dropdown-messages -->
	    </li>
	    <!-- /.dropdown -->
	    <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	            <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
	        </a>
	        <ul class="dropdown-menu dropdown-tasks">
	            <li>
	                <a href="#">
	                    <div>
	                        <p>
	                            <strong>Task 1</strong>
	                            <span class="pull-right text-muted">40% Complete</span>
	                        </p>
	                        <div class="progress progress-striped active">
	                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
	                                <span class="sr-only">40% Complete (success)</span>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <p>
	                            <strong>Task 2</strong>
	                            <span class="pull-right text-muted">20% Complete</span>
	                        </p>
	                        <div class="progress progress-striped active">
	                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
	                                <span class="sr-only">20% Complete</span>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <p>
	                            <strong>Task 3</strong>
	                            <span class="pull-right text-muted">60% Complete</span>
	                        </p>
	                        <div class="progress progress-striped active">
	                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
	                                <span class="sr-only">60% Complete (warning)</span>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <p>
	                            <strong>Task 4</strong>
	                            <span class="pull-right text-muted">80% Complete</span>
	                        </p>
	                        <div class="progress progress-striped active">
	                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
	                                <span class="sr-only">80% Complete (danger)</span>
	                            </div>
	                        </div>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a class="text-center" href="#">
	                    <strong>See All Tasks</strong>
	                    <i class="fa fa-angle-right"></i>
	                </a>
	            </li>
	        </ul>
	        <!-- /.dropdown-tasks -->
	    </li>
	    <!-- /.dropdown -->
	    <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	            <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
	        </a>
	        <ul class="dropdown-menu dropdown-alerts">
	            <!-- <li>
	                <a href="#">
	                    <div>
	                        <i class="fa fa-comment fa-fw"></i> New Comment
	                        <span class="pull-right text-muted small">4 minutes ago</span>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
	                        <span class="pull-right text-muted small">12 minutes ago</span>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li> -->
	            <li>
	                <a href="#">
	                    <div>
	                        <i class="fa fa-envelope fa-fw"></i> Message Sent
	                        <span class="pull-right text-muted small">4 minutes ago</span>
	                    </div>
	                </a>
	            </li>
	            <!-- <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <i class="fa fa-tasks fa-fw"></i> New Task
	                        <span class="pull-right text-muted small">4 minutes ago</span>
	                    </div>
	                </a>
	            </li>
	            <li class="divider"></li>
	            <li>
	                <a href="#">
	                    <div>
	                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
	                        <span class="pull-right text-muted small">4 minutes ago</span>
	                    </div>
	                </a>
	            </li> -->
	            <li class="divider"></li>
	            <li>
	                <a class="text-center" href="#">
	                    <strong>See All Alerts</strong>
	                    <i class="fa fa-angle-right"></i>
	                </a>
	            </li>
	        </ul>
	        <!-- /.dropdown-alerts -->
	    </li>
	    <!-- /.dropdown -->
	    <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
	            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
	        </a>
	        <ul class="dropdown-menu dropdown-user">
	        	<!-- 
	            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
	            </li>
	            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
	            </li>
	            <li class="divider"></li>
	            <li><a href="${pageContext.request.contextPath}/login"><i class="fa fa-sign-out fa-fw"></i> Login</a>
	            </li>
	             -->
	            <li><a href="${pageContext.request.contextPath}/user/userProfile"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
	            <li><a href="${pageContext.request.contextPath}/user/userSettings"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
	            <sec:authorize access="isAnonymous()">
	            	<li><a href="${pageContext.request.contextPath}/login/login"><i class="fa fa-sign-out fa-fw"></i> Login</a></li>
	            </sec:authorize>
	            <sec:authorize access="isAuthenticated()">
<%-- 					<li><a href="${pageContext.request.contextPath}/j_spring_security_logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li> --%>
					<li><a href="javascript:;" id="logout_btn"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
				</sec:authorize>
	        </ul>
	        <!-- /.dropdown-user -->
	    </li>
	    <!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->
	
	
