package com.study.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.study.common.CommonUtil;

@Service("apiService")
@ControllerAdvice
public class ApiServiceImpl implements ApiService{

	private static final Logger logger = LoggerFactory.getLogger(ApiServiceImpl.class);
	
	/** 애플리케이션 클라이언트 아이디값 **/
	@Value("#{contextProperties['naver.clientId']}")
	private String clientId;
	
	/** 애플리케이션 시크릿값 아이디값 **/
	@Value("#{contextProperties['naver.clientSecret']}")
	private String clientSecret;
	
	/** 캡챠 이미지 URL **/
	@Value("#{contextProperties['naver.captcha.apiURL']}")
	private String apiURL;
	
	/** 생성된 API key **/
	String captchaKey = "";
	
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	/** 캡차 이미지 생성 **/
	@Override
	public JSONObject createCaptchaImage(){
		JSONObject result = new JSONObject();
		/** API KEY 생성 **/
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("url", "https://openapi.naver.com/v1/captcha/nkey");
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", "0");
		
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("X-Naver-Client-Id", clientId);
		headers.put("X-Naver-Client-Secret", clientSecret);
		
		param.put("params", params);
		param.put("headers", headers);
		
		captchaKey = String.valueOf(commonUtil.APIExamInfo(param).get("key"));
		
		System.out.println("captchaKey >> "+captchaKey);
		
		result.put("result", apiURL+"?key="+captchaKey);
		
		return result;
	}
	
	/** 캡차 이미지와 사용자 입력값 비교 **/
	@Override
	public JSONObject compareCaptchaValue(String captchaText){
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("url", "https://openapi.naver.com/v1/captcha/nkey");
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", "1");
		params.put("key", captchaKey);
		params.put("value", captchaText);
		
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("X-Naver-Client-Id", clientId);
		headers.put("X-Naver-Client-Secret", clientSecret);
		
		param.put("params", params);
		param.put("headers", headers);
		
		return commonUtil.APIExamInfo(param);
	}
	
	/** 네이버 캡차 API 예제 - 키발급
	public String APIExamCaptchaNkey(){
		
		//생성된 key(결과) 값
		String result = "";
		
        try {
            String code = "0"; // 키 발급시 0,  캡차 이미지 비교시 1로 세팅
            String apiURL = "https://openapi.naver.com/v1/captcha/nkey?code=" + code;
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("X-Naver-Client-Id", clientId);
            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            //json 형태로 받기 위함
            JSONObject jsonObj = new JSONObject();
            JSONParser parser = new JSONParser();
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
                
                //string을 json형태로 받음 
                jsonObj = (JSONObject)parser.parse(inputLine);
                
                //json 형태의 value 값만 String으로 담음
                result = String.valueOf(jsonObj.get("key"));
            }
            br.close();
            logger.debug("captcha captchaKey : {}", result);
        } catch (Exception e) {
            System.out.println(e);
        }
		
		return result;
	}
	 **/
	/** 네이버 캡차 API 예제 - 입력값 비교
	public JSONObject APIExamCaptchaNkeyResult(String value){
		
		JSONObject result = new JSONObject();
		
		try {
            String code = "1"; // 키 발급시 0,  캡차 이미지 비교시 1로 세팅
            String apiURL = "https://openapi.naver.com/v1/captcha/nkey?code=" + code +"&key="+ captchaKey + "&value="+ value;

            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("X-Naver-Client-Id", clientId);
            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
          //json 형태로 받기 위함
    		JSONParser parser = new JSONParser();
    		
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
                
                //string을 json형태로 받음 
                result = (JSONObject)parser.parse(inputLine);
            }
            br.close();
            logger.debug("captcha compare : {}", result);
        } catch (Exception e) {
            System.out.println(e);
        }
		return result;
	}
	 **/
	
	@ExceptionHandler(Exception.class)
	public void  handleMyException(Exception exception, HttpServletRequest request) {
	    logger.debug("request url : {}", request.getRequestURI());
	    exception.printStackTrace();
	}

}
