package com.study.service;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.study.common.CommonUtil;
import com.study.dao.BoardDao;
import com.study.model.Board;
import com.study.model.Common;

@Service("boardService")
@ControllerAdvice
public class BoardServiceImpl implements BoardService{
	
	private static final Logger logger = LoggerFactory.getLogger(BoardServiceImpl.class);
	
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	@Resource(name = "boardDao")
	private BoardDao boardDao;
	
	/** 게시판 목록 조회 **/
	@Override
	public JSONObject selectBoardList(){
		JSONObject result = new JSONObject();
		result.put("result", boardDao.selectBoardList());
		return result;
	}

	/** 게시판 목록 조회 **/
	@Override
	public JSONObject selectDevelopBoardList(Common common){
		JSONObject result = new JSONObject();
		result.put("page", common.getPage());
		result.put("rec",  common.getRec());

		//게시판 목록 조회 
		List<Board> developBoardList = boardDao.selectDevelopBoardList(common);
		
		if(developBoardList.size() > 0){
			result.put("developBoardList", developBoardList);
			
			//목록 개수 조회 
			int records = boardDao.selectDevelopBoardListCount(common);
			
			result.put("records", records);
		}else{
			result.put("records", 0);
		}
		return result;
	}

	/** 게시판 상세 조회 **/
	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Board selectDevelopBoardView(Board board){
		
		Board developBoardView = null;
		
		if(board.getBoardNo() != 0 ){
			developBoardView = boardDao.selectDevelopBoardView(board);
			
			/** 게시판 조회수 증가 **/
			if(developBoardView.getBoardPwd() == null || developBoardView.getBoardPwd() == ""){
				if(!commonUtil.getLoginUser().getUserKey().equals(developBoardView.getUpdateUser())){
					boardDao.updateDevelopBoardReadCount(board);
				}
			}
			
		}
		
		return developBoardView;
	}

	/** 게시판 내용 수정 **/
	@Override
	public int updateDevelopBoardView(Board board){
		int result = 0;
		board.setUpdateUser(commonUtil.getLoginUser().getUserKey());
		if(board.getBoardNo() != 0){
			result = boardDao.updateDevelopBoardView(board);
		}else{
			result =  boardDao.insertDevelopBoardView(board);
		}
		return result;
	}

	/** 게시판 내용 삭제 **/
	@Override
	public int deleteDevelopBoardView(Board board){
		int result = 0;
		board.setUpdateUser(commonUtil.getLoginUser().getUserKey());
		result = boardDao.deleteDevelopBoardView(board);
		return result;
	}
	
	@ExceptionHandler(Exception.class)
	public void  handleMyException(Exception exception, HttpServletRequest request) {
	    logger.debug("request url : {}", request.getRequestURI());
	    exception.printStackTrace();
	}

	/** 비밀글 비밀번호 일치 조회 */
	@Override
	public JSONObject selectDevelopBoardPwd(Board board) {
		JSONObject result = new JSONObject();
		
		if(boardDao.selectDevelopBoardPwd(board) > 0){
			result.put("result", "Y");
		}else{
			result.put("result", "N");
		}
		
		return result;
	}

	/** 게시판 조회수 증가 */
	@Override
	public void updateDevelopBoardReadCount(Board board) {
		Board developBoardView = boardDao.selectDevelopBoardView(board);
		if(!commonUtil.getLoginUser().getUserKey().equals(developBoardView.getUpdateUser())){
			boardDao.updateDevelopBoardReadCount(board);
		}
	}
}
