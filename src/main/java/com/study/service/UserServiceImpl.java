package com.study.service;

import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.study.common.CommonUtil;
import com.study.dao.UserDao;
import com.study.model.Common;
import com.study.model.Login;
import com.study.security.ShaPasswordEncoder;

@Service("userService")
@ControllerAdvice
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	@Resource(name = "apiService")
	private ApiService apiService;
	
	@Resource(name = "userDao")
	private UserDao userDao;
	
	@Autowired
	private ShaPasswordEncoder shaPasswordEncoder;

	/**
	 * 사용자 가입 처리
	 * error 처리 try catch 사용시 rollback 되지 않는 문제 발생
	 * 정의된 Exception에 대해서는 rollback을 수행, 트랜잭션 전파규칙을 정의 , Default=REQURIED
	 */
	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public JSONObject InsertUserInfo(Login login){
		JSONObject result = new JSONObject();
		
		//사용자 등록
		if(login.getUserKey() == null || login.getUserKey() == ""){
			//captcha 비교
			if((Boolean) apiService.compareCaptchaValue(login.getCaptchaText()).get("result")){
				
				//아이디 사용여부 체크
				if(userDao.selectUserIdCount(login) == 0){
					
					//비밀번호 암호화
					shaPasswordEncoder.setBase64(true);	//한글깨짐문제로..
					login.setUserPass(shaPasswordEncoder.encode(login.getUserPass()));
					
					//가입
					userDao.InsertUserInfo(login);
					/* 수정필요 */
					login.setAutrNm("ROLE_USER");
					login.setAutrExpl("사용자");
					userDao.insertUserRole(login);
					result.put("result", "Y");
					
				}else{
					logger.info("사용 중인 아이디");
					result.put("result", "D");
				}
				
			}else{
				result.put("result", "C");
			}
		
		//사용자 수정
		}else{
			
			//비밀번호 암호화
			shaPasswordEncoder.setBase64(true);	//한글깨짐문제로..
			login.setUserPass(shaPasswordEncoder.encode(login.getUserPass()));
			userDao.updateUserInfo(login);
			logger.info("사용자 수정 완료");
			result.put("result", "Y");
		}
		return result;
	}
	
	/**
	 * 소셜 로그인 가입 체크 및 가입
	 */
	@Override
	public JSONObject userSnsCheck(Login login) {
		JSONObject result = new JSONObject();
			
		//아이디 사용여부 체크
		if(userDao.selectUserIdCount(login) == 0){
			
			//비밀번호 암호화
			shaPasswordEncoder.setBase64(true);	//한글깨짐문제로..
			login.setUserPass(shaPasswordEncoder.encode(login.getUserPass()));
			
			//가입
			userDao.InsertUserInfo(login);
			/* 수정필요 */
			login.setAutrNm("ROLE_USER");
			login.setAutrExpl("사용자");
			userDao.insertUserRole(login);
			logger.info("사용자 등록 완료");
			result.put("result", "Y");
			
		}else{
			logger.info("사용 중인 아이디");
			result.put("result", "D");
		}
		
		return result;
	}
	
	/** 사용자 정보 조회 */
	@Override
	public Login selectUserInfo() {
		Login login = new Login();
		login.setUserKey(commonUtil.getLoginUser().getUserKey());
		return userDao.selectUserInfo(login);
	}
	
	@ExceptionHandler(Exception.class)
	public void handleMyException(Exception exception, HttpServletRequest request) {
		/**
		 * 로그레벨은 TRACE > DEBUG > INFO > WARN > ERROR > FATAL 순 
		 * # Log Level
		 * # TRACE : 추적 레벨은 Debug보다 좀더 상세한 정보를 나타냄
		 * # DEBUG : 프로그램을 디버깅하기 위한 정보 지정
		 * # INFO :  상태변경과 같은 정보성 메시지를 나타냄
		 * # WARN :  처리 가능한 문제, 향후 시스템 에러의 원인이 될 수 있는 경고성 메시지를 나타냄 
		 * # ERROR :  요청을 처리하는 중 문제가 발생한 경우
		 * # FATAL :  아주 심각한 에러가 발생한 상태, 시스템적으로 심각한 문제가 발생해서 어플리케이션 작동이 불가능할 경우
		 */
	    logger.debug("=== Exception Handler === {}", request.getRequestURI());
	    exception.printStackTrace();
	}

	
	//구글 사용 이메일 발송
    public static String googleEmailSend(String userEmail) throws Exception{
    	
    	String sucYn = "Y";
    	
    	Properties p = new Properties();
  	  	p.put("mail.smtp.user", "springstudyjongno@gmail.com"); // Google계정@gmail.com으로 설정
  	  	p.put("mail.smtp.host", "smtp.gmail.com");
  	  	p.put("mail.smtp.port", "587");
  	  	p.put("mail.smtp.starttls.enable","true");
  	  	p.put( "mail.smtp.auth", "true");
  	 
//  	  	p.put("mail.smtp.debug", "true");
//  	  	p.put("mail.smtp.socketFactory.port", "465"); 
//  	  	p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); 
//  	  	p.put("mail.smtp.socketFactory.fallback", "false");
  	 
  	  	//Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
  	 
  	  	try {
  	  		
  	  		Authenticator auth = new SMTPAuthenticator();
  	  		Session session = Session.getInstance(p, auth);
  	  		session.setDebug(true); // 메일을 전송할 때 상세한 상황을 콘솔에 출력한다.
	  	 
  	  		//session = Session.getDefaultInstance(p);
  	  		MimeMessage msg = new MimeMessage(session);
  	  		String message = "";
  	  		

  	  		/*
  	  		// 1~10까지의 정수를 랜덤하게 출력
  	        // nextInt 에 10 을 입력하면 0~9 까지의 데이타가 추출되므로 +1 을 한것이다.
  	  		Random random = new Random();
  	        String data = "";
  	        for (int i = 0; i < 6; i++) {
  	            data += random.nextInt(10) + 1;
  	        }
  	        System.out.print("data >> "+data);
  	        */
  	        HttpServletRequest request =((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
  	    	HttpSession httpSession = request.getSession(true);
  	        
//	  	    ServletRequestAttributes servletRequestAttribute = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//	        HttpSession httpSession = servletRequestAttribute.getRequest().getSession(true);
	  
  	    	/*
	        httpSession.setAttribute("authNo",data);
  	        
  	  		message += "요청하신 인증번호를 알려드립니다.<br>";
	  	  	message += "아래의 인증번호를 인증번호 입력창에 입력해 주세요.<br>";
	  	  	message += "인증번호 : "+data;
  			*/
  	    	//token 사용해서 만료되게 해야됨(jwt 사용 해보기)
  	    	message += "Reset password  >> http://localhost:8080/controller/user/resetPassword?userPass=";
  	  		msg.setSubject("Reset password spring-dev-study");
  	  		Address fromAddr = new InternetAddress("springstudyjongno@gmail.com"); // 보내는 사람의 메일주소
  	  		msg.setFrom(fromAddr);
  	  		Address toAddr = new InternetAddress(userEmail);  					   // 받는 사람의 메일주소
  	  		msg.addRecipient(Message.RecipientType.TO, toAddr); 
  	  		msg.setContent(message, "text/plain;charset=KSC5601");
  	  		System.out.println("Message: " + msg.getContent());
  	  		Transport.send(msg);
  	  		System.out.println("Gmail SMTP서버를 이용한 메일보내기 성공");
  	  		
  	  	}catch (Exception mex) { // Prints all nested (chained) exceptions as well 
  	  		System.out.println("I am here??? ");
  	  		sucYn = "N";
  	  		mex.printStackTrace(); 
	  	} 
  	  	
  	  	return sucYn;
    }
    
    private static class SMTPAuthenticator extends javax.mail.Authenticator {
    	public PasswordAuthentication getPasswordAuthentication() {
    		return new PasswordAuthentication("springstudyjongno", "spring1234"); // Google id, pwd, 주의) @gmail.com 은 제외하세요
    	}
    } 
    
    //네이버 사용 이메일 발송
    public static void naverEmailSend(String userEmail) throws Exception{
    	
    	String host = "smtp.naver.com";
        final String username = "syj9302";       //네이버 이메일 주소중 @ naver.com앞주소만 기재합니다.
        final String password = "";   	//네이버 이메일 비밀번호를 기재합니다.
        int port=465;
         
        // 메일 내용
        String recipient = "syj9302@naver.com";    //메일을 발송할 이메일 주소를 기재해 줍니다.
        String subject = "네이버를 사용한 발송 테스트입니다.";
        String body = "내용 무";
         
        Properties props = System.getProperties();
          
          
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.trust", host);
           
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            String un=username;
            String pw=password;
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(un, pw);
            }
        });
        
        session.setDebug(true); //for debug
           
        Message mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(new InternetAddress("syj9302@naver.com"));
        mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        mimeMessage.setSubject(subject);
        mimeMessage.setText(body);
        Transport.send(mimeMessage);
        
    }

    /** 가입 정보 이메일과 일치 여부(Y, N) */
	@Override
	public JSONObject findUserPass(Login login){
		JSONObject result = new JSONObject();
		try{
			//가입 정보 이메일과 일치 여부 판단
			if(userDao.selectUserPass(login) != null && userDao.selectUserPass(login) != ""){
				result.put("result", googleEmailSend(String.valueOf(login.getUserEmail()))); 
			}else{
				System.out.println("사용자 정보와 일치하는 아이디 없음");
				result.put("result", "Y");
			}
		}catch(Exception e){
			result.put("result", "N");
			e.printStackTrace();
		}
		return result;
	}

	/** 사용자 정보 목록 조회 */
	@Override
	public JSONObject selectUserList(Common common) {
		
		JSONObject result = new JSONObject();
		result.put("page", common.getPage());
		result.put("rec",  common.getRec());
		
		//유저 목록 조회 
		List<Login> userList = userDao.selectUserList(common);
		
		if(userList.size() > 0){
			result.put("userList", userList);
			
			//목록 개수 조회 
			int records = userDao.selectUserListCount(common);
			
			result.put("records", records);
		}else{
			result.put("records", 0);
		}
		return result;
	}

}
