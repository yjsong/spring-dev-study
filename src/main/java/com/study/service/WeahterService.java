package com.study.service;

import java.util.Map;

import org.json.simple.JSONObject;

public interface WeahterService {

	/**
	 * 지역 날씨 정보 조회
	 * @param params
	 * @return 지역 날씨 정보
	 */
	JSONObject weatherInfo(Map<String, Object> params);

}
