package com.study.service;

import org.json.simple.JSONObject;

public interface ApiService {

	/**
	 * 캡차 이미지 생성 
	 * @return 캡챠 이미지 URL
	 */
	JSONObject createCaptchaImage();

	/**
	 * 캡차 이미지와 사용자 입력값 비교
	 * @param captchaText
	 * @return 비교 후 결과 값(true, false)
	 */
	JSONObject compareCaptchaValue(String captchaText);

}
