package com.study.service;

import org.json.simple.JSONObject;

public interface LoginService {

	/**
	 * 네이버 로그인 회원정보 조회
	 * @param code
	 * @return
	 */
	JSONObject selectNaverInfo(String code);

	/**
	 * 네이버 로그인 토큰 제거
	 * @return
	 */
	JSONObject deleteNaverToken();

}
