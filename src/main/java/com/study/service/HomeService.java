package com.study.service;

import org.json.simple.JSONObject;

public interface HomeService {

	/**
	 * 대시보드 정보
	 * @return 새로운 메시지 개수, 새로운 게시글 개수
	 */
	JSONObject selectDashboardInfo();

	/**
	 * 전체 방문자 수+1
	 */
	void insertGuestTotalCount();

	/**
	 * 대시보드 정보
	 * @return 최근 게시글 시간
	 */
	JSONObject dashInfo();

}
