package com.study.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.common.CommonUtil;
import com.study.dao.AdminDao;
import com.study.model.Message;

@Service("adminService")
public class AdminServiceImpl implements AdminService{
	
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;

	@Resource(name = "adminDao")
	private AdminDao adminDao;
	
	/**
	 *  보낸 메시지 저장
	 */
	@Override
	public int insertMessage(Message message) {
		int result = 0;
		try {
			message.setFromUser(commonUtil.getLoginUser().getUserKey());
			result = adminDao.insertMessage(message);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
