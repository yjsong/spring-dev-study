package com.study.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.study.dao.SampleDao;

@Service("SampleService")
public class SampleServiceImpl implements SampleService{

	@Resource(name = "sampleDao")
	private SampleDao sampleDao;
	
	/** 현재시각 조회 **/
	@Override
	public String selectSampleData() {
		return sampleDao.selectSampleData();
	}
}
