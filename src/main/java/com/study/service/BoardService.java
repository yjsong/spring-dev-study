package com.study.service;

import org.json.simple.JSONObject;

import com.study.model.Board;
import com.study.model.Common;

public interface BoardService {

	/**
	 * 게시판 목록 조회
	 * @return 게시판 목록 정보
	 */
	JSONObject selectBoardList();

	/**
	 * 게시판 목록 조회
	 * @param common
	 * @return 게시판 목록 정보
	 */
	JSONObject selectDevelopBoardList(Common common);

	/**
	 * 게시판 상세 조회
	 * @param board
	 * @return 게시판 상세 정보
	 */
	Board selectDevelopBoardView(Board board);

	/**
	 * 게시판 내용 수정 
	 * @param board
	 * @return 게시판 정보 수정 후 성공(1), 실패(0) 여부
	 */
	int updateDevelopBoardView(Board board);

	/**
	 * 게시판 내용 삭제
	 * @param board
	 * @return게시판 정보 삭제 후 성공(1), 실패(0) 여부
	 */
	int deleteDevelopBoardView(Board board);

	/**
	 * 비밀글 비밀번호 일치 조회
	 * @param board
	 * @return 일치(1)
	 */
	JSONObject selectDevelopBoardPwd(Board board);

	/**
	 * 게시판 조회수 증가
	 * @param board
	 */
	void updateDevelopBoardReadCount(Board board);


}
