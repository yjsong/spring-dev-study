package com.study.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.study.common.CommonUtil;
import com.study.dao.AdminDao;
import com.study.dao.BoardDao;
import com.study.dao.HomeDao;
import com.study.model.Common;
import com.study.model.Message;

@Service("homeService")
public class HomeServiceImpl implements HomeService{

	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	@Resource(name = "homeDao")
	private HomeDao homeDao;
	
	@Resource(name = "adminDao")
	private AdminDao adminDao;
	
	@Resource(name = "boardDao")
	private BoardDao boardDao;
	
	/** 대시보드 정보 */
	@Override
	public JSONObject selectDashboardInfo() {
		JSONObject result = new JSONObject();
		try {
			
			Common common = new Common();
			Date today = new Date(); 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			common.setStartDate(sdf.format(today));
			Message message = new Message();
			message.setUserKey(commonUtil.getLoginUser().getUserKey());
			result.put("messageCnt", adminDao.selectMessageListCount(message));
			result.put("boardCnt", boardDao.selectDevelopBoardListCount(common));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/** 전체 방문자 수 +1 */
	@Override
	public void insertGuestTotalCount() {
		homeDao.insertGuestTotalCount();
	}

	@Override
	public JSONObject dashInfo() {
		
		JSONObject result = new JSONObject();
//		Map<String, Object> test = homeDao.chartInfo();
//		System.out.println("test2 > "+test.toString());

		ArrayList<String> date = new ArrayList<String>();
		ArrayList<String> visit = new ArrayList<String>();
		ArrayList<String> members = new ArrayList<String>();
		
		
		date.add("\"date\"");		//key 값 셋팅
		visit.add("\"visit\"");
		members.add("\"members\"");
		
		for(int i=6; i>=0; i--){
			java.util.Calendar cal = java.util.Calendar.getInstance();
			java.text.DateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
			cal.add(cal.DATE, -i); // 7일(일주일)을 뺀다
			
			date.add("\""+format.format(cal.getTime())+"\"");
			visit.add("\""+homeDao.selectVisitCount(format.format(cal.getTime())).get("day")+"\"");
			members.add("\""+homeDao.selectMembersCount(format.format(cal.getTime())).get("day")+"\"");
			cal.clear();
		}
//		System.out.println("date > "+date);
//		System.out.println("visit > "+visit);
//		System.out.println("members > "+members);
		
//		JSONObject boardTime = new JSONObject();
		ArrayList<Object> boardTime = new ArrayList<Object>();
		Map<String, Object> map = homeDao.selectTimeTest();
		
		//수정 필요
		outerloop:
		for (String s : map.keySet()) {
//			System.out.println(boardTime.add(Math.abs(Integer.parseInt(String.valueOf(map.get(s)))) + " " + s));
			int time = Math.abs(Integer.parseInt(String.valueOf(map.get(s))));
			if(time < 60 && time != 0){
				boardTime.add(time + " " + s);
				break outerloop;
			}else if(s.equals("days") && time != 0){
				boardTime.add(time + " " + s);
			}
		}
		System.out.println("time > "+boardTime);
		
		result.put("date", date);
		result.put("visit", visit);
		result.put("members", members);
		result.put("boardTime", boardTime);
		Message message = new Message();
		message.setReadYn("N");
		message.setUserKey(commonUtil.getLoginUser().getUserKey());
		result.put("messageList", adminDao.selectMessageList(message));
		
		return result;
	}
	
	
}
