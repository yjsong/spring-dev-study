package com.study.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("weatherService")
public class WeatherServiceImpl implements WeahterService{

	private static final Logger logger = LoggerFactory.getLogger(WeatherServiceImpl.class);
	
	/** 지역 날씨 정보 */
	@Override
	public JSONObject weatherInfo(Map<String, Object> params) {
		return weatherInfoResult(params);
	}
	
	public JSONObject weatherInfoResult(Map<String, Object> params){
		
		JSONObject result = new JSONObject();
		
		try {
            String code = "1"; // 키 발급시 0,  캡차 이미지 비교시 1로 세팅
            String apiURL = "http://apis.skplanetx.com/weather/current/hourly?version=1&city="+params.get("city")+"&county="+params.get("county")+"&village="+params.get("village");

            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("appKey", "8c1bb43c-ca2e-3a31-9e8f-0c581f674fe2");
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
          //json 형태로 받기 위함
    		JSONParser parser = new JSONParser();
    		JSONObject weatherObj = new JSONObject();
    		JSONArray weatherArr = new JSONArray();
    		
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
                
                //string을 json형태로 받음 
                weatherObj = (JSONObject)parser.parse(inputLine);
                JSONObject resultCode = (JSONObject)weatherObj.get("result");
                System.out.println("resultCode >> "+resultCode);
                if(Integer.parseInt(resultCode.get("code").toString()) == 9200){
                	weatherObj = (JSONObject) weatherObj.get("weather");
                	weatherArr = (JSONArray) weatherObj.get("hourly");
                	
                	if(weatherArr.size() > 0){
                		result.put("result", "Y");
                		result.put("weather", weatherObj);
                	}else{
                		result.put("result", "N");
                	}
                }else{
                	result.put("result", "N");
                }
            }
            br.close();
            logger.debug("weatherInfo : {}", result);
        } catch (Exception e) {
            System.out.println(e);
        }
		return result;
	}

}
