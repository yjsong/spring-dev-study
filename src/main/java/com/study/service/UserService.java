package com.study.service;

import org.json.simple.JSONObject;

import com.study.model.Common;
import com.study.model.Login;

public interface UserService {

	/**
	 * 사용자 가입
	 * @param login
	 * @param request
	 * @return 가입 성공, 실패 여부(Y,N) 
	 */
	JSONObject InsertUserInfo(Login login);

	/**
	 * 소셜 로그인 가입 체크 및 가입
	 * @param login
	 * @return 소셜 로그인 가입 성공, 실패 여부(Y, D)
	 */
	JSONObject userSnsCheck(Login login);

	/**
	 * 사용자 정보 조회
	 * @return 사용자 정보
	 */
	Login selectUserInfo();

	/**
	 * 비밀번호 찾기
	 * @param login
	 * @return 가입 정보 이메일과 일치 여부(Y, N)
	 */
	JSONObject findUserPass(Login login);

	/**
	 * 사용자 정보 목록 조회 
	 * @param common
	 * @return 사용자 정보 목록
	 */
	JSONObject selectUserList(Common common);

}
