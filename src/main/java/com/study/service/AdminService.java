package com.study.service;

import com.study.model.Message;

public interface AdminService {

	/**
	 * 보낸 메시지 저장
	 * @param message
	 * @return
	 */
	int insertMessage(Message message);

}
