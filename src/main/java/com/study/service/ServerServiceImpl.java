package com.study.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service("serverService")
public class ServerServiceImpl implements ServerService {
	
	private static final Logger logger = LoggerFactory.getLogger(ServerServiceImpl.class);
	
	@Value("#{contextProperties['state.check.servers']}")
	private String servers;
	
	Map<String, Object> result;
	
	/** 서버 상태 체크  */
	@Override
	public Map<String, Object> serverStateInfo() {
		result = new TreeMap<String, Object>();
		String[] serverList = servers.split("\\|");
		int count = serverList.length;
		
		try {
			GetThread[] threads = new GetThread[serverList.length];
			
			for(int i=0; i<count; i++){
				threads[i] = new GetThread(serverList[i]);
				threads[i].start();
			}
			
			for(Thread t : threads){
				t.join();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	class GetThread extends Thread{
		private String url;
		
		public GetThread(String url){
			this.url = url;
		}
		
		public void run(){
			try {
				result.put(url.split("http://")[1].split(":")[0],serverCheck(url));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**서버 호출. HttpURLConnection  */
	@SuppressWarnings("unchecked")
	public JSONArray serverCheck(String strUrl){	
		JSONObject result = new JSONObject(); 
		
		JSONArray jsonArr = new JSONArray();

		logger.info("URL : "+strUrl);
		
		HttpURLConnection con = null;
		
		try{
			URL url = new URL(strUrl);
			con = (HttpURLConnection)url.openConnection();
			con.setConnectTimeout(3000);
			con.setReadTimeout(2000);
			con.setRequestMethod("GET");
			con.connect();
			
//			InputStream is = con.getInputStream();
			
//			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8));
			StringBuffer outBuffer = new StringBuffer();
			String line = "";
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObj = new JSONObject();
			while( (line=br.readLine()) != null){
				//JSON데이터를 넣어 JSON Object 로 만들어 준다.
				jsonObj = (JSONObject) jsonParser.parse(line);
				outBuffer.append(line);
			}
			
			result.put("result", "Y");
			result.put("cpu_usage", jsonObj.get("cpu_usage"));
			result.put("hostname", jsonObj.get("hostname"));

			JSONObject space = (JSONObject) jsonObj.get("space");
			result.put("useable_space", space.get("useable_space"));
			
			JSONObject memory = (JSONObject) jsonObj.get("memory");
			result.put("total_memory_size", memory.get("total_memory_size"));
			result.put("free_memory_size", memory.get("free_memory_size"));
			result.put("use_memory_percentage", memory.get("use_memory_percentage"));

		}catch(Exception e){
			result.put("result", "N");
			e.printStackTrace();
		}finally{
			con.disconnect();
		}		
		
		jsonArr.add(result);
		
		return jsonArr;
	}	

}
