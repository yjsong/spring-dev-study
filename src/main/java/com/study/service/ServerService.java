package com.study.service;

import java.util.Map;

public interface ServerService {

	/**
	 * 서버 상태 체크
	 * @return 서버 IP, 서버상태(Y,N), total_memory_size, cpu_usage, 
	 * 		   use_memory_percentage, hostname, free_memory_size, useable_space
	 */
	Map<String, Object> serverStateInfo();

}
