package com.study.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.study.common.CommonUtil;

@Service("loginService")
public class LoginServiceImpl implements LoginService{

	/** 애플리케이션 클라이언트 아이디값 **/
	@Value("#{contextProperties['naver.clientId']}")
	private String clientId;
	
	/** 애플리케이션 시크릿값 아이디값 **/
	@Value("#{contextProperties['naver.clientSecret']}")
	private String clientSecret;
	
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	String access_token =  "";
	String refresh_token =  "";
	
	/**
	 * 네이버 로그인 회원정보 조회
	 */
	@Override
	public JSONObject selectNaverInfo(String code) {
        
		Map<String, Object> code_param = new HashMap<String, Object>();
		code_param.put("url", "https://nid.naver.com/oauth2.0/token");
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grant_type", "authorization_code");
		params.put("client_id", clientId);
		params.put("client_secret", clientSecret);
		params.put("code", code);

		code_param.put("params", params);
		
		//네이버 로그인 토큰 발급
		JSONObject token_result = commonUtil.APIExamInfo(code_param);
		access_token = String.valueOf(token_result.get("access_token"));
//		refresh_token = String.valueOf(token_result.get("refresh_token"));
		System.out.println("access_token > "+access_token);
		
		
		Map<String, Object> token_params = new HashMap<String, Object>();
		token_params.put("url", "https://openapi.naver.com/v1/nid/me");
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("access_token", access_token);
	
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Authorization", "Bearer " + access_token);		
		
		token_params.put("params", param);
		token_params.put("headers", headers);
		
		//네이버 로그인 회원정보 조회
		return commonUtil.APIExamInfo(token_params);
	}
	
	/**
	 * 네이버 로그인 토큰 제거
	*/
	@Override
	public JSONObject deleteNaverToken() {
//		System.out.println("==========================Test start==========================");
//		APIExamDeleteToken(access_token);
//		APIExamLoginInfo(access_token);
//		APIExamRefreshToken(refresh_token);
//		APIExamLoginInfo(access_token);
		return null;
	}
	
	/** 네이버 로그인 토큰 발급 
	public JSONObject APIExamLoginToken(String code){
		
		JSONObject token = new JSONObject();
        try {
            String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&code="+code;
            System.out.println("url > "+apiURL);
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
//            con.setRequestProperty("Authorization", header);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            //json 형태로 받기 위함
            JSONObject jsonObj = new JSONObject();
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            System.out.println(response.toString());
            //string을 json형태로 받음 
            jsonObj = (JSONObject)parser.parse(response.toString());
            //json 형태의 value 값만 String으로 담음
            token.put("access_token", jsonObj.get("access_token"));
            token.put("refresh_token", jsonObj.get("refresh_token"));
            
//            token = jsonObj.get("access_token").toString();
        } catch (Exception e) {
            System.out.println(e);
        }
		
		return token;
	}
	
	/** 네이버 로그인 회원정보 조회 	**/
	public JSONObject APIExamLoginInfo(String token){
		System.out.println("=========APIExamLoginInfo===========");
		JSONObject result = new JSONObject();
		
		String header = "Bearer " + token; // Bearer 다음에 공백 추가
        try {
            String apiURL = "https://openapi.naver.com/v1/nid/me";
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", header);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            result = (JSONObject)parser.parse(response.toString());
            System.out.println("result > "+result);
        } catch (Exception e) {
            System.out.println(e);
        }
	
		return result;
	}
	/** 네이버 카페가입 **/
	public JSONObject APIExamLoginTest(String token){
		System.out.println("=========APIExamLoginInfo===========");
		JSONObject result = new JSONObject();
		
        String header = "Bearer " + token; // Bearer 다음에 공백 추가
        try {
            String apiURL = "https://openapi.naver.com/v1/cafe/16700487/members";
//            https://openapi.naver.com/v1/cafe/{clubid}/menu/{menuid}/articles66
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", header);
            // post request
            String nickname = URLEncoder.encode(URLEncoder.encode("yjsong", "UTF-8"), "MS949");
            // 카페 api 한글은  UTF8로 인코딩 한 값을 MS949로 한번더 인코딩 해야 함
            String postParams = "nickname="+nickname;
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            result = (JSONObject)parser.parse(response.toString());
            System.out.println("result > "+result);
        } catch (Exception e) {
            System.out.println(e);
        }
	
		return result;
	}

	
	/** 네이버 로그인 토큰 발급 
	public String APIExamDeleteToken(String code){
		
		System.out.println("=============APIExamDeleteToken================");
		String token = "";
		
        try {
//            String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&code="+code;
        	String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&access_token="+URLEncoder.encode(code, "UTF-8")+"&service_provider=NAVER";

            System.out.println("url > "+apiURL);
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
//            con.setRequestProperty("Authorization", header);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            //json 형태로 받기 위함
            JSONObject jsonObj = new JSONObject();
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            System.out.println(response.toString());
            //string을 json형태로 받음 
            jsonObj = (JSONObject)parser.parse(response.toString());
            //json 형태의 value 값만 String으로 담음
            token = jsonObj.get("access_token").toString();
        } catch (Exception e) {
            System.out.println(e);
        }
		
		return token;
	}

	 **/
	/** 네이버 로그인 토큰 발급 
	public String APIExamRefreshToken(String code){
		System.out.println("======= APIExamRefreshToken start ============");
		String token = "";
		
        try {
//            String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&code="+code;
//        	String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=delete&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&access_token="+code+"&service_provider=NAVER";
            String apiURL = "https://nid.naver.com/oauth2.0/token?grant_type=refresh_token&client_id=xNibYSumGv5F7CqCOnCw&client_secret=SpHPFbG9Xg&refresh_token="+code;
        	System.out.println("url > "+apiURL);
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
//            con.setRequestProperty("Authorization", header);
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            //json 형태로 받기 위함
            JSONObject jsonObj = new JSONObject();
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            System.out.println(response.toString());
            //string을 json형태로 받음 
            jsonObj = (JSONObject)parser.parse(response.toString());
            //json 형태의 value 값만 String으로 담음
            token = jsonObj.get("access_token").toString();
        } catch (Exception e) {
            System.out.println(e);
        }
		
		return token;
	}
	**/

}
