package com.study.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public interface CommonService {

	Map<String, Object> insertExcelData(MultipartFile file, String type);

}
