package com.study.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class ShaPasswordEncoder implements PasswordEncoder {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private boolean base64 = false;
	
	public void setBase64(boolean base64) {
		this.base64 = base64;
	}
	
	public boolean isBase64Encode(boolean base64Encode) {
		return this.base64;
	}

	private byte[] encryption(String value) throws NoSuchAlgorithmException {  
		MessageDigest md = MessageDigest.getInstance("SHA-256");  
		try {
			md.update(value.getBytes("UTF-8"));
		}
		catch(Exception ex) {
			md.update(value.getBytes());
		}
		return md.digest();  
	}  
	
	@Override
	public String encode(CharSequence rawPassword) {
		String encodePwd;
		byte[] arrEnc;
		try {
			arrEnc = encryption(rawPassword.toString());
			
			if (base64) {
				encodePwd = new String(Base64.encodeBase64(arrEnc));
			}
			else {
				encodePwd = new String(arrEnc);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			encodePwd = "";
		}

		logger.debug("Org password : {}, Encode password : {}", rawPassword, encodePwd);
		
		return encodePwd;
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		logger.debug("rawPassword : {}, encodedPassword : {}", rawPassword.toString(), encodedPassword );
		
		boolean isMatches = false;
		if (null == rawPassword || rawPassword.toString().isEmpty()) {
			isMatches = null == rawPassword ? rawPassword == encodedPassword : rawPassword.equals(encodedPassword);
		}
		else {
			isMatches = this.encode(rawPassword).equals(encodedPassword);
		}
		
		logger.debug("matches result : {}", isMatches );
		return isMatches;
	}

}
