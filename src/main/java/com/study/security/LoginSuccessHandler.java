package com.study.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

public class LoginSuccessHandler implements AuthenticationSuccessHandler
{

	@SuppressWarnings("unused")
	private final org.slf4j.Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);


	private RequestCache requestCache = new HttpSessionRequestCache();

    private String targetUrlParameter;
    private String defaultUrl;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public String getTargetUrlParameter() {
        return targetUrlParameter;
    }

    public void setTargetUrlParameter(String targetUrlParameter) {
        this.targetUrlParameter = targetUrlParameter;
    }

    public String getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException
    {
    	clearAuthenticationAttributes(request);

        SavedRequest savedRequest = requestCache.getRequest(request, response);

        String targetUrl = null;
        if (null != targetUrlParameter) {
        	targetUrl = request.getParameter(targetUrlParameter);
        }

    	if (null != targetUrl && !targetUrl.isEmpty()) {
    		logger.debug("Go requestParameter({})={}", targetUrlParameter, targetUrl);
            if(savedRequest != null) {
                requestCache.removeRequest(request, response);
            }
    	}
    	else {
        	if (savedRequest != null) {
        		logger.debug("Go requestCache.savedRequest={}", savedRequest.getRedirectUrl());
                targetUrl = savedRequest.getRedirectUrl();
        	}
        	else if (null != defaultUrl && !defaultUrl.isEmpty()) {
        		logger.debug("Go defaultUrl={}", defaultUrl);
        		targetUrl = this.defaultUrl;
        	}
        	else {
        		targetUrl = contextPathNormalize(request) + "/home";
        		logger.debug("Go {}", targetUrl);
        	}
    	}

    	redirectStrategy.sendRedirect(request, response, targetUrl);
//    	response.sendRedirect(request.getContextPath() + "/login/index");
    }

    public static String contextPathNormalize(HttpServletRequest request) {
		String contextPath = request.getContextPath();
		if (contextPath.endsWith("/")) {
			contextPath = contextPath.substring(0,contextPath.length() - 1);
		}
		//logger.debug("contextPathNormalize >>> request.getContextPath : [{}], result : [{}]", request.getContextPath(), contextPath);
		return contextPath;
	}

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);

        if (session == null) {
            return;
        }

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}