package com.study.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("sampleDao")
public class SampleDao {

	@Autowired
	public SqlSessionTemplate sqlSessionTemplate;
	
	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	this.sqlSessionTemplate = sqlSessionTemplate;    	
	}
	
	/** 현재시각 조회 **/
	public String selectSampleData() {
		return sqlSessionTemplate.selectOne("Sample.selectSampleData");
	}

}
