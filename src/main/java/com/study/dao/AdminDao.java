package com.study.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.study.model.Message;

@Repository("adminDao")
public class AdminDao {

	@Autowired
	public SqlSessionTemplate sqlSessionTemplate;
	
	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	this.sqlSessionTemplate = sqlSessionTemplate;    	
	}
	
	/**
	 * 보낸 메시지 저장
	 * @param message
	 */
	public int insertMessage(Message message) {
		return sqlSessionTemplate.insert("Admin.insertMessage", message);
	}

	/**
	 * 메시지 목록 개수 조회
	 * @return 메시지 목록 개수
	 */
	public int selectMessageListCount(Message message) {
		return sqlSessionTemplate.selectOne("Admin.selectMessageListCount", message);
	}

	/**
	 * 메시지 목록 정보 조회
	 * @return 메시지 목록 정보
	 */
	public List<Message> selectMessageList(Message message) {
		return sqlSessionTemplate.selectList("Admin.selectMessageList", message);
	}

}
