package com.study.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.study.model.Common;
import com.study.model.Login;

@Repository("userDao")
public class UserDao {

	@Autowired
	public SqlSessionTemplate sqlSessionTemplate;

	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	this.sqlSessionTemplate = sqlSessionTemplate;    	
	}
	
	/**
	 * 아이디 사용여부 체크
	 * @param login
	 * @return 아이디 사용 개수
	 */
	public int selectUserIdCount(Login login) {
		return sqlSessionTemplate.selectOne("User.selectUserIdCount", login);
	}
	
	/**
	 * 사용자 등록
	 * @param login
	 */
	public void InsertUserInfo(Login login) {
		sqlSessionTemplate.insert("User.InsertUserInfo", login);
	}
	
	/**
	 * 사용자 권한 등록
	 * @param login
	 */
	public void insertUserRole(Login login) {
		sqlSessionTemplate.insert("User.insertUserRole", login);
	}

	/**
	 * 사용자 정보 조회 
	 * @param login
	 * @return 사용자 정보
	 */
	public Login selectUserInfo(Login login) {
		return sqlSessionTemplate.selectOne("User.selectUserInfo", login);
	}

	/**
	 * 사용자 정보 수정
	 * @param login
	 */
	public void updateUserInfo(Login login) {
		sqlSessionTemplate.update("User.updateUserInfo", login);
	}

	/**
	 * 가입 정보 이메일과 일치 여부 판단
	 * @param login
	 * @return
	 */
	public String selectUserPass(Login login) {
		return sqlSessionTemplate.selectOne("User.selectUserPass", login);
	}

	/**
	 * 사용자 정보 목록 조회 
	 * @param common
	 * @return 사용자 정보 목록
	 */
	public List<Login> selectUserList(Common common) {
		return sqlSessionTemplate.selectList("User.selectUserList", common);
	}

	/**
	 * 사용자 정보 목록 개수 조회 
	 * @param common
	 * @return 사용자 정보 목록 개수
	 */
	public int selectUserListCount(Common common) {
		return sqlSessionTemplate.selectOne("User.selectUserListCount", common);
	}

}
