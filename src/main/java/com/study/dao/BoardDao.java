package com.study.dao;

import java.util.List;

import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.study.model.Board;
import com.study.model.Common;

@Repository("boardDao")
public class BoardDao {

	@Autowired
	public SqlSessionTemplate sqlSessionTemplate;
	
	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	this.sqlSessionTemplate = sqlSessionTemplate;    	
	}
	
	/**
	 * 게시판 목록 조회
	 * @return 게시판 목록 정보
	 */
	public List<Board> selectBoardList() {
		return sqlSessionTemplate.selectList("Board.selectBoardList");
	}

	/**
	 * 게시판 목록 조회
	 * @param common
	 * @return 게시판 목록 정보
	 */
	public List<Board> selectDevelopBoardList(Common common) {
		return sqlSessionTemplate.selectList("Board.selectDevelopBoardList", common);
	}

	/**
	 * 게시판 목록 개수 조회
	 * @return 게시판 목록 개수
	 */
	public int selectDevelopBoardListCount(Common common) {
		return sqlSessionTemplate.selectOne("Board.selectDevelopBoardListCount", common);
	}
	
	/**
	 * 게시판 조회수 증가
	 * @param board
	 */
	public void updateDevelopBoardReadCount(Board board) {
		sqlSessionTemplate.update("Board.updateDevelopBoardReadCount", board);
	}

	/**
	 * 게시판 상세 조회
	 * @param board
	 * @return 게시판 상세 정보
	 */
	public Board selectDevelopBoardView(Board board) {
		return sqlSessionTemplate.selectOne("Board.selectDevelopBoardView", board);
	}

	/**
	 * 게시판 내용 수정
	 * @param board
	 */
	public int updateDevelopBoardView(Board board) {
		return sqlSessionTemplate.update("Board.updateDevelopBoardView", board);
	}

	/**
	 * 게시판 내용 등록
	 * @param board
	 */
	public int insertDevelopBoardView(Board board) {
		return sqlSessionTemplate.insert("Board.insertDevelopBoardView", board);
	}
	
	/**
	 * 게시판 내용 삭제
	 * @param board
	 */
	public int deleteDevelopBoardView(Board board) {
		return sqlSessionTemplate.update("Board.deleteDevelopBoardView", board);
	}

	/**
	 * 비밀글 비밀번호 일치 조회
	 * @param board
	 * @return 일치(1)
	 */
	public int selectDevelopBoardPwd(Board board) {
		return sqlSessionTemplate.selectOne("Board.selectDevelopBoardPwd", board);
	}

}
