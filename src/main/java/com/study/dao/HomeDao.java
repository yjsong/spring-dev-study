package com.study.dao;

import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("homeDao")
public class HomeDao {
	
	private static HomeDao instance = new HomeDao();

	@Autowired
	public SqlSessionTemplate sqlSessionTemplate;
	
	@Autowired
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	this.sqlSessionTemplate = sqlSessionTemplate;    	
	}
	
	/** 전체 방문자 수 + 1 */
	public void insertGuestTotalCount() {
		sqlSessionTemplate.insert("Home.insertGuestTotalCount");
	}

	/** 오늘 방문자 수 */
	public int selectGuestTodayCount() {
		return sqlSessionTemplate.selectOne("Home.selectGuestTodayCount");
	}

	/** 전체 방문자 수 */
	public int selectGuestTotalCount() {
		return sqlSessionTemplate.selectOne("Home.selectGuestTotalCount");
	}

	public Map<String, Object> selectVisitCount(String date) {
		return sqlSessionTemplate.selectOne("Home.selectVisitCount", date);
	}

	public Map<String, Object> selectMembersCount(String date) {
		return sqlSessionTemplate.selectOne("Home.selectMembersCount", date);
	}

	public Map<String, Object> selectTimeTest() {
		return sqlSessionTemplate.selectOne("Home.selectTimeTest");
	}

}
