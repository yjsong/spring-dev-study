package com.study.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.study.model.Login;

@Service("commonUtil")
public class CommonUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);
	
	private static final String TRANSFORM = "AES/ECB/PKCS5Padding";
	
	
	public Login getLoginUser(){
		Login login = (Login) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		return login.getUserId();
		return login;
	}
	
	/** AES 암호화 */
	public static String encrypt(String message) throws Exception {

		//암호화 키 16자리
//		String key = "fe8025947de7cd71"; 
		String key = "adminpass**skbtv"; 
		
		String encrypted = null;
		try{
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		kgen.init(128);
		
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance(TRANSFORM);
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		encrypted = byteArrayToHex(cipher.doFinal(message.getBytes()));
		}catch(Exception e){
			return encrypted;
		}
		return encrypted;
	}
	
	/** AES 복호화 */
	public static String decrypt(String message) throws Exception {
		
		//암호화 키 16자리
//		String key = "fe8025947de7cd71"; 
		String key = "adminpass**skbtv"; 
			
		String decrypted = null;

		try{
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		kgen.init(128);

		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance(TRANSFORM);

		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		decrypted = new String(cipher.doFinal(hexToByteArray(message)));
			
		}catch(Exception e){
			return decrypted;
		}
		return decrypted;
	}
	
	private static String byteArrayToHex(byte buf[]) {
	    StringBuffer strbuf = new StringBuffer(buf.length * 2);
	    for (int i = 0; i < buf.length; i++) {
	        if (((int) buf[i] & 0xff) < 0x10) {
	            strbuf.append("0");
	        }
	        strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
	    }
	    return strbuf.toString();
	}
	
	private static byte[] hexToByteArray(String s) {
	    byte[] retValue = null;
	    if (s != null && s.length() != 0) {
	        retValue = new byte[s.length() / 2];
	        for (int i = 0; i < retValue.length; i++) {
	            retValue[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
	        }
	    }
	    return retValue;
	}
	
	public static void responseToString(HttpServletResponse response , String result )
	{
		response.setContentType("text/html; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = null;
		
		try {
			out = response.getWriter();	
		} catch (IOException e) {
			
		}
		out.write(result);
		out.flush();
	}
	
	/**
	 * API 호출시 사용
	 * @param params
	 * @return result
	 */
	public JSONObject APIExamInfo(Map<String, Object> param){
		logger.debug("=========APIExamInfo===========");
		JSONObject result = new JSONObject();
		
//		String header = "Bearer " + params.get("param"); // Bearer 다음에 공백 추가
        try {
            String apiURL = String.valueOf(param.get("url"));
            
            if(param.get("params") != null){
            Map<String, Object> params = (Map<String, Object>) param.get("params");
	            int gubun = 1;
	            for (Map.Entry<String, Object> entry : params.entrySet()) {
	            	if(gubun == 1){
	            		apiURL += "?"+entry.getKey() +"="+ entry.getValue();
	            	}else{
	            		apiURL += "&"+entry.getKey() +"="+ entry.getValue();
	            	}
	            	gubun++;
				}
            }
            
            logger.debug("apiURL : "+apiURL);
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
//            con.setRequestProperty("Authorization", header);
            if(param.get("headers") != null){
            	Map<String, Object> headers = (Map<String, Object>) param.get("headers");
	            for (Map.Entry<String, Object> entry : headers.entrySet()) {
	            	System.out.println("header > "+entry.getValue());
	            	con.setRequestProperty(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
				}
            }
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            JSONParser parser = new JSONParser();
            
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
            }
            br.close();
            result = (JSONObject)parser.parse(response.toString());
            logger.debug("result : "+result);
        } catch (Exception e) {
            e.printStackTrace();
        }
	
		return result;
	}
	
}
