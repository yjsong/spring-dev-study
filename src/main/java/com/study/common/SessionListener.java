package com.study.common;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.study.dao.HomeDao;

@WebListener
public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent event) {
    	
    	System.out.println("session check");

    	WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(event.getSession().getServletContext());

    	HomeDao homeDao = (HomeDao) context.getBean("homeDao");
    	
		// 전체 방문자 수 +1
    	homeDao.insertGuestTotalCount();
		 
		// 오늘 방문자 수
//		int todayCount = homeDao.selectGuestTodayCount();
		 
		// 전체 방문자 수
//		int totalCount = homeDao.selectGuestTotalCount();
		 
		
		// 세션 속성에 담아준다.
//		session.setAttribute("totalCount", totalCount); // 전체 방문자 수
		         
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
    	
//    	ApplicationContext context = WebApplicationContextUtils
//                .getWebApplicationContext(event.getSession().getServletContext());
//
//        HttpSessionListener target = context.getBean(
//                "httpSessionEventListener", HttpSessionListener.class);
//        target.sessionDestroyed(event);
        
    }
}