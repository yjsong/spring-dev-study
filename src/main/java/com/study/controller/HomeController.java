package com.study.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.study.service.HomeService;
import com.study.service.SampleService;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	@Resource(name = "homeService")
	private HomeService homeService;
	
	@Autowired
	@Resource(name = "SampleService")
	private SampleService sampleService;
	
	/**
	 * home page
	 * @param locale
	 * @param model
	 * @return new message, new board
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(ModelMap map) {
		map.put("dashboardInfo", homeService.selectDashboardInfo());
		return "home.tiles";
	}
	
	/**
	 * dashInfo Ajax
	 * @return 최근 게시글 시간
	 */
	@RequestMapping(value = "/dashInfoAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject dashInfoAjax(){
		return homeService.dashInfo();
	}
	
	
	@RequestMapping(value = "/googleTest", method = RequestMethod.GET)
	public String googleTest(Locale locale, Model model) {
		return "googleTest.default";
	}
	
	@RequestMapping(value = "/googleTestSuc", method = RequestMethod.GET)
	public String googleTestSuc(Locale locale, Model model) {
		return "googleTestSuc.default";
	}
	
	@RequestMapping(value = "/loadingTest", method = RequestMethod.GET)
	public String loadingTest(Locale locale, Model model) {
		return "loadingTest.tiles";
	}
	
	@RequestMapping(value = "/weatherTest", method = RequestMethod.GET)
	public String weatherTest(Locale locale, Model model) {
		return "weatherTest.tiles";
	}
	
	@RequestMapping(value = "/chatTest", method = RequestMethod.GET)
	public String chatTest(Locale locale, Model model) {
		return "chatTest.tiles";
	}
	
	@RequestMapping(value = "/movieMapTest", method = RequestMethod.GET)
	public String movieMapTest(Locale locale, Model model) {
		return "movieMapTest.default";
	}
	
	@RequestMapping(value = "/playerTest", method = RequestMethod.GET)
	public String playerTest(Locale locale, Model model) {
		return "playerTest.default";
	}
	
	@RequestMapping(value = "/d3Test", method = RequestMethod.GET)
	public String d3Test(Locale locale, Model model) {
		return "d3Test.default";
	}
	
	@RequestMapping(value = "/guestTest", method = RequestMethod.GET)
	public String guestTest(Locale locale, Model model) {
		return "guestTest.default";
	}
	
	@RequestMapping(value = "/guestCountTest", method = RequestMethod.GET)
	public String guestCountTest(Locale locale, Model model) {
		return "guestCountTest.default";
	}
	
	@RequestMapping(value = "/movieHouseMap", method = RequestMethod.GET)
	public String movieHouseMap(Locale locale, Model model) {
		return "movieHouseMap.default";
	}
	
	@RequestMapping(value = "/allMovieHouseMap", method = RequestMethod.GET)
	public String allMovieHouseMap(Locale locale, Model model) {
		return "allMovieHouseMap.default";
	}
	
	/**
	 * sample data 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/sample", method = RequestMethod.GET)
	public String sample(Locale locale, Model model) {
		String sampleData = sampleService.selectSampleData();
		logger.info("sampleData : "+sampleData);
		return "home.tiles";
	}
	
	@RequestMapping(value = "/readme", method = RequestMethod.GET)
	public String readme(Locale locale, Model model) {
		return "readme.default";
	}
	
	
	/* Test */
	
	
	
	@RequestMapping(value = "/fileTest", method = RequestMethod.GET)
	public String fileTest(Locale locale, Model model) {
		return "fileTest.default";
	}
	
	@RequestMapping(value = "/fileUpload") // method = RequestMethod.GET 
	public Map fileUpload(HttpServletRequest req, HttpServletResponse rep) { //파일이 저장될 path 설정 
		
		String path = "/svc/test/";
		Map returnObject = new HashMap(); try { // MultipartHttpServletRequest 생성 
			
		MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest) req; 
		Iterator iter = mhsr.getFileNames(); 
		MultipartFile mfile = null; 
		String fieldName = ""; 
		List resultList = new ArrayList(); // 디레토리가 없다면 생성 
		File dir = new File(path); 
		if (!dir.isDirectory()) { dir.mkdirs(); } // 값이 나올때까지 
		while (iter.hasNext()) { 
			fieldName = (String) iter.next(); // 내용을 가져와서 
			mfile = mhsr.getFile(fieldName); 
			String origName; 
			origName = new String(mfile.getOriginalFilename().getBytes("8859_1"), "UTF-8"); //한글꺠짐 방지 // 파일명이 없다면 
			if ("".equals(origName)) { continue; } // 파일 명 변경(uuid로 암호화)
			String ext = origName.substring(origName.lastIndexOf('.')); // 확장자 
			String saveFileName = getUuid() + ext; // 설정한 path에 파일저장 
			File serverFile = new File(path + File.separator + saveFileName); 
			mfile.transferTo(serverFile); 
			Map file = new HashMap(); 
			file.put("origName", origName); 
			file.put("sfile", serverFile); 
			resultList.add(file); 
		} 
		returnObject.put("files", resultList); 
		returnObject.put("params", mhsr.getParameterMap()); 
		}catch (UnsupportedEncodingException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		}catch (IllegalStateException e) { // TODO Auto-generated catch block 
			e.printStackTrace(); 
		} catch (IOException e) { // TODO Auto-generated catch block 
			e.printStackTrace(); 
		} return null; 
	} //uuid생성 
	
	public static String getUuid() {
		return UUID.randomUUID().toString().replaceAll("-", ""); 
	}



}
