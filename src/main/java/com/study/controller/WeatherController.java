package com.study.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.service.WeahterService;

@Controller
@RequestMapping(value = "/weahter")
public class WeatherController {

	private static final Logger logger = LoggerFactory.getLogger(WeatherController.class);
	
	@Autowired
	@Resource(name = "weatherService")
	private WeahterService weatherService;
	
	/**
	 * skplanetxWeather page
	 * @return 
	 */
	@RequestMapping(value = "/skplanetxWeather", method = RequestMethod.GET)
	public String skplanetxWeather() {
		return "skplanetxWeather.tiles";
	}
	
	/**
	 * skplanetx Weather Info Ajax
	 * @param city
	 * @param county
	 * @param village
	 * @return 지역 날씨 정보
	 */
	@RequestMapping(value = "/skplanetxWeatherInfoAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject skplanetxWeatherInfoAjax(@RequestParam("city") String city,
			@RequestParam("county") String county, @RequestParam("village") String village){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("city", city);
		params.put("county", county);
		params.put("village", village);
		
		return weatherService.weatherInfo(params);
	}
}
