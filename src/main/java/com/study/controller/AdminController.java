package com.study.controller;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.model.Common;
import com.study.model.Login;
import com.study.model.Message;
import com.study.service.AdminService;
import com.study.service.UserService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	@Resource(name = "userService")
	private UserService userService;
	
	@Autowired
	@Resource(name = "adminService")
	private AdminService adminService;
	
	/**
	 * userList page
	 * @return
	 */
	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public String userList(){
		return "userList.tiles";
	}
	
	/**
	 * userList Ajax
	 * @param common
	 * @return 사용자 목록
	 */
	@RequestMapping(value = "/userListAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject userListAjax(@ModelAttribute Common common){
		return userService.selectUserList(common);
	}
	
	/**
	 * messageView popup
	 * @param user
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/messageView", method = RequestMethod.GET)
	public String messageView(@ModelAttribute Login user, ModelMap map){
		map.put("userKey", user.getUserKey());
		map.put("userId", user.getUserId());
		return "messageView.pop";
	}
	
	/**
	 * insertMessage Ajax
	 * @param message
	 * @return 전송 성공(1), 실패 여부 반환
	 */
	@RequestMapping(value = "/insertMessageAjax", method = RequestMethod.POST)
	public @ResponseBody int insertMessageAjax(@ModelAttribute Message message){
		return adminService.insertMessage(message);
	}
	
	/**
	 * labelList page
	 * @return
	 */
	@RequestMapping(value = "/labelList", method = RequestMethod.GET)
	public String labelList(){
		return "labelList.tiles";
	}
}
