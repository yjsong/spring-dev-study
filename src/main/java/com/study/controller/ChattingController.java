package com.study.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ChattingController {

	private static final Logger logger = LoggerFactory.getLogger(ChattingController.class);
	
	/**
	 * chatting page
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/chatting", method = RequestMethod.GET)
	public String chatting(ModelMap map) {
		return "chatting.tiles";
	}
}
