package com.study.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.study.model.Tags;

/**
 * spring 관련
 */
@Controller
@RequestMapping(value = "/spring")
public class SpringController {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringController.class);

	/**
	 * spring tags 간단 예제
	 * @param model
	 * @param tags
	 * @return 
	 */
	@RequestMapping(value = "/springTags", method = RequestMethod.GET)
	public String springTags(Model model, @ModelAttribute Tags tags){
		
		tags.setName("홍길동");
		
		model.addAttribute(tags);		// spring tags test
		
		model.addAttribute("test", tags.getName());	//el tags test
		
		
		return "springTags";
	}
	
	@RequestMapping(value = "/securityTags", method = RequestMethod.GET)
	public String securityTags(){
		return "securityTags";
	}
}
