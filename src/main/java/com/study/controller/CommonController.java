package com.study.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.study.model.FileUploadForm;
import com.study.service.CommonService;

@Controller
@RequestMapping(value = "/common")
public class CommonController {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
	
	@Autowired
	@Resource(name = "commonService")
	private CommonService commonService;
	
	@Value("#{contextProperties['fileupload.temp.path']}")
	private String uploadTempPath;

	@Value("#{contextProperties['fileupload.path']}")
	private String uploadPath;
	
	@RequestMapping(value = "/uploadExcel" , method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> uploadExcel(HttpServletRequest request, MultipartHttpServletRequest mRquest){
		Map<String, Object> result = new HashMap<String, Object>();	
		try{
			Iterator<String> itr = mRquest.getFileNames();
			String type = mRquest.getParameter("type");
			String fileNames = itr.next();
			MultipartFile file = (MultipartFile) mRquest.getFile(fileNames);
			
			if(file != null){				
				String fileName = StringUtils.defaultIfEmpty(file.getOriginalFilename(), "");
				long fileSize = file.getSize();
				if(fileSize > 0 && !fileName.equals("")){
					result = commonService.insertExcelData(file, type);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.put("result", "fail");
			result.put("size", "0");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/fileUpload")
	public @ResponseBody Map<String, Object> fileUpload(@ModelAttribute("uploadForm") FileUploadForm uploadForm,
						   HttpServletRequest request, HttpServletResponse response)
						   throws IllegalStateException, IOException {
		
		JSONObject obj = new JSONObject();
		List<MultipartFile> files = uploadForm.getFiles();
		String fileNm = "";
		List<String> list = new ArrayList<String>();
		
		logger.debug("upload file count : {}", files.size());
		if (null != files && files.size() > 0) {
			for (MultipartFile multipartFile : files) {
				fileNm = multipartFile.getOriginalFilename();
				logger.debug("upload file name : {}", fileNm);
				File file = null;
				String nwFileName = Long.toString(Calendar.getInstance().getTimeInMillis());
				try {
					//file = new File(uploadPath + fileNm);
					file = new File(uploadTempPath + "/" + nwFileName);
					multipartFile.transferTo(file);
					
					list.add(fileNm + "||" + nwFileName);
					// Excel 파일 읽기!!
					//list = readExcelFile(file);
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		obj.put("filePath", uploadPath + fileNm);
		obj.put("fileList", list);

		return obj;
	}
	
	

}
