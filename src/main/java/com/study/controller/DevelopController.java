package com.study.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DevelopController {
	
	/**
	 * develop page
	 * @return
	 */
	@RequestMapping(value = "/develop", method = RequestMethod.GET)
	public String develop(){
		return "develop.tiles";
	}
}
