package com.study.controller;

import java.security.Principal;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.CommonUtil;
import com.study.model.Board;
import com.study.model.Common;
import com.study.service.BoardService;

@Controller
@RequestMapping(value = "/board")
public class BoardController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	@Resource(name = "boardService")
	private BoardService boardService;
	
	@Autowired
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	/**
	 * bootstrap page
	 * @return 
	 */
	@RequestMapping(value = "/bootstrapBoardList", method = RequestMethod.GET)
	public String bootstrapBoardList() {
		return "bootstrapBoardList.tiles";
	}

	/**
	 * boardList ajax
	 * @return 게시판 목록 정보
	 */
	@RequestMapping(value = "/boardListAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject boardListAjax(){
		return boardService.selectBoardList();
	}
	
	/**
	 * developBoardList page 
	 * @return
	 */
	@RequestMapping(value = "/developBoardList", method = RequestMethod.GET)
	public String developBoardList() {
		return "developBoardList.tiles";
	}
	
	/**
	 * developBoardList ajax
	 * @param common
	 * @param request
	 * @return 게시판 목록 정보
	 */
	@RequestMapping(value = "/developBoardListAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject developBoardListAjax(@ModelAttribute Common common){
		return boardService.selectDevelopBoardList(common);
	}
	
	/**
	 * developBoardModify page
	 * @param board
	 * @param map
	 * @return 로그인 유저, 게시판 상세 정보
	 */
	@RequestMapping(value = "/developBoardView", method = RequestMethod.GET)
	public String developBoardView(@ModelAttribute Board board, ModelMap map, Principal principal){
		map.put("loginUserKey", commonUtil.getLoginUser().getUserKey());
//		map.put("loginUserNm", commonUtil.getLoginUser().getUserNm());
		map.put("loginUserId", commonUtil.getLoginUser().getUserId());
		map.put("boardData", boardService.selectDevelopBoardView(board));
		return "developBoardView.pop";
	}
	
	/**
	 * developBoardView modify ajax
	 * @param board
	 * @return 게시판 정보 수정 후 성공(1), 실패(0) 여부
	 */
	@RequestMapping(value = "/developBoardModifyAjax", method = RequestMethod.POST)
	public @ResponseBody int developBoardModifyAjax(@ModelAttribute Board board){
		return boardService.updateDevelopBoardView(board);
	}
	
	/**
	 * developBoardView delete ajax
	 * @param board
	 * @return 게시판 정보 삭제 후 성공(1), 실패(0) 여부
	 */
	@RequestMapping(value = "/developBoardDeleteAjax", method = RequestMethod.POST)
	public @ResponseBody int developBoardDeleteAjax(@ModelAttribute Board board){
		return boardService.deleteDevelopBoardView(board);
	}
	
	/**
	 * developBoard Pwd Check Ajax
	 * @param board
	 * @return 비밀번호 일치 여부
	 */
	@RequestMapping(value = "/developBoardPwdCheckAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject developBoardPwdCheckAjax(@ModelAttribute Board board){
		return boardService.selectDevelopBoardPwd(board);
	}
	
	/**
	 * anonymousBoardList(익명 게시판) page
	 * @return
	 */
	@RequestMapping(value = "/anonymousBoardList", method = RequestMethod.GET)
	public String anonymousBoardList() {
		return "anonymousBoardList.tiles";
	}
	
	/**
	 * developBoard Read Count Ajax
	 * @param board
	 */
	@RequestMapping(value = "/developBoardReadCountAjax", method = RequestMethod.POST)
	public void developBoardReadCountAjax(@ModelAttribute Board board){
		boardService.updateDevelopBoardReadCount(board);
	}
	
}
