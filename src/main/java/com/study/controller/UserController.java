package com.study.controller;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.common.CommonUtil;
import com.study.model.Login;
import com.study.service.ApiService;
import com.study.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	@Resource(name = "apiService")
	private ApiService apiService;
	
	@Autowired
	@Resource(name = "userService")
	private UserService userService;
	
	@Autowired
	@Resource(name = "commonUtil")
	private CommonUtil commonUtil;
	
	/**
	 * userInsert page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userInsert", method = RequestMethod.GET)
	public String userInsert(Model model){
		return "userInsert.pop";
	}
	
	/**
	 * userInsert Ajax
	 * @param login
	 * @param request
	 * @return 가입 성공, 실패 여부(Y, D, C) 
	 */
	@RequestMapping(value = "/userInsertAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject userInsertAjax(@ModelAttribute Login login){
		return userService.InsertUserInfo(login);
	}

	/**
	 * userSnsCheck Ajax
	 * @param login
	 * @return 소셜 로그인 가입 성공, 실패 여부(Y, D)
	 */
	@RequestMapping(value = "/userSnsCheckAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject userSnsCheckAjax(@ModelAttribute Login login){
		return userService.userSnsCheck(login);
	}
	
	/**
	 * userProfile page
	 * @param model
	 * @param map
	 * @return 사용자 정보
	 */
	@RequestMapping(value = "/userProfile", method = RequestMethod.GET)
	public String userProfile(Model model, ModelMap map){
		map.put("userInfo",userService.selectUserInfo());
		return "userProfile.tiles";
	}
	
	/**
	 * userSettings page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userSettings", method = RequestMethod.GET)
	public String userSettings(Model model){
		model.addAttribute("loginUserId", commonUtil.getLoginUser().getUserId());
		return "userSettings.tiles";
	}
	
	/**
	 * userFindPass page 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/userFindPass", method = RequestMethod.GET)
	public String userFindPass(Model model){
		return "userFindPass.pop";
	}
	
	/**
	 * userFindPass Ajax
	 * @param login
	 * @return 가입 정보 이메일과 일치 여부(Y, N)
	 */
	@RequestMapping(value = "/userFindPassAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject userFindPassAjax(@ModelAttribute Login login){
		return userService.findUserPass(login);
	}

}
