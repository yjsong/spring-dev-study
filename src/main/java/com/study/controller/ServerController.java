package com.study.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.service.ServerService;

@Controller
@RequestMapping(value = "/server")
public class ServerController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	@Resource(name = "serverService")
	private ServerService serverService;
	
	/**
	 * state page
	 * @return
	 */
	@RequestMapping(value = "/state", method = RequestMethod.GET)
	public String state(){
		return "state.tiles";
	}
	
	/**
	 * 서버 상태 체크
	 * @return 
	 */
	@RequestMapping(value = "/sysInfo", method = RequestMethod.GET)
	public String sysInfo(){
		
		return "sysInfo";
	}
	
	/**
	 * 서버 상태 체크 Ajax
	 * @return  서버 IP, 서버상태(Y,N), total_memory_size, cpu_usage, 
	 * 			use_memory_percentage, hostname, free_memory_size, useable_space
	 */
	@RequestMapping(value = "/serverStateCheckAjax", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> serverStateCheckAjax(){
		return serverService.serverStateInfo();
	}

}
