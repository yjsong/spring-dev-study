package com.study.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.security.ShaPasswordEncoder;
import com.study.service.LoginService;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private ShaPasswordEncoder shaPasswordEncoder;
	
	@Autowired
	private LoginService loginService;
	
	/**
	 * login page
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login.default";
	}
	
	/**
	 * naver login page
	 */
	@RequestMapping(value = "/naver", method = RequestMethod.GET)
	public String naver() {
		return "naver.pop";
	}

	/**
	 * 네이버 로그인 회원정보 조회
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/naverAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject naverAjax(HttpServletRequest request){
		return loginService.selectNaverInfo(request.getParameter("code"));
	}

	/**
	 * 네이버 로그인 토큰 제거
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/deleteNaverTokenAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject deleteNaverTokenAjax(HttpServletRequest request){
		return loginService.deleteNaverToken();
	}
	
}
