package com.study.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.study.service.ApiService;

@Controller
@RequestMapping(value = "/api")
public class ApiController {

	private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	@Resource(name = "apiService")
	private ApiService apiService;

	/**
	 * captcha page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/captcha", method = RequestMethod.GET)
	public String captcha(Model model) {
		return "captcha.tiles";
	}

	/**
	 * captcha ajax(캡챠 이미지 URL 전달)
	 * @param request
	 * @return 캡챠 이미지 URL
	 */
	@RequestMapping(value = "/captchaAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject captchaAjax(HttpServletRequest request) {
		/** 캡챠 이미지 URL jsp로 전달 **/
		return apiService.createCaptchaImage();
	}

	/**
	 * captchaCompare ajax(입력받은 값과 비교)
	 * @param request
	 * @return 비교 후 결과 값(true, false)
	 */
	@RequestMapping(value = "/captchaCompareAjax", method = RequestMethod.POST)
	public @ResponseBody JSONObject captchaCompareAjax(HttpServletRequest request) {
		return apiService.compareCaptchaValue(request.getParameter("captchaText"));
	}

	/**
	 * mail page
	 * @return
	 */
	@RequestMapping(value = "/mail", method = RequestMethod.GET)
	public String mail() {
		return "mail.tiles";
	}

	/**
	 * map page
	 * @return
	 */
	@RequestMapping(value = "/map", method = RequestMethod.GET)
	public String map() {
		return "map.tiles";
	}
	
	/**
	 * naver map page(test)
	 * @return
	 */
	@RequestMapping(value = "/naverMap", method = RequestMethod.GET)
	public String naverMap() {
		return "naverMap.tiles";
	}

	/**
	 * google map page(test)
	 * @return
	 */
	@RequestMapping(value = "/googleMap", method = RequestMethod.GET)
	public String googleMap() {
		return "googleMap.tiles";
	}
	
	/**
	 * weather page
	 * @return 
	 */
	@RequestMapping(value = "/weather", method = RequestMethod.GET)
	public String skplanetxWeather() {
		return "weather.tiles";
	}

}
