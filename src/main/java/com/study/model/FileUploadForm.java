package com.study.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
 
public class FileUploadForm {
 
    private List<MultipartFile> files;
    private String pathDir;
    private String fileSeq;
    private String file_Type_Cd;
    private String user_Nm;

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public String getPathDir() {
		return pathDir;
	}

	public void setPathDir(String pathDir) {
		this.pathDir = pathDir;
	}

	public String getFileSeq() {
		return fileSeq;
	}

	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}

	public String getUser_Nm() {
		return user_Nm;
	}

	public void setUser_Nm(String user_Nm) {
		this.user_Nm = user_Nm;
	}

	public String getFile_Type_Cd() {
		return file_Type_Cd;
	}

	public void setFile_Type_Cd(String file_Type_Cd) {
		this.file_Type_Cd = file_Type_Cd;
	}
	
}
