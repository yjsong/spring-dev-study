package com.study.model;


public class Common {

	private int page;				//현재 페이지 번째
	private int rec;				//한 페이지당 표시 행 수
	private int records;			//전체 레코드 수
	private int pageSize;			//페이징 쿼리를 위한페이지 변수
	
	private String searchUserId;	//검색 아이디 
	private String searchUserPwd;	//검색 비밀번호
	private String searchUserNm;	//검색 사용자명
	
	private String searchItem;		//검색항목
	private String searchValue;		//검색 값
	private String searchYn;		//검색 유무
	
	//날짜 검색
	private String startDate;		//시작 날짜
	private String endDate;			//종료 날짜
	
	
	public String getSearchYn() {
		return searchYn;
	}
	public void setSearchYn(String searchYn) {
		this.searchYn = searchYn;
	}
	public String getSearchItem() {
		return searchItem;
	}
	public void setSearchItem(String searchItem) {
		this.searchItem = searchItem;
	}
	public String getSearchValue() {
		return searchValue;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSearchUserPwd() {
		return searchUserPwd;
	}
	public void setSearchUserPwd(String searchUserPwd) {
		this.searchUserPwd = searchUserPwd;
	}
	public String getSearchUserId() {
		return searchUserId;
	}
	public void setSearchUserId(String searchUserId) {
		this.searchUserId = searchUserId;
	}
	public String getSearchUserNm() {
		return searchUserNm;
	}
	public void setSearchUserNm(String searchUserNm) {
		this.searchUserNm = searchUserNm;
	}
	public int getRec() {
		return rec;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public void setRec(int rec) {
		this.rec = rec;
	}
	public int getRecords() {
		return records;
	}
	public void setRecords(int records) {
		this.records = records;
	}
	public int getPageSize() {
		return (page - 1) * 10;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
