package com.study.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;



public class Login implements UserDetails{
	private static final long serialVersionUID = -8051313615469680074L;

	private int rnum;
	private String userKey;
	private String userId;
	private String userNm;
	private String userPass;
	private String userEmail;
	private String userCellNum;
	private String joinDate;
	private String useYn;
	private String snsType;
	private String autrNm;
	private String autrExpl;
	private String captchaText;
	
	private Set<GrantedAuthority> authorities;
	
	public Login() {
		
	}
	
	public Login(String userKey, String userId, String userNm, String userPass, String userEmail, String userCellnum, String joinDate,
					Collection<? extends GrantedAuthority> authorities, String useYn, String snsType) {
		this.userKey = userKey;
		this.userId = userId;
		this.userNm = userNm;
		this.userPass = userPass;
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
		this.useYn = useYn;
		this.snsType = snsType;
	}
	
	
	public Login(String userKey, String userId, String userNm, String userPass, String name, String kunnr, String kunnrName, String csPernr, String csName,
			Collection<? extends GrantedAuthority> authorities, String useYn, String snsType) {
		this.userKey = userKey;
		this.userId = userId;
		this.userNm = userNm;
		this.userPass = userPass;
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
		this.useYn = useYn;
		this.snsType = snsType;
	}
	
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getSnsType() {
		return snsType;
	}
	public void setSnsType(String snsType) {
		this.snsType = snsType;
	}
	public String getCaptchaText() {
		return captchaText;
	}
	public void setCaptchaText(String captchaText) {
		this.captchaText = captchaText;
	}
	public String getAutrNm() {
		return autrNm;
	}
	public void setAutrNm(String autrNm) {
		this.autrNm = autrNm;
	}
	public String getAutrExpl() {
		return autrExpl;
	}
	public void setAutrExpl(String autrExpl) {
		this.autrExpl = autrExpl;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserCellNum() {
		return userCellNum;
	}
	public void setUserCellNum(String userCellNum) {
		this.userCellNum = userCellNum;
	}
	public String getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}
	
	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}


	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return userPass;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return userKey;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return "Y".equals(useYn);
	}
	
	public boolean hasAuthority(String authorityCode) {
		GrantedAuthority ga = new SimpleGrantedAuthority(authorityCode);
		return this.authorities.contains(ga);
	}
	
	private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
        // Ensure array iteration order is predictable (as per UserDetails.getAuthorities() contract and SEC-717)
        SortedSet<GrantedAuthority> sortedAuthorities =
            new TreeSet<GrantedAuthority>(new AuthorityComparator());

        for (GrantedAuthority grantedAuthority : authorities) {
            sortedAuthorities.add(grantedAuthority);
        }

        return sortedAuthorities;
    }
	
	public int getRnum() {
		return rnum;
	}

	public void setRnum(int rnum) {
		this.rnum = rnum;
	}

	private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {
        private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            // Neither should ever be null as each entry is checked before adding it to the set.
            // If the authority is null, it is a custom authority and should precede others.
            if (g2.getAuthority() == null) {
                return -1;
            }

            if (g1.getAuthority() == null) {
                return 1;
            }

            return g1.getAuthority().compareTo(g2.getAuthority());
        }
    }
}
